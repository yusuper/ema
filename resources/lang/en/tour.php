<?php

return [
    'tours'=>'Tours',
    'private_tour'=>'PRIVATE TOUR',
    'standard_tour'=>'STANDARD TOUR',
    'standard_tours'=>'Standard tours',
    'private_tours'=>'Private tours',
];