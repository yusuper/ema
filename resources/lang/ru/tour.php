<?php

return [
    'tours'=>'Туры',
    'private_tour'=>'ПРИВАТНЫЙ ТУР',
    'standard_tour'=>'СТАНДАРТНЫЙ ТУР',
    'standard_tours'=>'Стандартные туры',
    'private_tours'=>'Приватные туры',
];