<?php

return [
    'home'=>'Главная',
    'tours'=>'Туры',
    'price'=>'Цена',
    'address'=>'Адрес',
    'more'=>'Подробнее',
    'hotels'=>'Отели',
    'leisure'=>'Досуг',
    'search'=>'Поиск',
    'search_filters'=>'Поиск и фильтры',
    'search_by_name'=>'Поиск по названию',
    'quote'=>'- Удивительный Кыргызстан!<br>Путешествие
                    в<br>прошлое и будущее',
    'facts'=>'Факты',
    'interesting_facts'=>'Интересные факты',
    'about_us'=>'О нас',
    'our_team'=>'Наша команда',
    'our_contact'=>'НАШИ КОНТАКТЫ',
    'write_us'=>'НАПИШИТЕ НАМ',
    'your_name'=>'Ваше Имя',
    'your_phone_number'=>'Номер вашего телефона',
    'your_email'=>'Ваш email',
    'message'=>'Сообщение',
    'send'=>'Отправить',
    'publication_date'=>'Дата публикации',
    'photogallery'=>'Фотогалерея',
    'video'=>'Видео',
    'next'=>'След',
    'prev'=>'Пред',
    'interesting_news'=>'Интересные новости'
];