<div class="row">
    @if(!$create)
        <div class="col-md-12">
            <fieldset>
                <legend>Изображения</legend>

                <form action="{{route('admin.static.pictures.media',['staticPictureId'=>$staticPicture->id])}}"
                      method="post"
                      id="formImage">
                    {{csrf_field()}}
                    <input type="file" name="image[]" class="form-input-image" style="display: none" multiple
                           accept="image/x-png,image/gif,image/jpeg">
                    <button type="button" class="btn btn-success btn-sm add-photo">Добавить фото</button>
                </form>
                <div class="media-block">

                    @foreach($medias as $row)
                        <div class="row">
                            @foreach($row as $media)
                                @include('backend.static_pictures.media_item')
                            @endforeach
                        </div>
                    @endforeach

                </div>

            </fieldset>
        </div>
    @endif
</div>