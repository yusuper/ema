@extends('backend.layouts.master')

@section('title')
    {{ $title }}
@endsection

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $title }}
                    </h3>
                </div>
            </div>
        </div>


        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <table class="table table-bordered m-table ajax-content" id="ajaxTable"
                       data-ajax-content-url="{{ route('admin.static.pictures.list') }}">
                    <thead>
                    <tr>
                        <th class="text-center" width="50">Id</th>
                        <th>Заголовок</th>
                        <th>Картинка</th>
                        <th class="text-center" width="100"><i class="fa fa-bars" aria-hidden="true"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

                <div class="pagination_placeholder" data-table-id="ajaxTable"></div>
            </div>
        </div>
        <!--end::Section-->
    </div>

@endsection

@push('modules')
    <script src="/app/js/modules/userManagement.js"></script>
@endpush