@foreach($staticPictures as $staticPicture)
    @include('backend.static_pictures.item')
@endforeach

@if(!$staticPictures->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif