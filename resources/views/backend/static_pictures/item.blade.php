<tr class="row-{{$staticPicture->id }}">

    <td style="text-align: center">{{ $staticPicture->id }}</td>
    <td align="auto">{{ $staticPicture->title }}</td>
    <td>
        @if(isset($staticPicture->mainImage))
            <a href="#" data-modal="#largeModal"
               data-modal-content-url="{{ route('admin.static.pictures.crop.create',['modelId'=>$staticPicture->id]) }}"
               class="m-portlet__nav-link m-portlet__nav-link--icon has-modal-content" data-container="body"
               data-toggle="m-tooltip" data-placement="top">
                <img style="max-width: 100px" src="{{$staticPicture->mainImage->original_file_name}}">
            </a>
        @endif
    </td>

    <td class="text-center">
        <a href="{{ route('admin.static.pictures.edit', ['staticPictureId' => $staticPicture->id ]) }}"  class="handle-click" data-type="modal" data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>
        {{--<a class="handle-click" data-type="delete-table-row"--}}
           {{--data-confirm-title="Удаление"--}}
           {{--data-confirm-message="Вы уверены, что хотите удалить страницу"--}}
           {{--data-cancel-text="Нет"--}}
           {{--data-confirm-text="Да, удалить" href="{{ route('admin.hotels.destroy', [ 'hotelId' => $hotel->id ]) }}">--}}
            {{--<i class="la la-trash"></i>--}}

        {{--</a>--}}
    </td>
</tr>