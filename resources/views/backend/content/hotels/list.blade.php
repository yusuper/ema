@foreach($hotels as $hotel)
    @include('backend.content.hotels.hotel_item')
@endforeach

@if(!$hotels->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif