<div class="col-md-6" style="margin-top: 5px;margin-bottom: 5px">
    {{--{!! dump($media->video_id) !!}--}}
    @if($media->type == 'image' && $media->original_file_name)
        <div class="text-center">
            <img style="max-width: 100%;" src="{{$media->original_file_name}}" alt="нет медиа"  class="img-thumbnail">
        </div>
    @elseif($media->type == 'video')

        <div class="text-center">
            <img style="max-width: 100%;"  src=@if(!isset($media->size)) "https://img.youtube.com/vi/{{$media->video_id}}/maxresdefault.jpg"
            @else
                "{{$media->original_file_name}}"
            @endif
            alt="нет медиа"  class="img-thumbnail">
        </div>
    @endif
    <div class="controls" style="margin-top: 10px;">
        <a class="delete-media-data btn btn-sm btn-danger pull-right" href="{{route('admin.tours.media.delete',['mediaId'=>$media->id])}}"><i class="fa fa-trash"></i></a>
        @if($media->type != 'image')
            <form   action="{{route('admin.hotels.media.update',['mediaId'=>$media->id])}}"
                    enctype="multipart/form-data"
                    method="POST"
                    {{--id="mediaForm"--}}>
                {{csrf_field()}}
                <input type="file" name="image" class="media-file-input" style="display: none"
                        {{--accept="image/x-png,image/gif,image/jpeg"--}}>
                <button type="submit" class="btn btn-warning btn-sm pull-right edit-media"><i class="fa fa-edit"></i></button>
            </form>
        @endif
        <a @if($media->main_image) style="display: none" @endif class="make-main-image btn btn-sm btn-info pull-left" href="{{route('admin.hotels.main.media',['hotelId'=>$hotel->id, 'mediaId'=>$media->id])}}">Главная</a>
    </div>
</div>