<tr class="row-{{$tour->id }}">

    <td style="text-align: center">{{ $tour->id }}</td>
    <td align="auto">{{ $tour->title }}</td>
    {{--<td>@if(isset($tour->category)){{$tour->category->title}} @else '' @endif</td>--}}
    <td>
        <a href="#" data-modal="#largeModal"
           data-modal-content-url="{{ route('admin.tours.crop.create',['tourId'=>$tour->id]) }}"
           class="m-portlet__nav-link m-portlet__nav-link--icon has-modal-content" data-container="body"
           data-toggle="m-tooltip" data-placement="top">
            <img style="max-width: 100px" src="@if($tour->mainImage){{$tour->mainImage->original_file_name}} @else /images/placeholder_small.png @endif">
        </a>
    </td>
    <td>
        @foreach(config('project.foreign_locales') as $locale)
        <a href="{{route('home.tours.item', ['locale' => $locale,'slug'=>$tour->slug])}}" target="_blank">
        {{$locale}}
        </a>
        @endforeach
    </td>
    <td class="text-center">
        <a href="{{ route('admin.tours.edit', ['tourId' => $tour->id ]) }}"  class="handle-click" data-type="modal" data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>
        <a class="handle-click" data-type="delete-table-row"
           data-confirm-title="Удаление"
           data-confirm-message="Вы уверены, что хотите удалить страницу"
           data-cancel-text="Нет"
           data-confirm-text="Да, удалить" href="{{ route('admin.tours.destroy', [ 'tourId' => $tour->id ]) }}">
            <i class="la la-trash"></i>

        </a>
    </td>
</tr>