@foreach($tours as $tour)
    @include('backend.content.tours.tour_item')
@endforeach

@if(!$tours->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif