<tr class="row-{{$fact->id }}">

    <td style="text-align: center">{{ $fact->id }}</td>
    <td align="auto">{{ $fact->title }}</td>
    <td>
        @if(isset($fact->mainImage))
            <a href="#" data-modal="#largeModal"
               data-modal-content-url="{{ route('admin.facts.crop.create',['modelId'=>$fact->id]) }}"
               class="m-portlet__nav-link m-portlet__nav-link--icon has-modal-content" data-container="body"
               data-toggle="m-tooltip" data-placement="top">
                <img style="max-width: 100px" src="{{$fact->mainImage->original_file_name}}">
            </a>
        @endif
    </td>
    <td align="auto">{{ $fact->short_desc }}</td>
    {{--<td>--}}
        {{--@foreach($locales as $locale)--}}
        {{--<a href="{{route('frontend.pages.page', ['locale' => $locale,'slug'=>$page->slug])}}" target="_blank">--}}
        {{--{{$locale}}--}}
        {{--</a>--}}
        {{--@endforeach--}}
    {{--</td>--}}
    <td class="text-center">
        <a href="{{ route('admin.facts.edit', ['factId' => $fact->id ]) }}"  class="handle-click" data-type="modal" data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>
        <a class="handle-click" data-type="delete-table-row"
           data-confirm-title="Удаление"
           data-confirm-message="Вы уверены, что хотите удалить страницу"
           data-cancel-text="Нет"
           data-confirm-text="Да, удалить" href="{{ route('admin.facts.destroy', [ 'factId' => $fact->id ]) }}">
            <i class="la la-trash"></i>

        </a>
    </td>
</tr>