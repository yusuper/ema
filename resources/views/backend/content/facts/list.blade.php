@foreach($facts as $fact)
    @include('backend.content.facts.item')
@endforeach

@if(!$facts->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif