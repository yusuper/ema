@foreach($items as $page)
    @include('backend.content.pages.pages_item')
@endforeach

@if(!$items->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif