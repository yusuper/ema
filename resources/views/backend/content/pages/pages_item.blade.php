<tr class="row-{{$page->id }}">

    <td style="text-align: center">{{ $page->id }}</td>
    <td align="auto">{{ $page->title }}</td>
    <td>
        {{--@foreach($locales as $locale)--}}
            {{--<a href="{{route('frontend.pages.page', ['locale' => $locale,'slug'=>$page->slug])}}" target="_blank">--}}
                {{--{{$locale}}--}}
            {{--</a>--}}
        {{--@endforeach--}}
    </td>
    <td class="text-center">
        <a href="{{ route('admin.content.pages.edit', ['pageId' => $page->id ]) }}"  class="handle-click" data-type="modal" data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>
        <a class="handle-click" data-type="delete-table-row"
           data-confirm-title="Удаление"
           data-confirm-message="Вы уверены, что хотите удалить страницу"
           data-cancel-text="Нет"
           data-confirm-text="Да, удалить" href="{{ route('admin.content.pages.destroy', [ '$pageId' => $page->id ]) }}">
            <i class="la la-trash"></i>

        </a>
    </td>
</tr>