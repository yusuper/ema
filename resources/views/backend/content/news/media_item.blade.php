<div class="col-md-6" style="margin-top: 5px;margin-bottom: 5px">
    {{--{{dump($media)}}--}}
    <div class="text-center">
        <img style="max-width: 100%;" src="{{$media->original_file_name}}" alt="нет медиа"  class="img-thumbnail">
    </div>
    <div class="controls" style="margin-top: 10px;">
        <a class="delete-media-data btn btn-sm btn-danger pull-right" href="{{route('admin.content.news.delete.media',['mediaId'=>$media->id])}}"><i class="fa fa-trash"></i></a>
        <a @if($media->main_image) style="display: none" @endif class="make-main-image btn btn-sm btn-info pull-left" href="{{route('admin.content.news.main.media',['itemId'=>$news->id, 'mediaId'=>$media->id])}}">Главная</a>
    </div>
</div>