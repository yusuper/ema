@foreach($leisures as $leisure)
    @include('backend.leisure.leisure_item')
@endforeach

@if(!$leisures->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif