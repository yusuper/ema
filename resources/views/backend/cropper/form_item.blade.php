@foreach($images as $row)
    <div class="row">
        @foreach($row as $image)
            <div class="col-md-5">

                <a class="show-crop"
                   href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                    <img style="max-width: 100%;" class="img-thumbnail" src="{{$image->original_file_name}}" alt="нет медиа">
                </a>
                @if($image->main_image == 1)

                    <a class="btn btn-sm btn-info">Главное фото</a>
                @endif
                <div class="row options">
                    <div class="col-md-5">
                        <a class="show-crop btn-success" data-original-title="1.77777"
                           href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                            16/9
                        </a>
                    </div>
                    <div class="col-md-5">
                        <a class="show-crop btn-success" data-original-title="1.33333"
                           href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                            4/3
                        </a>
                    </div>
                    <div class="col-md-5">
                        <a class="show-crop btn-success" data-original-title="1"
                           href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                            1 / 1
                        </a>
                    </div>
                    <div class="col-md-5">
                        <a class="show-crop rotate" data-rotate="45"
                           href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                            <i class="fa fa-sync-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a class="handle-click" data-type="ajax-get" href="{{route('admin.gallery.crop.media.up',['imageId'=>$image->id])}}">
                        <i class="fa fa-arrow-alt-circle-left"></i>
                    </a><br>
                    <a class="handle-click" data-type="ajax-get" href="{{route('admin.gallery.crop.media.down',['imageId'=>$image->id])}}">
                        <i class="fa fa-arrow-alt-circle-right"></i>
                    </a>
                </div>

            </div>
        @endforeach
    </div>
@endforeach