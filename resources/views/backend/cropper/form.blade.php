<div class="row" style="min-height: 400px;">
    <div class="col-md-4 order-list" style="border-right: 1px outset">
        @foreach($images as $row)
            <div class="row">
                @foreach($row as $image)
                    <div class="col-md-5">

                        <a class="show-crop"
                           href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                            <img style="max-width: 100%;" class="img-thumbnail"
                                 src="{{asset('storage/uploaded_images/128_' . $image->getOriginal('original_file_name'))}}"
                                 alt="нет медиа">
                        </a>
                        @if($image->main_image == 1)

                            <a class="btn btn-sm btn-info">Главное фото</a>
                        @endif
                        <div class="row options">
                            <div class="col-md-5">
                                <a class="show-crop btn-success" data-original-title="1.77777"
                                   href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                                    16/9
                                </a>
                            </div>
                            <div class="col-md-5">
                                <a class="show-crop btn-success" data-original-title="1.33333"
                                   href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                                    4/3
                                </a>
                            </div>
                            <div class="col-md-5">
                                <a class="show-crop btn-success" data-original-title="1"
                                   href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                                    1 / 1
                                </a>
                            </div>
                            <div class="col-md-5">
                                <a class="show-crop rotate" data-rotate="45"
                                   href="{{route('admin.gallery.crop.show',['imageId'=>$image->id])}}">
                                    <i class="fa fa-sync-alt"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a class="handle-click" data-type="ajax-get"
                               href="{{route('admin.gallery.crop.media.up',['imageId'=>$image->id])}}">
                                <i class="fa fa-arrow-alt-circle-left"></i>
                            </a><br>
                            <a class="handle-click" data-type="ajax-get"
                               href="{{route('admin.gallery.crop.media.down',['imageId'=>$image->id])}}">
                                <i class="fa fa-arrow-alt-circle-right"></i>
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-5">
                <div class="col-md-12" id="cropImagePlaceholder">

                </div>


                <div class="col-md-12 scale-form pull-left" style="display: none;">
                    <form method="post" id="cropForm" class="ajax-submit"
                          data-block-element="#largeModal .modal-body" action="">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="width">Ширина</label><br>
                            <input name="width" id="width">
                        </div>
                        <div class="form-group">
                            <label for="height">Высота</label><br>
                            <input name="height" id="height">
                        </div>
                        <div class="form-group">
                            {{--<label for="scaleX">Масштаб X</label><br>--}}
                            <input type="hidden" name="X" id="X">
                        </div>
                        <div class="form-group">
                            {{--<label for="scaleY">Масштаб Y</label><br>--}}
                            <input type="hidden" name="Y" id="Y">
                        </div>
                        <button class="btn btn-success btn-sm">Сохранить</button>
                    </form>
                </div>
            </div>
            {{--<div class="col-md-8 " style="display: none;">--}}
            {{----}}
            {{--</div>--}}

            <div class="col-md-6" id="croppedDiv" style="margin-left: 6%">
                <div class="col-md-12 pull-right">
                    <h4 id="croppedText"></h4>
                    <img id="croppedImage" class="img-thumbnail" style="max-width: 100%" src="">
                    <form method="post" id="mediaName" class="ajax-submit"
                          data-block-element="#largeModal .modal-body"
                          action="{{route('admin.news.media.desc.save',['mediaId'=>$image->id])}}">
                        <div class="form-group">
                            <label for="imageName">Название</label><br>
                            <input name="imageName" id="imageName" value="">
                        </div>
                        <div class="form-group">
                            <label for="imageSecondName">Второе название</label><br>
                            <input name="imageSecondName" id="imageSecondName" value="">
                        </div>
                        <button class="btn btn-success btn-sm">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


