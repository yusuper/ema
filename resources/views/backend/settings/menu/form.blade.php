<form action="{{ $formAction }}" method="post" class="ajax-submit " data-block-element="#regularModal .modal-body"
      id="menusForm">
    <ul class="nav nav-tabs" role="tablist">
        @foreach(config('project.locales') as $locale)
            <li class="nav-item ">
                <a class="nav-link @if($loop->first) active @endif" data-toggle="tab"
                   href="#{{$locale['locale']}}">
                    {{$locale['desc']}}
                </a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach(config('project.locales') as $locale)
            <div class="tab-pane @if($loop->first) active @endif" id="{{$locale['locale']}}" role="tabpanel">
                <div class="form-group m-form__group">
                    <label for="name.{{$locale['locale']}}">Наименование ({{$locale['locale']}})</label>
                    <input type="text" name="name[{{$locale['locale']}}]" id="name.{{$locale['locale']}}"
                           class="form-control m-input"
                           @if(isset($menu)) value="{{ $menu->getTranslation('name', $locale['locale']) }}" @endif>
                    <span class="help-block"></span>
                </div>
            </div>
        @endforeach
    </div>


    <div class="form-group">
        <button type="submit" class="btn btn-success btn-sm">{{  $buttonText }} </button>
        <button type="button" class="btn btn-accent btn-sm float-right" data-dismiss="modal">Отмена</button>
    </div>
</form>