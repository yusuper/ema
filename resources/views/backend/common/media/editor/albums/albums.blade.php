@foreach($albums as $row)
    <div class="row">
        @foreach($row as $album)
            <div class="col-md-3">
                <a href="@foreach($album->media as $image){{$image->original_file_name.' '}}@endforeach" class="insert-album" data-name="{{$album->desc}}"
                   data-name_2 = "{{$album->desc_2}}" data-description="{{$album->getTranslation('title','ru')}}">
                    <img class="img-thumbnail" width="100%" src="{{$album->mainImage->thumb_256}}">
                    <h6>{{$album->title}}</h6>
                </a>

            </div>
        @endforeach
    </div>
@endforeach