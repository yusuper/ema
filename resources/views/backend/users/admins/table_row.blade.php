<tr class="table-row-{{$result->id}}">
    <td class="text-center"><input type="checkbox" @if(isset($keepChecked)) checked @endif class="parent-checkbox" name="checkbox[{{$result->id}}]" data-id="{{$result->id}}"></td>
    <td class="text-center">
        <a href="#" class="has-modal-content" data-modal-selector="#largeModal"
           data-modal-content-url="{{route('admin.users.admins.edit', ['id' => $result->id])}}">
            {{$result->id}}
        </a>
    </td>
    <td>{{$result->name}}</td>
    <td>{{$result->login}}</td>
    <td>{{$result->jabber}}</td>
    <td>{!! $result->telegram !!}</td>
    <td class="clickable has-modal-content" data-modal-selector="#defaultModal"
        data-modal-content-url="{{route('admin.users.admins.editroles', ['id' => $result->id])}}">{{$result->listRoles()}}</td>
    <td class="text-center">
        <select class="select-inplace form-control m-bootstrap-select m_selectpicker"
                data-url="{{route('admin.users.admins.updatenotifytype', ['id' => $result->id])}}" data-field="notify_type">
            @foreach(config('project.adminNotifyTypes') as $k => $value)
                <option value="{{$k}}"
                        @if((int) $result->notify_type == $k) selected @endif>{{$value}}</option>
            @endforeach
        </select>
    </td>
    @if(!isset($myId) || $myId != $result->id)
        <td class="text-center switch-active clickable"
            data-url="{{route('admin.users.admins.switchactive', ['id' => $result->id])}}">{!! $result->active !!}</td>
    @else
        <td class="text-center"></td>
    @endif
    <td class="text-center switch-super clickable"
        data-url="{{route('admin.users.admins.switchsuper', ['id' => $result->id])}}">{!! $result->super_user !!}</td>
    @if(!isset($myId) || $myId != $result->id)
        <td class="text-center delete-item clickable"
            data-url="{{route('admin.users.admins.delete', ['id' => $result->id])}}"><i class="fa fa-trash-o"
                                                                                        aria-hidden="true"></i></td>
    @else
        <td class="text-center"></td>
    @endif

</tr>