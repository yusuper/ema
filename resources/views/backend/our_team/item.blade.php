<tr class="row-{{$man->id }}">

    <td style="text-align: center">{{ $man->id }}</td>
    <td align="auto">{{ $man->title }}</td>
    <td>
        @if(isset($man->mainImage))
            <a href="#" data-modal="#largeModal"
               data-modal-content-url="{{ route('admin.team.crop.create',['modelId'=>$man->id]) }}"
               class="m-portlet__nav-link m-portlet__nav-link--icon has-modal-content" data-container="body"
               data-toggle="m-tooltip" data-placement="top">
                <img style="max-width: 100px" src="{{$man->mainImage->original_file_name}}">
            </a>
        @endif
    </td>

    <td class="text-center">
        <a href="{{ route('admin.team.edit', ['manId' => $man->id ]) }}"  class="handle-click" data-type="modal" data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>
        <a class="handle-click" data-type="delete-table-row"
           data-confirm-title="Удаление"
           data-confirm-message="Вы уверены, что хотите удалить сотрудника"
           data-cancel-text="Нет"
           data-confirm-text="Да, удалить" href="{{ route('admin.team.destroy', [ 'manId' => $man->id ]) }}">
            <i class="la la-trash"></i>

        </a>
    </td>
</tr>