@foreach($ourTeam as $man)
    @include('backend.our_team.item')
@endforeach

@if(!$ourTeam->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif