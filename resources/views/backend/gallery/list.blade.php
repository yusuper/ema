@foreach($albums as $album)
    @include('backend.gallery.item')
@endforeach

@if(!$albums->count())
    <tr>
        <td colspan="6" class="text-center">Данных не найдено</td>
    </tr>
@endif