<tr class="row-{{$album->id }}">

    <td style="text-align: center">{{ $album->id }}</td>
    <td align="auto">{{ $album->title }}</td>
    <td>
        @if(isset($album->mainImage))
        <a href="#" data-modal="#largeModal"
           data-modal-content-url="{{ route('admin.gallery.crop.create',['modelId'=>$album->id]) }}"
           class="m-portlet__nav-link m-portlet__nav-link--icon has-modal-content" data-container="body"
           data-toggle="m-tooltip" data-placement="top">
            <img style="max-width: 100px" src="{{$album->mainImage->original_file_name}}">
        </a>
            @endif
    </td>
    <td class="text-center">
        @if($album->is_video == 1) Видео @elseif($album->is_video == 0) Фото @endif
    </td>
    <td>
        {{--@foreach($locales as $locale)--}}
        {{--<a href="{{route('frontend.pages.page', ['locale' => $locale,'slug'=>$page->slug])}}" target="_blank">--}}
        {{--{{$locale}}--}}
        {{--</a>--}}
        {{--@endforeach--}}
    </td>
    <td class="text-center">
        <a href="@if($album->is_video == 0){{ route('admin.gallery.edit', ['itemId' => $album->id ]) }}
        @else{{ route('admin.gallery.edit.video', ['itemId' => $album->id ]) }} @endif" class="handle-click" data-type="modal"
           data-modal="#superLargeModal">
            <i class="la la-edit"></i>
        </a>
        <a class="handle-click" data-type="delete-table-row"
           data-confirm-title="Удаление"
           data-confirm-message="Вы уверены, что хотите удалить альбом"
           data-cancel-text="Нет"
           data-confirm-text="Да, удалить" href="{{ route('admin.gallery.destroy', [ 'itemId' => $album->id ]) }}">
            <i class="la la-trash"></i>

        </a>
    </td>
</tr>