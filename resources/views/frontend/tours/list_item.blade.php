<div class="col-lg-4 col-md-6">
    <a href="@if(isset($standardTour))
    {{route('home.tours.standard.item',['locale'=>$currentLocale,'slug'=>$tour->slug])}}
    @else
    {{route('home.tours.private.item',['locale'=>$currentLocale,'slug'=>$tour->slug])}}
    @endif" class="tours__item">
        <div class="tours__img"><img
                    src="@if(isset($tour->mainImage)){{$tour->mainImage->original_file_name}}@else /images/placeholder_small.png @endif"
                    alt="">
            <div class="tours__price"><p>@lang('common.price'): {{$tour->price}}</p></div>
        </div>
        <div class="tours__desc"><h2
                    class="tours__caption">{{$tour->getTranslation('title',$currentLocale)}}</h2>
            <p>{{$tour->getTranslation('short_desc',$currentLocale)}}...</p></div>
    </a>
</div>