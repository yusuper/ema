@extends('frontend.layouts.master')

@section('content')
    @if(isset($tour))
        <div class="news news--detail">
            <div class="container" style="padding-bottom: 4rem">
                <div class="breadCrumbs">
                    <ul>
                        <li><a href="{{route('home',['locale'=>$currentLocale])}}">@lang('common.home')</a></li>
                        <li><a href="@if(isset($standardTour))
                            {{route('home.tours.standard',['locale'=>$currentLocale])}}
                            @else
                            {{route('home.tours.vip',['locale'=>$currentLocale])}}
                            @endif">
                                @if(isset($standardTour))
                                    @lang('tour.standard_tours')
                                @else
                                    @lang('tour.private_tours')
                                @endif</a></li>
                        <li>{{$tour->title}}</li>
                    </ul>
                </div>
                <div class="news__data"><span>@lang('common.publication_date')</span>
                    <p>{{date('Y-m-d',strtotime($tour->created_at))}}</p></div>
                <h2 class="news__detailCaption">{{$tour->title}}</h2>
                <p>@if(isset($standardTour))
                        {!! $tour->getTranslation('standard_desc',$currentLocale) !!}
                    @else
                        {!! $tour->getTranslation('private_desc',$currentLocale) !!}
                    @endif
                </p>
                {{--<div class="fb-share-button"--}}
                     {{--data-href="{{$currentUrl}}"--}}
                     {{--data-layout="button_count">--}}
                {{--</div>--}}
                <div class="likely">
                    <div data-url="{{$currentUrl}}" class="facebook">Share</div>
                    <div data-url="{{$currentUrl}}" class="twitter">Tweet</div>
                    {{--<div class="gplus">+1</div>--}}
                    {{--<div data-url="{{$currentUrl}}" class="vkontakte">Share</div>--}}
                    {{--<div class="pinterest">Pin</div>--}}
                    {{--<div class="odnoklassniki">Like</div>--}}
                    <div data-url="{{$currentUrl}}" class="telegram">Send</div>
                    {{--<div class="linkedin">Share</div>--}}
                    <div data-url="{{$currentUrl}}" class="whatsapp">Send</div>
                </div>
            </div>
        </div>

    @endif


@endsection
@push('scripts')
    {{--<script>(function (d, s, id) {--}}
            {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
            {{--if (d.getElementById(id)) return;--}}
            {{--js = d.createElement(s);--}}
            {{--js.id = id;--}}
            {{--js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";--}}
            {{--fjs.parentNode.insertBefore(js, fjs);--}}
        {{--}(document, 'script', 'facebook-jssdk'));</script>--}}
@endpush