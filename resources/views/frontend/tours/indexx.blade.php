@extends('frontend.partials.parts')

@section('content')
    <div class="container">
        @include('frontend.partials.breadcrumps')
        <div class="page__header">
            <h1 class="page__header-title">{{$pageItem}}</h1>
        </div>
        <div class="row">
            {{--<div class="flex-md-wrap">--}}
                @foreach($tours as $tour)
                    <div class="col-md-4 tour-item">
                        <div class="card-mb-4 box-shadow">
                            <img class="card-img"
                                 src="@if(isset($tour->mainImage)){{$tour->mainImage->original_file_name}}@else /images/placeholder_small.png @endif">


                        </div>
                        <div class="card-body">
                            <h4>{{$tour->getTranslation('title',$currentLocale)}}</h4>
                            <p>{{$tour->getTranslation('short_desc',$currentLocale)}}...</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{route('home.tours.item',['locale'=>$currentLocale,'slug'=>$tour->slug])}}" class="more-link" style="text-decoration: none;">
                                        <h6>@lang('common.more')</h6></a>
                                </div>
                                <p class="tour__price"><span>@lang('common.price'):</span>{{$tour->price}}</p>
                            </div>
                        </div>

                    </div>
                @endforeach
            {{--</div>--}}
        </div>
        {{--{!! $page->getTranslation('content',$currentLocale) !!}--}}


    </div>
@endsection