@extends('frontend.layouts.master')

@section('content')

    <div class="tours">
        <div class="container">
            <div class="breadCrumbs">
                <ul>
                    <li><a href="{{route('home',['locale'=>$currentLocale])}}">@lang('common.home')</a></li>
                    {{--<li><a href="#">lorem</a></li>--}}
                    <li>{{$title}}</li>
                </ul>
            </div>
            <div class="captionSection captionSection--dark wow fadeInDown"
                 style="visibility:visible;animation-name:fadeInDown"><h2>{{$title}}</h2></div>
            {{--<div class="tours__tabs">--}}
            {{--<div class="tours__tabsItem tours__tabsItem--active"><p>VIP туры</p></div>--}}
            {{--<div class="tours__tabsItem"><p>Страндартные</p></div>--}}
            {{--</div>--}}
            <div class="row" style="padding-top: 40px;">
                <div class="col-lg-3 order-lg-last">
                    <div class="tours__rightBar"><h2 class="tours__searchCaption">@lang('common.search')</h2>
                        <div class="tours__searchWrap">
                            <form action="{{ route('home.tours.list',['locale' => $currentLocale]) }}" id="filterForm"
                                  method="get">
                                <div class="tours__search">
                                    <input id="filterSearch" type="search" placeholder="@lang('common.search_by_name')"
                                           data-url="{{route('home.tours.list',['locale'=>$currentLocale])}}"
                                           name="filterSearch" class="form-control filters__search">
                                </div>
                                <div class="tours__checkWraphidden">
                                    {{--<div class="tours__checkWrap"><input type="checkbox" id="t1"> <label--}}
                                    {{--for="t1">lorem</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="tours__checkWrap"><input type="checkbox" id="t2"> <label--}}
                                    {{--for="t2">lorem</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="tours__checkWrap"><input type="checkbox" id="t3"> <label--}}
                                    {{--for="t3">lorem</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="tours__checkWrap"><input type="checkbox" id="t4"> <label--}}
                                    {{--for="t4">lorem</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="tours__checkWrap"><input type="checkbox" id="t5"> <label--}}
                                    {{--for="t5">lorem</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="tours__checkWrap"><input type="checkbox" id="t6"> <label--}}
                                    {{--for="t6">lorem</label>--}}
                                    {{--</div>--}}
                                </div>
                            </form>
                        </div>
                        <div class="tours__hamburger trigger-js">
                            <svg fill="currentColor" width="24" height="24" viewBox="0 0 24 24"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 18.0001H14V16.0001H10V18.0001ZM3 6.00006V8.00006H21V6.00006H3ZM6 13.0001H18V11.0001H6V13.0001Z"></path>
                            </svg>
                            <p>фильтры</p></div>
                    </div>
                </div>
                <div class="col-lg-9" id="objects_list"
                     data-url="{{ route('home.tours.list',['locale'=> $currentLocale]) }}">
                    <div class="row">

                            @include('frontend.tours.list')

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="/app/js/modules/hotel.js"></script>
@endpush