@extends('frontend.partials.parts')

@section('content')
    <div class="container" style="margin-bottom: 4rem;">
        @include('frontend.partials.breadcrumps')
        @if(isset($page))
        <div class="article-boxed">
            <div class="article__title">
                <h1>{{$page->getTranslation('title',$currentLocale)}}</h1>
            </div>
            {!! $page->getTranslation('content',$currentLocale) !!}
        </div>
        @endif

    </div>
@endsection