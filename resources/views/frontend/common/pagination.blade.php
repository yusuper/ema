

@if ($paginator->hasPages())
     <ul class="pagination paginator text-right">

        @if ($paginator->onFirstPage())

             <li class="paginator__item -disabled">
                 <a href="{{ $paginator->previousPageUrl() }}" class="paginator__link" rel="prev">
                     <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                         <path d="M15.41 7.41L14 6L8 12L14 18L15.41 16.59L10.83 12L15.41 7.41Z" />
                     </svg>
                 </a>
             </li>
        @else


             <li class="paginator__item">
                 <a href="{{ $paginator->previousPageUrl() }}" class="paginator__link" rel="prev">
                     <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                         <path d="M15.41 7.41L14 6L8 12L14 18L15.41 16.59L10.83 12L15.41 7.41Z" />
                     </svg>
                 </a>
             </li>
        @endif


        @foreach ($elements as $element)

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="paginator__item -active"><a href="#" onclick="return false" class="paginator__link">{{ $page }}</a>
                        </li>
                    @else
                        <li class="paginator__item"><a href="{{ $url }}" class="paginator__link">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach


        @if ($paginator->hasMorePages())

            <li class="paginator__item">
                <a href="{{ $paginator->nextPageUrl() }}" class="paginator__link" rel="next">
                    <svg width="24" height="24" viewBox="0 0 18 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.49988 4.5L6.44238 5.5575L9.87738 9L6.44238 12.4425L7.49988 13.5L11.9999 9L7.49988 4.5Z" />
                    </svg>
                </a>
            </li>

            @else
                <li class="paginator__item -disabled">
                    <a href="#" class="paginator__link" onclick="return false" rel="next">
                        <svg width="24" height="24" viewBox="0 0 18 18" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.49988 4.5L6.44238 5.5575L9.87738 9L6.44238 12.4425L7.49988 13.5L11.9999 9L7.49988 4.5Z" />
                        </svg>
                    </a>
                </li>

        @endif
    </ul>
@endif







