@extends('frontend.layouts.masterr')

@section('content')
    <div class="container-fluid slider">
        <div class="row">
            <div class="col-md-12">
                <div class="index-slider">
                    <div class="fotorama" data-autoplay="6000" data-transition="crossfade" data-loop="true"
                         {{--data-nav="thumbs"--}} data-arrows="false" data-maxheight="600" data-fit="cover"
                         {{--data-nav="false"--}}
                         data-width="100%">
                        <a class="hght" href=""><img
                                    src="http://baysal.kg/wp-content/uploads/2015/12/karakol.jpg"></a>
                        <a class="hght" href=""><img
                                    src="http://azure.kg/wp-content/uploads/2017/06/sport_resort_azure_issykkui.jpg"></a>
                        <a class="hght" href=""><img
                                    src="http://krugozorro.com/wp-content/uploads/2018/12/post_5bf31c25f2661.jpg"></a>
                        {{--<img src="http://i.imgur.com/5tVJiI9.jpg">--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container main-page">
        <div class="row">
            {{--<div class="col-lg-1"></div>--}}
            <div class="col-lg-4 col-xs-8 card-margin">
                <div class="card">
                    <img class="card-img-top"
                         src="https://cdn.britannica.com/s:300x500/62/99562-050-0AA84E72.jpg"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title text-center">Интересные факты</h5>
                        <p class="card-texte text-center">Три четверти территории страны занимают горы. Самая знаменитая
                            - Пик Победы. Его называют самым грозным и недоступным семитысячником мира.</p>
                    </div>
                </div>
                <a href="#" style="text-decoration: none;"><h6 class="more">Подробнее</h6></a>
            </div>
            <div class="col-lg-4 col-xs-8 card-margin">
                <div class="card">
                    <img class="card-img-top"
                         src="http://www.president.kg/files/content_photo/manas.jpg"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title text-center">Эпос Манас</h5>
                        <p class="card-text text-center">Эпос "Манас" - золотая сокровищница народной мысли, в которой
                            отразился более чем трёх тысячелетний опыт истории и духовной жизни кыргызов.</p>
                    </div>
                </div>
                <a href="#" style="text-decoration: none;"><h6 class="more">Подробнее</h6></a>
            </div>
            <div class="col-lg-4 col-xs-8 card-margin">
                <div class="card">
                    <img class="card-img-top"
                         src="https://ich.unesco.org/img/photo/thumb/08364-BIG.jpg"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title text-center">Культура</h5>
                        <p class="card-text text-center">Культура Кыргызстана формировалась под сильным воздействием
                            кочевой жизни. Также, на неё повлияли культуры России, Персии и Турции, и все же
                            она осталась уникальной.</p>
                    </div>
                </div>
                <a href="#" style="text-decoration: none;"><h6 class="more">Подробнее</h6></a>
            </div>
        </div>
        <div class="card-deck">
        </div>
        <hr class="figurette-divider">
        <div class="row featurette">
            <div class="col-md-7">
                <a class="nounderline" href="{{route('home.tours',['locale'=>$currentLocale])}}"><h2 class="featurette-heading">Дайвинг на Иссык-Куле</h2></a>
                <p class="lead text-muted">Плавать под водой, погружаться в эту мощную необъятную, самовольную стихию мечтал еще Леонардо Да Винчи. Эру доступного дайвинга, который сегодня стал восприниматься как развлечение и спорт, открыл изобретатель акваланга Жак-Ив Кусто</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" data-src="" alt="" src="https://www.akchabar.kg/media/news/70edfc28-042f-4a91-ba28-362d8f504334.jpg" data-holder-rendered="true" style="width: 500px; height: 270px;">
            </div>
        </div>
        <hr class="figurette-divider">
        <div class="row featurette">
            <div class="col-md-7 order-md-2">
                <a class="nounderline" href="{{route('home.tours',['locale'=>$currentLocale])}}"><h2 class="featurette-heading">Горнолыжный туризм</h2></a>
                <p class="lead text-muted">
                    Горнолыжные базы Кыргызстана очень разнообразны. Здесь будет где покататься и профессионалам и новичкам. На многих базах существуют трассы различных сложностей.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" data-src="" alt="" src="https://gdb.rferl.org/7CB18912-8705-4A7B-AE8C-D834564F2808_cx0_cy8_cw0_w1023_r1_s.jpg" data-holder-rendered="true" style="width: 500px; height: 270px;">
            </div>
        </div>
        <hr class="figurette-divider">
        <div class="row featurette">
            <div class="col-md-7">
                <a class="nounderline" href="{{route('home.tours',['locale'=>$currentLocale])}}"><h2 class="featurette-heading">Конные туры по Кыргызстану</h2></a>
                <p class="lead text-muted">Маршрут пройдет через национальный парк Чон-Кемин. Ландшафт парка очень разнообразен: от плоскогорий до зеленых холмов, на склонах которых растут высокие хвойные ели и смешанные леса. На территории парка обитают животные, занесенные в Красную книгу, среди которых снежный барс и марал.</p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" data-src="" alt="" src="https://www.baibol.kg/img/album_gallery/thmb2_150085286136652.jpg" data-holder-rendered="true" style="width: 500px; height: 270px;">
            </div>
        </div>
    </div>


@endsection
@stack('scripts')

