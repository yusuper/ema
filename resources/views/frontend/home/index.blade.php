@extends('frontend.layouts.master')

@section('content')
    {{--<div class="parallaxSection"--}}
    {{--style="background-image:url(@if($mainPicture->mainImage){{$mainPicture->mainImage->original_file_name}}@else /frontend_html/dist/images/headerSlider/item1.jpg @endif)">--}}
    {{--<div class="parallaxSection__modalWindow wow fadeInUp">--}}
    {{--<div class="modal">--}}
    {{--@if($fact[0]->mainImage)--}}
    {{--<div class="modal__img wow fadeInUp"><img src="{{$fact[0]->mainImage->original_file_name}}"--}}
    {{--alt=""></div>--}}
    {{--@endif--}}
    {{--<div class="modal__question"><h4>{{$fact[0]->getTranslation('title',$currentLocale)}}</h4></div>--}}
    {{--<div class="modal__descCaption"><p>{{$fact[0]->getTranslation('short_desc',$currentLocale)}}</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="container">--}}
    {{--<div class="captionWrap"><h1 class="caption wow fadeInUp">@lang('common.quote')</h1></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="parallaxSection">
        <div class="parallaxSection__modalWindow wow fadeInUp">
            <div class="modal">
                <div class="modal__img wow fadeInUp">
                    @if($fact[0]->mainImage)
                        <img src="@if(isset($fact[0]->mainImage->cropped) && $fact[0]->mainImage->cropped)
                        {{$fact[0]->mainImage->cropped}}
                        @elseif($fact[0]->mainImage){{asset('storage/uploaded_images/256_' . $fact[0]->mainImage->getOriginal('original_file_name'))}}
                        @else /images/placeholder_small.png @endif" alt="">
                    @endif
                </div>
                <div class="modal__question"><h4>{{$fact[0]->getTranslation('title',$currentLocale)}}</h4></div>
                <div class="modal__descCaption"><p>{{$fact[0]->getTranslation('short_desc',$currentLocale)}}</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="captionWrap"><h1 class="caption wow fadeInUp"> @lang('common.quote')</h1></div>
        </div>
    </div>
    <div id="about" class="about" style="background-image:url(/frontend_html/dist/images/about/about.jpg)">
        <div class="container"></div>
    </div>
    <div class="intro" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div style="text-align: center;margin-right: 60px;" class="captionWrap"><h2
                                class="captionSecond wow fadeInLeft" data-wow-duration="1s">
                            @lang('common.about_us')</h2></div>
                    <div class="intro__desc wow fadeInLeft" data-wow-duration="1.7s">
                        <p>@if(isset($aboutUs->content) && $aboutUs->content != null){!!$aboutUs->getTranslation('content',$currentLocale)!!}@endif</p>
                    </div>
                </div>
                {{--<div class="col-lg-6">--}}
                {{--<div class="captionWrap">--}}
                {{--<h2 class="captionSecond wow fadeInLeft" data-wow-duration="1s">--}}
                {{--@lang('common.our_team')--}}
                {{--</h2>--}}
                {{--</div>--}}
                {{--<div class="intro__about wow fadeInRight">--}}
                {{--@if(isset($ourTeam) && $ourTeam->count()!=0)--}}
                {{--@php $i = 2 @endphp--}}
                {{--@foreach($ourTeam as $man)--}}
                {{--@if($i%2 != 0)--}}
                {{--<div class="intro__item">--}}
                {{--<div class="intro__itemDesc">--}}
                {{--<p>{{$man->getTranslation('short_desc',$currentLocale)}}</p></div>--}}
                {{--<div class="intro__itemAvatar">--}}
                {{--<div class="intro__itemImg"><img--}}
                {{--src="@if(isset($man->mainImage->cropped) && $man->mainImage->cropped)--}}
                {{--{{$man->mainImage->cropped}}--}}
                {{--@elseif($man->mainImage){{$man->mainImage->original_file_name}}--}}
                {{--@else /images/placeholder_small.png @endif" alt=""></div>--}}
                {{--<div class="intro__itemName">--}}
                {{--<h3>{{$man->getTranslation('title',$currentLocale)}}</h3></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--@else--}}
                {{--<div class="intro__item">--}}
                {{--<div class="intro__itemAvatar">--}}
                {{--<div class="intro__itemImg"><img--}}
                {{--src="@if(isset($man->mainImage->cropped) && $man->mainImage->cropped)--}}
                {{--{{$man->mainImage->cropped}}--}}
                {{--@elseif($man->mainImage){{$man->mainImage->original_file_name}}--}}
                {{--@else /images/placeholder_small.png @endif" alt=""></div>--}}
                {{--<div class="intro__itemName">--}}
                {{--<h3>{{$man->getTranslation('title',$currentLocale)}}</h3></div>--}}
                {{--</div>--}}
                {{--<div class="intro__itemDesc">--}}
                {{--<p>{{$man->getTranslation('short_desc',$currentLocale)}}</p></div>--}}
                {{--</div>--}}
                {{--@endif--}}
                {{--@php $i ++ @endphp--}}
                {{--@endforeach--}}
                {{--@endif--}}
                {{--<div class="intro__item">--}}
                {{--<div class="intro__itemAvatar">--}}
                {{--<div class="intro__itemImg"><img src="/images/Photo3.jpg" alt=""></div>--}}
                {{--<div class="intro__itemName"><h3>Азамат Сарайбеков</h3></div>--}}
                {{--</div>--}}
                {{--<div class="intro__itemDesc"><p>Ваш персональный гид Ваш персональный гид Ваш персональный--}}
                {{--гид</p></div>--}}
                {{--</div>--}}
                {{--<div class="intro__item">--}}
                {{--<div class="intro__itemDesc"><p>Ваш персональный гид Ваш персональный гид Ваш персональный--}}
                {{--гид</p></div>--}}
                {{--<div class="intro__itemAvatar">--}}
                {{--<div class="intro__itemImg"><img src="/images/Photo2.jpg" alt=""></div>--}}
                {{--<div class="intro__itemName"><h3>Эдиль Кадыров</h3></div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    </div>
    {{--<div class="intro" id="about">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-6">--}}
    {{--<div class="captionWrap"><h2 class="captionSecond wow fadeInLeft"--}}
    {{--data-wow-duration="1s">{{$aboutUs->getTranslation('title',$currentLocale)}}</h2>--}}
    {{--<h3 class="subCaption"></h3></div>--}}
    {{--<div class="intro__desc wow fadeInLeft" data-wow-duration="1.7s">--}}
    {{--<p>{!!$aboutUs->getTranslation('content',$currentLocale)!!}</p></div>--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-6">--}}
    {{--<div class="intro__desc wow fadeInLeft" data-wow-duration="1.7s">--}}
    {{--<p>{!!$aboutUs->getTranslation('content',$currentLocale)!!}</p></div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-6">--}}
    {{--<div class="intro__about wow fadeInRight">--}}
    {{--<div class="intro__item">--}}
    {{--<div class="intro__itemDesc"><p>Ваш персональный гид</p></div>--}}
    {{--<div class="intro__itemAvatar">--}}
    {{--<div class="intro__itemImg"><img src="/images/Photo1.jpg" alt=""></div>--}}
    {{--<div class="intro__itemName"><p>Мирбек Шакиров</p></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="intro__item">--}}
    {{--<div class="intro__itemAvatar">--}}
    {{--<div class="intro__itemImg"><img src="/images/Photo2.jpg" alt="">--}}
    {{--</div>--}}
    {{--<div class="intro__itemName"><p>Эдиль Кадыров</p></div>--}}
    {{--</div>--}}
    {{--<div class="intro__itemDesc"><p>Ваш персональный гид</p></div>--}}
    {{--</div>--}}
    {{--<div class="intro__item">--}}
    {{--<div class="intro__itemDesc"><p>Ваш персональный гид</p></div>--}}
    {{--<div class="intro__itemAvatar">--}}
    {{--<div class="intro__itemImg"><img src="/images/Photo3.jpg" alt=""></div>--}}
    {{--<div class="intro__itemName"><p>Азамат Сарайбеков</p></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="kind"
         style="background-image:url(@if($secondPicture->mainImage){{$secondPicture->mainImage->original_file_name}}@else /frontend_html/dist/images/kind/bg.jpg @endif)">
        <div class="container">
            <div class="captionSection"><h2>@lang('tour.tours')</h2></div>
            <div class="swiper-container kind__slider kind-js">
                <div class="swiper-wrapper">
                    @foreach($tours as $tour)
                        <div class="swiper-slide kind__item wow fadeInDown" data-wow-duration="1.5s">
                            <div class="kind__itemSubItem">
                                <div class="kind__SubItemImg"><img
                                            src="@if(isset($tour->mainImage->cropped) && $tour->mainImage->cropped)
                                            {{$tour->mainImage->cropped}}
                                            @elseif($tour->mainImage){{$tour->mainImage->original_file_name}}
                                            @else /images/placeholder_small.png @endif"
                                            alt=""></div>
                                <div class="kind__SubItemDesc">
                                    <div class="kind__descSubCap">
                                        <h3>{{$tour->getTranslation('title',$currentLocale)}}</h3>
                                        @if(isset($tour->short_desc))

                                                @if(mb_strlen($tour->getTranslation('short_desc',$currentLocale)) <= 120)

                                                    <p style="font-size: 14px">{{$tour->getTranslation('short_desc',$currentLocale)}}</p>
                                                @else
                                                    <p style="font-size: 14px">{{mb_substr($tour->getTranslation('short_desc',$currentLocale),0,120).'...'}}</p>
                                                @endif
                                        @endif
                                    </div>
                                    <div class="kind__ofButton">
                                        <a href="{{route('home.tours.standard.item',['locale'=>$currentLocale,'slug'=>$tour->slug])}}">
                                            @lang('tour.standard_tour')
                                        </a>
                                        <a href="{{route('home.tours.private.item',['locale'=>$currentLocale,'slug'=>$tour->slug])}}">
                                            @lang('tour.private_tour')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
    <div class="facts">
        <div class="container">
            <div class="captionSection"><h2>@lang('common.facts')</h2></div>
            <div class="swiper-container facts__slider facts-js">
                <div class="swiper-wrapper">
                    @foreach($facts as $fact)
                        <div class="swiper-slide facts__item wow fadeInDown" data-wow-duration="1s">
                            <div class="facts__itemWrap"><h2
                                        class="facts__caption">{{$fact->getTranslation('title',$currentLocale)}}</h2>
                                <div class="facts__img"><img
                                            src="@if(isset($fact->mainImage->cropped) && $fact->mainImage->cropped)
                                            {{$fact->mainImage->cropped}}
                                            @elseif($fact->mainImage){{asset('storage/uploaded_images/256_' . $fact->mainImage->getOriginal('original_file_name'))}}
                                            @else /images/placeholder_small.png @endif"
                                            alt=""></div>
                                <p>{{$fact->getTranslation('short_desc',$currentLocale)}}</p></div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="swiper-button-nexta"><i class="fal fa-angle-right"></i></div>
            <div class="swiper-button-preva"><i class="fal fa-angle-left"></i></div>
        </div>
    </div>
    <div class="news">
        <div class="container">
            <div class="captionSection captionSection--dark wow fadeInDown"
                 style="visibility:visible;animation-name:fadeInDown"><h2>@lang('news.news')</h2></div>
            <div class="news__newsWrap">
                <div class="row">
                    @foreach($news as $item)
                        <div class="col-lg-6 col-sm-6">
                            <div class="news__item wow fadeInLeft" data-wow-duration="1s"><a
                                        href="{{route('home.news.show',['locale'=>'ru','slug'=>$item->slug])}}"
                                        class="news__img"><img
                                            src="@if(isset($item->mainImage->cropped) && $item->mainImage->cropped)
                                            {{$item->mainImage->cropped}}
                                            @elseif($item->mainImage){{asset('storage/uploaded_images/256_' . $item->mainImage->getOriginal('original_file_name'))}}
                                            @else /images/placeholder_small.png @endif" alt=""></a>
                                <div class="news__desc"><a
                                            href="{{route('home.news.show',['locale'=>'ru','slug'=>$item->slug])}}"
                                            class="news__caption">{{$item->getTranslation('title',$currentLocale)}}</a>
                                    <p>{{$item->getTranslation('short_content',$currentLocale)}}...</p><a
                                            class="news__more"
                                            href="{{route('home.news.show',['locale'=>'ru','slug'=>$item->slug])}}">@lang('common.more')
                                        ...</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <style>.parallaxSection::before {
            background-image: url(@if(isset($mainPicture->mainImage->cropped) && $mainPicture->mainImage->cropped)
                                            {{$mainPicture->mainImage->cropped}}@elseif($mainPicture->mainImage){{$mainPicture->mainImage->original_file_name}}@else        /frontend_html/dist/images/headerSlider/item1.jpg @endif)
               }</style>


@endsection