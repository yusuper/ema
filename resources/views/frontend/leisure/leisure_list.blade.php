{{--@if(isset($category))--}}
{{--<h5>{{$category->getTranslation('name',$currentLocale)}}</h5>--}}
{{--@endif--}}
<div class="row">
    @foreach($leisures as $leisure)
        <div class="col-md-4 tour-item">
            <div class="card-mb-4 box-shadow">
                <img class="card-img"
                     src="@if(isset($leisure->mainImage)){{$leisure->mainImage->original_file_name}}@else /images/placeholder_small.png @endif">
            </div>
            <div class="card-body">
                <h4>{{$leisure->getTranslation('title',$currentLocale)}}</h4>
                <p>{{$leisure->getTranslation('short_desc',$currentLocale)}}</p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <a href="{{route('home.leisure.category.item',['locale'=>$currentLocale,'slug'=>$leisure->slug])}}" class="more-link" style="text-decoration: none;">
                            <h6>@lang('common.more')</h6></a>
                    </div>
                    <p class="tour__price" style="margin-bottom: .5rem">
                        <span>@lang('common.price'):</span>{{$leisure->price}}
                    </p>
                </div>
            </div>
        </div>
    @endforeach
    @if(!$leisures->count())
            <div>
                <h5>Данных не найдено</h5>
            </div>
    @endif
</div>