@extends('frontend.partials.parts')

@section('content')
    <div class="container">
        @include('frontend.partials.breadcrumps')
        <div class="page__header">
            <h1 class="page__header-title">{{$pageItem}}</h1>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="leisure-wrapper">
                    @foreach($categories as $category)
                        <a href="{{route('home.leisure.category',['locale'=>$currentLocale,'categoryId'=>$category->id])}}"
                           class="handle-click" data-type="ajax-get" style="text-decoration: none"
                           data-container="body" data-block-elem="#leisureList">
                            <div class="leisure-cat-wrapper">
                                <div class="leisure">
                                    <h5>{{$category->getTranslation('name',$currentLocale)}}</h5>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-9">
                <div id="leisureList">
                    @include('frontend.leisure.leisure_list')
                </div>
            </div>
            {{--<div class="col-lg-9">--}}
            {{--<div class="content">--}}
            {{--@foreach($hotels as $hotel)--}}
            {{--<ul class="hotels">--}}
            {{--<li class="hotel__item">--}}
            {{--<div class="hotel__logo">--}}
            {{--<img style="width: 100%;" class="hotel__img" src="@if(isset($hotel->mainImage)){{$hotel->mainImage->original_file_name}}@else /images/placeholder_small.png @endif">--}}
            {{--</div>--}}
            {{--<div class="hotel_content">--}}
            {{--<a href="#" class="hotel__title">{{$hotel->getTranslation('title',$currentLocale)}}</a><br>--}}
            {{--<small class="text-muted">{{$hotel->getTranslation('short_desc',$currentLocale)}}</small><br>--}}
            {{--<a href="tel:{{$hotel->phone}}">{{$hotel->phone}}</a>--}}
            {{--<p>@lang('common.price'): {{$hotel->price}}</p>--}}
            {{--</div>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--@endforeach--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection