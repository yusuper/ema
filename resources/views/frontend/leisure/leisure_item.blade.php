@extends('frontend.partials.parts')

@section('content')
    <div class="container">
        @include('frontend.partials.breadcrumps')
        @if(isset($leisure))
            <div class="article-boxed" style="padding-bottom: 5rem">
                <div class="article__title">
                    <h1>{{$leisure->getTranslation('title',$currentLocale)}}</h1>
                </div>
                {!! $leisure->getTranslation('desc',$currentLocale) !!}
            </div>
        @endif

    </div>
@endsection