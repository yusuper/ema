@extends('frontend.layouts.master')

@section('content')
    <div class="news news--detail">
        <div class="container">
            <div class="breadCrumbs">
                <ul>
                    <li><a href="{{route('home',['locale'=>$currentLocale])}}">@lang('common.home')</a></li>
                    <li>@lang('news.news')</li>
                </ul>
            </div>
            <div class="captionSection captionSection--dark wow fadeInDown"
                 style="visibility:visible;animation-name:fadeInDown"><h2>@lang('news.news')</h2></div>
            <div class="news__newsWrap">
                <div class="row">
                    @foreach($news as $item)
                        <div class="col-lg-6 col-sm-6">
                            <div class="news__item wow fadeInLeft" data-wow-duration="1s"><a
                                        href="{{route('home.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}"
                                        class="news__img"><img
                                            src="@if(isset($item->mainImage->cropped) && $item->mainImage->cropped)
                                            {{$item->mainImage->cropped}}
                                            @elseif($item->mainImage){{$item->mainImage->original_file_name}}
                                            @else /images/placeholder_small.png @endif" alt=""></a>
                                <div class="news__desc"><a
                                            href="{{route('home.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}"
                                            class="news__caption">{{$item->getTranslation('title',$currentLocale)}}</a>
                                    <p>{{$item->getTranslation('short_content',$currentLocale)}}...</p>
                                    <div class="news__bottomWrap"><p
                                                class="news__date">{{date('d-m-Y',strtotime($item->created_at))}}</p><a
                                                class="news__more"
                                                href="{{route('home.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}">@lang('common.more')...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="pagination">
                <div class="pagination__links">
                    @if(!$news->onFirstPage())
                        <a class="pagination__link--prev pagination__link" href="{{$news->previousPageUrl()}}"><i
                                    class="flabIcon flabio-left-open"></i>@lang('common.prev')</a>
                        <a class="pagination__link"
                           href="{{$news->previousPageUrl()}}">{{$news->currentPage() - 1}}</a>
                    @endif


                        <a class="pagination__link pagination__link--current"
                           href="#">{{$news->currentPage()}}</a>


                    {{--<span class="pagination__link">2</span> <a class="pagination__link"
                                                               href="#">3</a>--}}
                    @if($news->hasMorePages())
                            <a class="pagination__link"
                               href="{{$news->nextPageUrl()}}">{{$news->currentPage()+1}}</a>
                        <a class="pagination__link--next pagination__link" href="{{$news->nextPageUrl()}}">@lang('common.next')
                            <i class="flabIcon flabio-right-open"></i></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection