<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>project</title>
    <link rel="stylesheet" href="/frontend_html/dist/styles/vendor.css">
    <link rel="stylesheet" href="/frontend_html/dist/styles/main.css">
    <script src="/frontend_html/dist/scripts/vendor/modernizr.js"></script>
</head>
<body><!--[if IE]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
<div class="mainWrap">
    <div class="contentWrap">
        <div class="hamburger"><span class="hamburger__line"></span> <span class="hamburger__line"></span> <span
                    class="hamburger__line"></span></div>
        <div class="header wow fadeInDown">
            <div class="container">
                <div class="row flexWrap">
                    <div class="col-lg-2 col-sm-9 col-8">
                        <div class="header__logo"><a href="#"><img src="/frontend_html/dist/images/logo.png" alt=""></a></div>
                    </div>
                    <div class="col-lg-2 col-sm-3 order-lg-2 col-4">
                        <div class="header__userPanel">
                            <div class="header__feedBack"><a data-fancybox="" data-src="#feedBack" href="#"
                                                             href="javascript:;"><i class="fal fa-envelope"></i></a>
                            </div>
                            <select>
                                <option>KG</option>
                                <option>RU</option>
                                <option>EN</option>
                            </select></div>
                    </div>
                    <div class="col-lg-8 order-lg-1">
                        <div class="header__nav mnu-js">
                            <ul>
                                <li class="header__item header__item--active"><a data-scroll class="header__link"
                                                                                 href="#">Главная</a></li>
                                <li class="header__item"><a data-scroll class="header__link" href="#">О нас</a></li>
                                <li class="header__item"><a data-scroll class="header__link" href="#">Туры</a></li>
                                <li class="header__item"><a data-scroll class="header__link" href="#">Отели</a></li>
                                <li class="header__item"><a data-scroll class="header__link" href="#">Досуг</a></li>
                                <li class="header__item"><a data-scroll class="header__link" href="#">Контакты</a>
                                    <ul class="header__social">
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                            <form class="header__search"><input placeholder="Поиск ..." class="header__searchInput"
                                                                type="text">
                                <button class="header__searchSubmit" type="submit"><i class="fal fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="news news--detail">
            <div class="container">
                <div class="news__detailImg"><img src="/frontend_html/dist/images/about/about.jpg" alt=""></div>
                <h2 class="news__dertilCaption">lorem</h2></div>
        </div>
    </div>
</div>
<div class="feedBackWrap" id="feedBack">
    <div class="feedBack">
        <div class="feedBack__logo"><img src="/frontend_html/dist/images/logo.png" alt=""></div>
        <h2 class="feedBack__caption"></h2>
        <form action="" class="feedBack__form"><input placeholder="Имя" class="feedBack__input" type="text" name="Имя">
            <input placeholder="E-mail" class="feedBack__input" type="text" name="Email"> <input placeholder="Телефон"
                                                                                                 class="feedBack__input"
                                                                                                 type="text"
                                                                                                 name="телефон">
            <textarea placeholder="Сообщение" class="feedBack__textarea" name=""></textarea>
            <button type="submit">Отправить</button>
        </form>
    </div>
</div>
<script src="/frontend_html/dist/scripts/vendor.js"></script>
<script src="/frontend_html/dist/scripts/main.js"></script>
</body>
</html>