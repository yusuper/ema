@extends('frontend.layouts.master')

@section('content')

    <div class="news news--detail">
        <div class="container" style="padding-bottom: 4rem">
            {{--<div class="news__detailImg"><img src="/frontend_html/dist/images/about/about.jpg" alt=""></div>--}}
            <h1 class="news__dertilCaption">{{$newsItem->getTranslation('title',$currentLocale)}}</h1>
            <p>{!! $newsItem->getTranslation('long_content',$currentLocale) !!}</p>
            {{--<div class="fb-share-button"--}}
            {{--data-href="{{$currentUrl}}"--}}
            {{--data-layout="button_count">--}}
            {{--</div>--}}
            <div class="likely">
                <div data-image="@if(isset($newsItem->mainImage->cropped) && $newsItem->mainImage->cropped)
                {{$newsItem->mainImage->cropped}}
                @elseif($newsItem->mainImage){{$newsItem->mainImage->original_file_name}}
                @else /images/placeholder_small.png @endif" data-url="{{$currentUrl}}" class="facebook">Share
                </div>
                <div data-url="{{$currentUrl}}" class="twitter">Tweet</div>
                {{--<div class="gplus">+1</div>--}}
                <div data-url="{{$currentUrl}}"
                     data-image="@if(isset($newsItem->mainImage->cropped) && $newsItem->mainImage->cropped)
                     {{$newsItem->mainImage->cropped}}
                     @elseif($newsItem->mainImage){{$newsItem->mainImage->original_file_name}}
                     @else /images/placeholder_small.png @endif" class="vkontakte">Share
                </div>
                {{--<div class="pinterest">Pin</div>--}}
                {{--<div class="odnoklassniki">Like</div>--}}
                <div data-url="{{$currentUrl}}" class="telegram">Send</div>
                {{--<div class="linkedin">Share</div>--}}
                <div data-url="{{$currentUrl}}" class="whatsapp">Send</div>
            </div>
        </div>
        <div class="lastNews">
            <div class="container">
                <div><h2 class="news__dertilCaption">@lang('common.interesting_news')</h2></div>
                <div class="swiper-container simelarNews-js">
                    <div class="swiper-wrapper">
                        @foreach($randomNews as $item)
                            <div class="swiper-slide slider__item">
                                <div class="slider__itemImg"><a href="#"><img
                                                src="@if(isset($item->mainImage->cropped) && $item->mainImage->cropped)
                                                {{$item->mainImage->cropped}}
                                                @elseif($item->mainImage){{$item->mainImage->original_file_name}}
                                                @else /images/placeholder_small.png @endif" alt=""></a></div>
                                <h4>
                                    <a href="{{route('home.news.show',['locale'=>'ru','slug'=>$item->slug])}}">{{$item->getTranslation('title',$currentLocale)}}
                                        .</a></h4>
                                <div class="slider__itemdesc">
                                    <p>{{$item->getTranslation('short_content',$currentLocale)}}</p></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-button-nextNews"><i class="fal fa-angle-right"></i></div>
                <div class="swiper-button-prevNews"><i class="fal fa-angle-left"></i></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    {{--<script>(function (d, s, id) {--}}
    {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
    {{--if (d.getElementById(id)) return;--}}
    {{--js = d.createElement(s);--}}
    {{--js.id = id;--}}
    {{--js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";--}}
    {{--fjs.parentNode.insertBefore(js, fjs);--}}
    {{--}(document, 'script', 'facebook-jssdk'));--}}
    {{--</script>--}}
@endpush