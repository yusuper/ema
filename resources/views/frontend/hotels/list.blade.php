<div class="content">
    @foreach($hotels as $hotel)
        <ul class="hotels">
            <li class="hotel__item">
                <div class="hotel__logo">
                    <img style="width: 100%;" class="hotel__img"
                         src="@if(isset($hotel->mainImage)){{$hotel->mainImage->original_file_name}}@else /images/placeholder_small.png @endif">
                </div>
                <div class="hotel_content">
                    <a href="#"
                       class="hotel__title">{{$hotel->getTranslation('title',$currentLocale)}}</a><br>
                    <small class="text-muted">{{$hotel->getTranslation('short_desc',$currentLocale)}}</small>
                    <br>
                    <a href="tel:{{$hotel->phone}}">{{$hotel->phone}}</a>
                    <p>@lang('common.price'): {{$hotel->price}}</p>
                </div>
            </li>
        </ul>
    @endforeach
</div>
<div id="ajaxPagination">
    {!! $pagination !!}
</div>