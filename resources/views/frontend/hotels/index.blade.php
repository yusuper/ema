@extends('frontend.partials.parts')

@section('content')
    <div class="container">
        @include('frontend.partials.breadcrumps')
        <div class="page__header">
            <h1 class="page__header-title">{{$pageItem}}</h1>
        </div>
        <div class="row">
            <div class="col-lg-3 order-lg-last">
                <div class="filters">
                    <form action="{{ route('home.hotels.list',['locale' => $currentLocale]) }}" id="filterForm"
                          method="get">
                        <div class="filter__title">@lang('common.search_filters')</div>
                        <div class="form-group">
                            <input id="filterSearch" type="search" class="form-control filters__search"
                                   placeholder="@lang('common.search_by_name')" name="filterSearch">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-9" id="objects_list"
                 data-url="{{ route('home.hotels.list',['locale'=> $currentLocale]) }}">

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="/app/js/modules/hotel.js"></script>
@endpush