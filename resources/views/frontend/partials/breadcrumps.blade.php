<nav aria-label="breadcrumb">
    <ol class="breadcrumb" style="background-color: white">
        <li class="breadcrumb-item"><a href="{{route('home',['locale'=>$currentLocale])}}"><i class="fa fa-home"></i></a></li>
        <li class="breadcrumb-item active" aria-current="page">
            @if(isset($page)){{$page->getTranslation('title',$currentLocale)}}
            @elseif(isset($pageItem)) {{$pageItem}} @endif</li>
    </ol>
</nav>
