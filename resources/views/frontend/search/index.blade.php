@extends('frontend.layouts.master')

@section('content')
    <div class="news news--detail">
        <div class="container">
            <div class="breadCrumbs">
                <ul>
                    <li><a href="{{route('home',['locale'=>$currentLocale])}}">Главная</a></li>
                    <li>Поиск</li>
                </ul>
            </div>
            <div class="captionSection captionSection--dark wow fadeInDown"
                 style="visibility:visible;animation-name:fadeInDown"><h2>Результаты поиска по "{{$searchName}}"</h2>
            </div>
            <div class="news__newsWrap">
                <div class="row" style="display: block;">
                    @if(isset($result) && $result->count() != 0)
                        @foreach($result as $key=>$item)
                            {{--                            {{dd($item->getTable())}}--}}
                            @if($item->getTable() == 'news')
                                <a href="{{route('home.news.show',['locale'=>$currentLocale,'slug'=>$item->slug])}}">
                                    <h3 style="display: inline;">{!! $item->getTranslation('title',$currentLocale) !!}</h3>
                                    <p style="display: inline;">(новость)</p>
                                </a><br>
                            @elseif($item->getTable() == 'tours')
                                @if($item->category_id == 1)
                                    <a href="{{route('home.tours.item',['locale'=>$currentLocale,'slug'=>$item->slug])}}">
                                        <h3 style="display: inline;">{!! $item->getTranslation('title',$currentLocale) !!}</h3>
                                        <p style="display: inline;">(VIP тур)</p>
                                    </a><br>
                                @elseif($item->category_id == 2)
                                    <a href="{{route('home.tours.item',['locale'=>$currentLocale,'slug'=>$item->slug])}}">
                                        <h3 style="display: inline;">{!! $item->getTranslation('title',$currentLocale) !!}</h3>
                                        <p style="display: inline;">(Стандартный тур)</p>
                                    </a><br>
                                @endif

                            @elseif($item->getTable() == 'galleries')
                                @if($item->is_video == 1)
                                    <a href="{{route('home.gallery.video.item',['locale'=>$currentLocale,'slug'=>$item->slug])}}">
                                        <h3 style="display: inline;">{!! $item->getTranslation('title',$currentLocale) !!}</h3>
                                        <p style="display: inline;">(Видео)</p>
                                    </a><br>
                                @else
                                    <a href="{{route('image-gallery',['locale'=>$currentLocale])}}">
                                        <h3 style="display: inline;">{!! $item->getTranslation('title',$currentLocale) !!}</h3>
                                        <p style="display: inline;">(Фотогалерея)</p>
                                    </a><br>
                                @endif
                            @endif
                        @endforeach
                    @else
                        <a href="#"><h1>Извините, ничего не найдено</h1></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection