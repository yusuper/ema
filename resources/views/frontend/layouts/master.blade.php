<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="description" @if(isset($metaDescription)) content="{{$metaDescription}}" @else content="" @endif >
    <meta name="keywords" @if(isset($metaKeywords)) content="{{$metaKeywords}}" @else content="" @endif >
    <meta name="viewport" content="width=device-width,initial-scale=1">
    @if(isset($newsItem))
        <meta property="og:title" content="@if(isset($newsItem->title)){{ $newsItem->getTranslation('title',$currentLocale) }}@endif" />
        <meta property="og:url" content="{{  Request::url()  }}" />
        <meta property="og:description" content="@if(isset($newsItem->short_content)){{ strip_tags($newsItem->getTranslation('short_content',$currentLocale)) }}@endif" />
    <meta property="og:image" content="@if(isset($newsItem->mainImage)){{ $newsItem->mainImage->original_file_name }}@endif"/>
    @endif
    <title>@if(isset($metaTitle)){{$metaTitle}}@endif</title>
    <link rel="shortcut icon" href="/frontend_html/dist/images/logo3.png" type="image/x-icon">
    <link rel="stylesheet" href="/frontend_html/dist/styles/vendor.css">
    <link rel="stylesheet" href="/frontend_html/dist/styles/main.css">
    <link href="/app/js/vendors/Likely-master/release/likely.css" rel="stylesheet">
    <script src="/frontend_html/dist/scripts/vendor/modernizr.js"></script>
</head>
<body><!--[if IE]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
<!-- preloader -->
{{--<div class="preloader">--}}
    {{--<div class="loader"></div>--}}
{{--</div>--}}
<!-- preloader end -->

<div class="mainWrap">
    <div class="contentWrap">
        <div class="hamburger"><span class="hamburger__line"></span> <span class="hamburger__line"></span> <span
                    class="hamburger__line"></span></div>
        <div class="header wow fadeInDown">
            <div class="container">
                <div class="row flexWrap">
                    <div class="col-lg-2 col-sm-9 col-8">
                        <div class="header__logo"><a href="{{route('home',['locale'=>$currentLocale])}}"><img src="/frontend_html/dist/images/logo3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-3 order-lg-2 col-4">
                        <div class="header__userPanel">
                            <div class="header__feedBack"><a data-fancybox="" data-src="#feedBack" href="#"
                                                             href="javascript:;"><i class="fal fa-envelope"></i></a>
                            </div>
                            <div class="select">
                                <div class="selected"><p>{{strtoupper($currentLocale)}} <i class="fal fa-angle-down"></i></p></div>
                                <ul class="selectList">
                                    <li><a href="{{route('changeLocale',['locale'=>'ru'])}}">RU</a></li>
                                    <li><a href="{{route('changeLocale',['locale'=>'en'])}}">EN</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 order-lg-1">
                        <div class="header__nav mnu-js">
                            <ul class="header__firstLevel">
                                <li class="header__item header__item--active"><a class="header__link"
                                                                                 href="{{route('home',['locale'=>$currentLocale])}}">@lang('common.home')</a></li>
                                @foreach($menu as $item)
                                    @if(!$item->parent_id)
                                        <li class="header__item"><a class="header__link"
                                                                    @if($item->slug =='kontakty'/* || $item->slug == 'o-nas'*/) data-scroll
                                                                    @endif
                                                                    href="@if($item->slug =! 'tury' || $item->slug != 'galereya'){{$item->getTranslation('url',$currentLocale)}} @else # @endif">{{$item->getTranslation('name',$currentLocale)}}</a>
                                            @if($item->children->count())
                                                <i class="fal fa-angle-right triggerSubmenu-js"></i>
                                            @endif
                                            <ul class="header__dropDownMenu subMnu-js">
                                                @foreach($children as $child)
                                                    @if($child->parent_id == $item->id)
                                                        <li>
                                                            <a href="{{$child->getTranslation('url',$currentLocale)}}">{{$child->getTranslation('name',$currentLocale)}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach
                                <ul class="header__social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a target="_blank" href="https://instagram.com/wanderertour?igshid=vh0oc7tyhe2u"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                </ul>
                                </li>
                            </ul>
                            <form class="header__search" action="{{route('home.search',['locale'=>$currentLocale])}}">
                                <input placeholder="@lang('common.search') ..." class="header__searchInput" type="text" name="search">
                                <button class="header__searchSubmit" type="submit"><i class="fal fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @yield('content')


        <div class="contacts" id="contacts">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="contacts__item"><h2 class="contacts__caption">@lang('common.our_contact')</h2>
                            <div class="contacts__list">
                                <ul>
                                    {{--<li><i class="fas fa-map-marker-alt"></i> Lorem ipsum dolor sit.</li>--}}
                                    <li><i class="fal fa-envelope"></i><a
                                                href="mailto:wanderertourkg@gmail.com">wanderertourkg@gmail.com</a>
                                    </li>
                                    {{--<li><i class="fal fa-mobile"></i><a href="tel:+996 706 07 04 90">+996 706 07 04 90</a>--}}
                                    <li><i class="fal fa-mobile"></i><a href="tel:+996 550 51 99 00">+996 550 51 99 00</a>
                                    </li>
                                </ul>
                                {{--<h2 class="contacts__caption">МЫ В СОЦ. СЕТЯХ</h2>--}}
                                <ul class="contacts__social" style="margin-top: 30px;margin-left: -30px;">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a target="_blank" href="https://instagram.com/wanderertour?igshid=vh0oc7tyhe2u"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-6">
                        <div class="contacts__item"><h2 class="contacts__caption">@lang('common.write_us')</h2>
                            <form class="contacts__form">
                                <div class="row">
                                    <div class="col-lg-6"><input type="text" class="contacts__input"
                                                                 placeholder="@lang('common.your_name')">
                                        <input type="text" class="contacts__input" placeholder="@lang('common.your_phone_number')">
                                        <input type="text" class="contacts__input" placeholder="@lang('common.your_email')"></div>
                                    <div class="col-lg-6"><textarea class="contacts__textarea"
                                                                    placeholder="@lang('common.message')"></textarea>
                                        <button type="submit">@lang('common.send')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="feedBackWrap" id="feedBack">
        <div class="feedBack">
            <div class="feedBack__logo"><img src="/frontend_html/dist/images/logo3.png" alt=""></div>
            <h2 class="feedBack__caption"></h2>
            <form action="" class="feedBack__form"><input placeholder="Имя" class="feedBack__input" type="text"
                                                          name="Имя">
                <input placeholder="E-mail" class="feedBack__input" type="text" name="Email"> <input
                        placeholder="Телефон"
                        class="feedBack__input"
                        type="text"
                        name="телефон">
                <textarea placeholder="Сообщение" class="feedBack__textarea" name=""></textarea>
                <button type="submit">Отправить</button>
            </form>
        </div>
    </div>
    <!-- footer -->
</div>
<!-- footer end -->
<script src="/frontend_html/dist/scripts/vendor.js"></script>
<script src="/frontend_html/dist/scripts/main.js"></script>
<script src="/app/js/vendors/Likely-master/release/likely.js" type="text/javascript"></script>
<script src="/app/js/common/frontend.js"></script>
@stack('scripts')
</body>
</html>
