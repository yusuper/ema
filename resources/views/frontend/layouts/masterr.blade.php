<!doctype html>


<html lang="{{ $currentLocale }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <link href="/app/css/common/app.css" rel="stylesheet">
    <link href="/app/css/common/responsive.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    {{--<title>{{ MetaTag::get('title') }}</title>--}}
    {{--{!! MetaTag::tag('description') !!}--}}
    {{--{!! MetaTag::tag('keywords') !!}--}}
    {{--{!! MetaTag::tag('image') !!}--}}
    {{--{!! MetaTag::openGraph() !!}--}}
    {{--{!! MetaTag::twitterCard() !!}--}}
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}

    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

    {{--<!-- favicon--}}
    {{--============================================ -->--}}
    {{--<link rel="shortcut icon" type="image/x-icon" href="/app/images/favicon.ico">--}}

    {{--<!-- Google Fonts--}}
    {{--============================================ -->--}}
    {{--<link href='https://fonts.googleapis.com/css?family=Raleway:400,600' rel='stylesheet' type='text/css'>--}}
    {{--<link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>--}}

    {{--<!-- Bootstrap CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/bootstrap.min.css">--}}
    {{--<!-- Font awesome CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/font-awesome.min.css">--}}
    {{--<!-- owl.carousel CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/owl.carousel.css">--}}
    {{--<link rel="stylesheet" href="/electronics/css/owl.theme.css">--}}
    {{--<link rel="stylesheet" href="/electronics/css/owl.transitions.css">--}}
    {{--<!-- nivo slider CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/nivo-slider.css" type="text/css"/>--}}
    {{--<!-- meanmenu CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/meanmenu.min.css">--}}
    {{--<!-- jquery-ui CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/jquery-ui.css">--}}
    {{--<!-- animate CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/animate.css">--}}
    {{--<!-- main CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/main.css">--}}
    {{--<!-- style CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/style.css">--}}
    {{--<!-- responsive CSS--}}
    {{--============================================ -->--}}
    {{--<link rel="stylesheet" href="/electronics/css/responsive.css">--}}
    {{--<link rel="stylesheet" href="/frontend/css/app.css">--}}
    {{--<link rel="stylesheet" href="/frontend/ilya-birman-likely-2.3.1/likely.css">--}}
    {{--<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css"/>--}}
</head>
<body>
<div id="navbar" style="/*display: none;*/width:100%;">
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                        aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        @php $i=0; @endphp
                        @foreach($menu as $item)
                            {{--@if($i==2)--}}
                            <li class="nav-item active">
                                <a class="nav-link"
                                   href="{{$item->getTranslation('url',$currentLocale)}}">{{$item->getTranslation('name',$currentLocale)}}</a>
                            </li>
                            @php $i++; @endphp
                        @endforeach
                    </ul>
                </div>
                <div class="navbar-phone">
                    <ul>
                        <li class="nav-item pull-right navbar-numbers">
                            <a href="tel:+971 525 293 898"><h6 class="pull-right">+971 525 293 898</h6></a>
                            <a href="tel:+996 312 65 36 85"><h6 class="pull-right">+996 312 65 36 85</h6></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="top">
        <ul id="top-menu">
            @php $i=0; @endphp
            @foreach($menu as $item)
                <li class="header-item"><a class="header-item-link"
                                           href="{{$item->getTranslation('url',$currentLocale)}}">{{$item->getTranslation('name',$currentLocale)}}</a>
                </li>
                @if($i == 1)
                    <img class="header-image"
                         src="https://concept.kg/media/one_stop_travel_shop/%D0%B8%D0%BA%D0%BE%D0%BD%D0%BA%D0%B0_%D1%82%D1%80%D1%8D%D0%B2%D0%B5%D0%BB_%D0%BB%D0%BE%D0%BA%D0%B0%D0%BB.png"
                         width="100px">
                @endif
                @php $i++; @endphp
            @endforeach
        </ul>
        <div class="phone-numbers">
            {{--<ul>--}}
                {{--<li>--}}
                    <h4 class="number-item pull-right">
                        <span>+971 525 293 898</span>
                        <span>+996 312 65 36 85</span>
                    </h4>
                {{--</li>--}}
            {{--</ul>--}}
        </div>
    {{--</div>--}}
</div>
@yield('content')
<footer class="page-footer font-small blue pt-4">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">

                <!-- Content -->
                <h5 class="text-uppercase">Footer Content</h5>
                <p>Here you can use rows and columns here to organize your footer content.</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 mb-3">

                <!-- Links -->
                <h5 class="text-uppercase">Links</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Link 1</a>
                    </li>
                    <li>
                        <a href="#!">Link 2</a>
                    </li>
                    <li>
                        <a href="#!">Link 3</a>
                    </li>
                    <li>
                        <a href="#!">Link 4</a>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 mb-3">

                <!-- Links -->
                <h5 class="text-uppercase">Links</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Link 1</a>
                    </li>
                    <li>
                        <a href="#!">Link 2</a>
                    </li>
                    <li>
                        <a href="#!">Link 3</a>
                    </li>
                    <li>
                        <a href="#!">Link 4</a>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© EMA local
        <a href="https://mdbootstrap.com/education/bootstrap/"> emaaa.pp.ua</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<script src="/app/js/core.js"></script>
<script src="/app/js/common/app.js"></script>
@stack('scripts')
</body>
</html>