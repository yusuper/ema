@extends('frontend.layouts.master')

@section('content')

    <div class="news news--detail">
        <div class="container" style="padding-bottom: 4rem">
            {{--<div class="news__detailImg"><img src="/frontend_html/dist/images/about/about.jpg" alt=""></div>--}}
            <h1 class="news__dertilCaption">{{$album->getTranslation('title',$currentLocale)}}</h1>
                        <p>{!! $album->getTranslation('desc',$currentLocale) !!}</p>
            @if(isset($album->mainImage))
                <div class="col-lg-12 col-sm-12">
                    <a class="gallery__item" data-fancybox="gallery1{{--{{$i}}--}}"
                       href="{{$album->mainImage->original_file_name}}">
                        <div class="gallery__albumName">
                            {{--<h3>{{$album->getTranslation('short_desc',$currentLocale)}}</h3>--}}
                            <h3><i style="font-size: 24px" class="fas fa-search-plus"></i></h3>
                        </div>
                        <img src="@if(isset($album->mainImage->cropped) && $album->mainImage->cropped)
                        {{$album->mainImage->cropped}}
                        @elseif($album->mainImage){{$album->mainImage->original_file_name}}
                        @else /images/placeholder_small.png @endif">
                    </a>
                    {{--<a href="{{$album->mainImage->original_file_name}}" data-fancybox="gallery1--}}{{--{{$i}}--}}{{--"--}}
                       {{--class="gallery__itemCaption">--}}
                        {{--{{$album->getTranslation('title',$currentLocale)}}--}}
                    {{--</a>--}}
                    @php $i = 0 @endphp
                    @foreach($album->media as $media)
                        @if($i>=0)
                        <a class="gallery__hiddenItem" data-fancybox="gallery1{{--{{$i}}--}}"
                           data-caption="{{$media->desc}} {{$media->desc_2}}"
                           href="{{$media->original_file_name}}"><img
                                    src="{{$media->original_file_name}}">
                        </a>
                        @endif
                        @php $i+=1 @endphp
                    @endforeach
                </div>
            @endif

            {{--<div class="fb-share-button"--}}
            {{--data-href="{{$currentUrl}}"--}}
            {{--data-layout="button_count">--}}
            {{--</div>--}}
            <div class="likely">
                <div data-image="@if(isset($album->mainImage->cropped) && $album->mainImage->cropped)
                {{$album->mainImage->cropped}}
                @elseif($album->mainImage){{$album->mainImage->original_file_name}}
                @else /images/placeholder_small.png @endif" data-url="{{$currentUrl}}" class="facebook">Share
                </div>
                <div data-url="{{$currentUrl}}" class="twitter">Tweet</div>
                {{--<div class="gplus">+1</div>--}}
                <div data-url="{{$currentUrl}}"
                     data-image="@if(isset($album->mainImage->cropped) && $album->mainImage->cropped)
                     {{$album->mainImage->cropped}}
                     @elseif($album->mainImage){{$album->mainImage->original_file_name}}
                     @else /images/placeholder_small.png @endif" class="vkontakte">Share
                </div>
                {{--<div class="pinterest">Pin</div>--}}
                {{--<div class="odnoklassniki">Like</div>--}}
                <div data-url="{{$currentUrl}}" class="telegram">Send</div>
                {{--<div class="linkedin">Share</div>--}}
                <div data-url="{{$currentUrl}}" class="whatsapp">Send</div>
            </div>
        </div>
        <div class="lastNews">
            <div class="container">
                <div><h2 class="news__dertilCaption">Другие альбомы</h2></div>
                <div class="swiper-container simelarNews-js">
                    <div class="swiper-wrapper">
                        @foreach($randomGalleries as $gallery)
                            <div class="swiper-slide slider__item">
                                <div class="slider__itemImg"><a
                                            href="{{route('home.gallery.image.inner',['locale'=>'ru','slug'=>$gallery->slug])}}">
                                        <img src="@if(isset($gallery->mainImage->cropped) && $gallery->mainImage->cropped)
                                        {{$gallery->mainImage->cropped}}
                                        @elseif($gallery->mainImage){{$gallery->mainImage->original_file_name}}
                                        @else /images/placeholder_small.png @endif" alt=""></a></div>
                                <h4>
                                    <a href="{{route('home.gallery.image.inner',['locale'=>'ru','slug'=>$gallery->slug])}}">
                                        {{$gallery->getTranslation('title',$currentLocale)}}
                                    </a>
                                </h4>
                                <div class="slider__itemdesc">
                                    <p>{!! $gallery->getTranslation('short_desc',$currentLocale) !!}</p></div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-button-nextNews"><i class="fal fa-angle-right"></i></div>
                <div class="swiper-button-prevNews"><i class="fal fa-angle-left"></i></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    {{--<script>(function (d, s, id) {--}}
    {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
    {{--if (d.getElementById(id)) return;--}}
    {{--js = d.createElement(s);--}}
    {{--js.id = id;--}}
    {{--js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";--}}
    {{--fjs.parentNode.insertBefore(js, fjs);--}}
    {{--}(document, 'script', 'facebook-jssdk'));--}}
    {{--</script>--}}
@endpush