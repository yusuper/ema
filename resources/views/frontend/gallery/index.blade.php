@extends('frontend.layouts.master')

@section('content')
    <div class="gallery">
        <div class="container">
            <div class="breadCrumbs">
                <ul>
                    <li><a href="{{route('home',['locale'=>$currentLocale])}}">@lang('common.home')</a></li>
                    <li>{{$title}}</li>
                </ul>
            </div>
            <div class="captionSection captionSection--dark wow fadeInDown"
                 style="visibility:visible;animation-name:fadeInDown"><h2>{{$title}}</h2></div>
            <div class="gallery__wrap">
                @if(!isset($is_video))
                    <div class="row">
                        @php $i=1 @endphp
                        @foreach($albums as $album)
                            @if(isset($album->mainImage))
                                <div class="col-lg-4 col-sm-6">
                                    <a class="gallery__item"
                                       href="{{route('home.gallery.image.inner',['locale'=>$currentLocale,'slug'=>$album->slug])}}">
                                        <div class="gallery__albumName">
                                            {{--<h3>{{$album->getTranslation('short_desc',$currentLocale)}}</h3>--}}</div>
                                        <img src="@if(isset($album->mainImage->cropped) && $album->mainImage->cropped)
                                        {{$album->mainImage->cropped}}
                                        @elseif($album->mainImage){{$album->mainImage->original_file_name}}
                                        @else /images/placeholder_small.png @endif">
                                    </a>
                                    <a href="{{route('home.gallery.image.inner',['locale'=>$currentLocale,'slug'=>$album->slug])}}"
                                       class="gallery__itemCaption">
                                        {{$album->getTranslation('title',$currentLocale)}}
                                    </a>
                                    {{--@foreach($album->media as $media)--}}
                                        {{--<a class="gallery__hiddenItem" data-fancybox="gallery{{$i}}"--}}
                                           {{--data-caption="{{$media->desc}} {{$media->desc_2}}"--}}
                                           {{--href="{{$media->original_file_name}}"><img--}}
                                                    {{--src="{{$media->original_file_name}}">--}}
                                        {{--</a>--}}
                                    {{--@endforeach--}}
                                </div>
                            @endif
                            @php $i+=1 @endphp
                        @endforeach
                    </div>
                @else
                    <div class="row">
                        @foreach($albums as $album)
                            <div class="col-lg-4 col-md-6">
                                <a href="{{route('home.gallery.video.item',['locale'=>$currentLocale,'slug'=>$album->slug])}}"
                                   class="tours__item">
                                    <div class="tours__img"><img
                                                src="@if(isset($album->mainImage->cropped) && $album->mainImage->cropped)
                                                {{$album->mainImage->cropped}}
                                                @elseif($album->mainImage){{$album->mainImage->original_file_name}}
                                                @else /images/placeholder_small.png @endif" alt="">
                                        {{--<div class="tours__price"><p>@lang('common.price'): {{$album->price}}</p></div>--}}
                                    </div>
                                    <div class="tours__desc"><h2
                                                class="tours__caption">{{$album->getTranslation('title',$currentLocale)}}</h2>
                                        {{-- <p>{{$album->getTranslation('short_desc',$currentLocale)}}...</p>--}}</div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection