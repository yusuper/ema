@extends('frontend.layouts.master')

@section('content')
    <div class="news news--detail">
        <div class="container">
            <div class="breadCrumbs">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li>{{$page->getTranslation('title',$currentLocale)}}</li>
                </ul>
            </div>
            <h2 class="news__detailCaption">{{$page->getTranslation('title',$currentLocale)}}</h2>
            <p>{!! $page->getTranslation('content',$currentLocale) !!}</p></div>
    </div>
@endsection