<?php

return [
    'default_locale' => 'ru',
    'foreign_locales' => ['ru', 'en', 'kg'],
    'news_images_upload_path' => storage_path() . '/app/public/news_images',
    'media_upload_path' => storage_path() . '/app/public/uploaded_images',


    'locales' => [
        [
            'locale' => 'ru',
            'desc' => 'Русский'
        ],

        [
            'locale' => 'en',
            'desc' => 'Английский'
        ],

        [
            'locale' => 'kg',
            'desc' => 'Кыргызский'
        ]
    ],

    'menus' => [
        'top' => [
            'title' => 'Верхнее меню',
            'slug' => ''
        ],

        'footer' => [
            'title' => 'Футер',
            'slug' => ''
        ]
    ]
];