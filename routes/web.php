<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;




Route::get('admin/auth/login', 'Backend\AuthController@getLogin')->name('admin.get.login');
Route::post('admin/auth/login', 'Backend\AuthController@postLogin')->name('admin.post.login');
Route::get('admin/auth/logout', 'Backend\AuthController@logout')->name('admin.logout');


Route::group(['prefix' => 'admin', 'middleware' => 'adminMiddleware:admins'], function () {
    Route::get('/', 'Backend\HomeController@index')->name('admin.home');

    Route::group(['prefix' => 'users/admins/profile'], function () {
        Route::get('/', 'Backend\Users\Admins\AdminProfileController@profile')->name('admin.users.admins.profile');
        Route::post('update', 'Backend\Users\Admins\AdminProfileController@update')->name('admin.users.admins.profile.update');
    });


    Route::group(['prefix' => 'users/admins', 'middleware' => 'adminPermissionMiddleware:manage_admins'], function () {

        Route::get('/', 'Backend\Users\Admins\AdminController@index')->name('admin.users.admins');
        Route::get('get-list', 'Backend\Users\Admins\AdminController@getList')->name('admin.users.admins.list');
        Route::get('create', 'Backend\Users\Admins\AdminController@create')->name('admin.users.admins.create');
        Route::post('store', 'Backend\Users\Admins\AdminController@store')->name('admin.users.admins.store');
        Route::get('{userId}/edit', 'Backend\Users\Admins\AdminController@edit')->name('admin.users.admins.edit');
        Route::post('{userId}/update', 'Backend\Users\Admins\AdminController@update')->name('admin.users.admins.update');

        Route::group(['prefix' => 'roles'], function () {

            Route::get('/', 'Backend\Users\Admins\RoleController@index')->name('admin.users.admins.roles');
            Route::get('get-list', 'Backend\Users\Admins\RoleController@getList')->name('admin.users.admins.roles.list');
            Route::get('create', 'Backend\Users\Admins\RoleController@create')->name('admin.users.admins.roles.create');
            Route::post('store', 'Backend\Users\Admins\RoleController@store')->name('admin.users.admins.roles.store');
            Route::get('{roleId}/edit', 'Backend\Users\Admins\RoleController@edit')->name('admin.users.admins.roles.edit');
            Route::post('{roleId}/update', 'Backend\Users\Admins\RoleController@update')->name('admin.users.admins.roles.update');
        });
    });

    Route::group(['prefix' => 'categories'], function () {

        Route::get('list', 'Backend\CategoriesController@getCategoryList')->name('admin.categories.list');
        Route::get('create', 'Backend\CategoriesController@create')->name('admin.categories.create');
        Route::post('store', 'Backend\CategoriesController@store')->name('admin.categories.store');
        Route::get('{categoryId}/edit', 'Backend\CategoriesController@edit')->name('admin.categories.edit');
        Route::post('{categoryId}/update', 'Backend\CategoriesController@update')->name('admin.categories.update');
        Route::get('{categoryId}/destroy', 'Backend\CategoriesController@destroy')->name('admin.categories.destroy');
        Route::get('{categoryId}/up', 'Backend\CategoriesController@up')->name('admin.categories.up');
        Route::get('{categoryId}/down', 'Backend\CategoriesController@down')->name('admin.categories.down');
    });

    Route::group(['prefix' => 'news', 'middleware' => 'adminPermissionMiddleware:content_news'], function () {
        Route::get('/', 'Backend\Content\NewsController@index')->name('admin.content.news');
        Route::get('get-list', 'Backend\Content\NewsController@getList')->name('admin.content.news.list');
        Route::get('create', 'Backend\Content\NewsController@create')->name('admin.content.news.create');
        Route::post('store', 'Backend\Content\NewsController@store')->name('admin.content.news.store');
        Route::get('{itemId}/edit', 'Backend\Content\NewsController@edit')->name('admin.content.news.edit');
        Route::post('{itemId}/update', 'Backend\Content\NewsController@update')->name('admin.content.news.update');
        Route::get('{itemId}/destroy', 'Backend\Content\NewsController@destroy')->name('admin.content.news.destroy');
        Route::post('{itemId}/media', 'Backend\Content\NewsController@media')->name('admin.content.news.media');
        Route::get('{mediaId}/delete-media', 'Backend\Content\NewsController@deleteMedia')->name('admin.content.news.delete.media');
        Route::get('{itemId}/{mediaId}/main-media', 'Backend\Content\NewsController@mainMedia')->name('admin.content.news.main.media');
        Route::get('{itemId}/crop-create','Backend\Content\NewsController@cropCreate')->name('admin.news.crop.create');
        Route::get('{mediaId}/crop-show','Backend\Content\NewsController@showCrop')->name('admin.news.crop.show');
        Route::post('{mediaId}/save-crop','Backend\Content\NewsController@saveCrop')->name('admin.news.crop.save');
            Route::post('{mediaId}/save-media-desc','Backend\Content\NewsController@saveMediaDesc')->name('admin.news.media.desc.save');

    });

//    Route::group(['prefix'=>'albums'],function(){
//        Route::get('create','Backend\Gallery\AlbumController@create')->name('admin.gallery.album.create');
//        Route::post('store','Backend\Gallery\AlbumController@store')->name('admin.gallery.album.store');
////        Route::get()
//    });

    Route::group(['prefix'=>'static-pictures'],function(){
        Route::get('/','Backend\StaticPicturesController@index')->name('admin.static.pictures');
        Route::get('get-list','Backend\StaticPicturesController@getList')->name('admin.static.pictures.list');
        Route::get('{staticPictureId}/edit','Backend\StaticPicturesController@edit')->name('admin.static.pictures.edit');
        Route::post('{staticPictureId}/update','Backend\StaticPicturesController@update')->name('admin.static.pictures.update');
        Route::post('{staticPictureId}/media', 'Backend\StaticPicturesController@media')->name('admin.static.pictures.media');
        Route::get('{mediaId}/delete-media', 'Backend\StaticPicturesController@deleteMedia')->name('admin.static.pictures.delete.media');
        Route::get('{staticPictureId}/{mediaId}/main-media', 'Backend\StaticPicturesController@mainMedia')->name('admin.static.pictures.main.media');
        Route::get('{modelId}/crop-create','Backend\StaticPicturesController@cropCreate')->name('admin.static.pictures.crop.create');
        Route::get('{mediaId}/crop-show','Backend\StaticPicturesController@showCrop')->name('admin.static.pictures.crop.show');
        Route::post('{mediaId}/save-crop','Backend\StaticPicturesController@saveCrop')->name('admin.static.pictures.crop.save');
    });

    Route::group(['prefix'=>'our-team'],function(){
        Route::get('/','Backend\OurTeamController@index')->name('admin.team');
        Route::get('get-list','Backend\OurTeamController@getList')->name('admin.team.list');
        Route::get('create', 'Backend\OurTeamController@create')->name('admin.team.create');
        Route::post('store', 'Backend\OurTeamController@store')->name('admin.team.store');
        Route::get('{manId}/edit','Backend\OurTeamController@edit')->name('admin.team.edit');
        Route::post('{manId}/update','Backend\OurTeamController@update')->name('admin.team.update');
        Route::post('{manId}/media', 'Backend\OurTeamController@media')->name('admin.team.media');
        Route::get('{mediaId}/delete-media', 'Backend\OurTeamController@deleteMedia')->name('admin.team.delete.media');
        Route::get('{manId}/{mediaId}/main-media', 'Backend\OurTeamController@mainMedia')->name('admin.team.main.media');
        Route::get('{modelId}/crop-create','Backend\OurTeamController@cropCreate')->name('admin.team.crop.create');
        Route::get('{mediaId}/crop-show','Backend\OurTeamController@showCrop')->name('admin.team.crop.show');
        Route::post('{mediaId}/save-crop','Backend\OurTeamController@saveCrop')->name('admin.team.crop.save');
        Route::get('{manId}/destroy','Backend\OurTeamController@destroy')->name('admin.team.destroy');
    });

    Route::group(['prefix'=>'gallery'],function(){
        Route::get('/','Backend\Gallery\GalleryController@index')->name('admin.gallery.index');
        Route::get('get-list','Backend\Gallery\GalleryController@getList')->name('admin.gallery.list');
        Route::get('create','Backend\Gallery\GalleryController@create')->name('admin.gallery.create');
        Route::get('create-video', 'Backend\Gallery\GalleryController@createVideo')->name('admin.gallery.create.video');
        Route::post('store','Backend\Gallery\GalleryController@store')->name('admin.gallery.store');
        Route::get('{itemId}/edit','Backend\Gallery\GalleryController@edit')->name('admin.gallery.edit');
        Route::get('{itemId}/edit-video','Backend\Gallery\GalleryController@editVideo')->name('admin.gallery.edit.video');
        Route::post('{itemId}/update','Backend\Gallery\GalleryController@update')->name('admin.gallery.update');
        Route::get('{itemId}/destroy','Backend\Gallery\GalleryController@destroy')->name('admin.gallery.destroy');
        Route::post('{itemId}/media','Backend\Gallery\GalleryController@media')->name('admin.gallery.media');
        Route::get('{itemId}/delete-media','Backend\Gallery\GalleryController@deleteMedia')->name('admin.gallery.delete.media');
        Route::get('{itemId}/{mediaId}/main-media','Backend\Gallery\GalleryController@mainMedia')->name('admin.gallery.main.media');
        Route::get('{modelId}/crop-create','Backend\Gallery\GalleryController@cropCreate')->name('admin.gallery.crop.create');
        Route::get('{imageId}/crop-show','Backend\Gallery\GalleryController@showCrop')->name('admin.gallery.crop.show');
        Route::post('{imageId}/save-crop','Backend\Gallery\GalleryController@saveCrop')->name('admin.gallery.crop.save');
        Route::get('{imageId}/up','Backend\Gallery\GalleryController@mediaUp')->name('admin.gallery.crop.media.up');
        Route::get('{imageId}/down','Backend\Gallery\GalleryController@mediaDown')->name('admin.gallery.crop.media.down');
    });

    Route::group(['prefix' => 'pages', 'middleware' => 'adminPermissionMiddleware:content_pages'], function() {
        Route::get('/', 'Backend\Content\PageController@index')->name('admin.content.pages');
        Route::get('get-list', 'Backend\Content\PageController@getList')->name('admin.content.pages.list');
        Route::get('create', 'Backend\Content\PageController@create')->name('admin.content.pages.create');
        Route::post('store', 'Backend\Content\PageController@store')->name('admin.content.pages.store');
        Route::get('{pageId}/edit', 'Backend\Content\PageController@edit')->name('admin.content.pages.edit');
        Route::post('{pageId}/update', 'Backend\Content\PageController@update')->name('admin.content.pages.update');
        Route::get( '{pageId}/destroy', 'Backend\Content\PageController@destroy' )->name( 'admin.content.pages.destroy' );
        Route::post('{pageId}/media', 'Backend\Content\PageController@media')->name('admin.content.pages.media');
        Route::get('{mediaId}/delete-media', 'Backend\Content\PageController@deleteMedia')->name('admin.content.pages.delete.media');
        Route::get('{pageId}/{mediaId}/main-media', 'Backend\Content\PageController@mainMedia')->name('admin.content.pages.main.media');
        Route::post('{mediaId}/update-media', 'Backend\Content\PageController@updateMedia')->name('admin.content.pages.update.media');
    });

    Route::group(['prefix' => 'tours'], function(){
        Route::get('/','TourController@index')->name('admin.tours');
        Route::get('create-category','TourController@category')->name('admin.tours.category');
        Route::post('store-category','TourController@categoryStore')->name('admin.tours.category');
        Route::get('get-list','TourController@getList')->name('admin.tours.list');
        Route::get('create','TourController@create')->name('admin.tours.create');
        Route::post('store','TourController@store')->name('admin.tours.store');
        Route::get('{tourId}/edit','TourController@edit')->name('admin.tours.edit');
        Route::post('{tourId}/update','TourController@update')->name('admin.tours.update');
        Route::get('{tourId}/destroy','TourController@destroy')->name('admin.tours.destroy');
        Route::post('{tourId}/media','TourController@media')->name('admin.tours.media');
        Route::get('{tourId}/{mediaId}/main-media','TourController@mainMedia')->name('admin.tours.main.media');
        Route::get('{mediaId}/delete-media','TourController@deleteMedia')->name('admin.tours.media.delete');
        Route::get('{mediaId}/update-media','TourController@updateMedia')->name('admin.tours.media.update');
        Route::get('{tourId}/crop-create','TourController@cropCreate')->name('admin.tours.crop.create');
        Route::get('{mediaId}/crop-show','TourController@showCrop')->name('admin.tours.crop.show');
        Route::post('{mediaId}/save-crop','TourController@saveCrop')->name('admin.tours.crop.save');
    });

    Route::group(['prefix' => 'hotels'], function(){
        Route::get('/','HotelController@index')->name('admin.hotels');
        Route::get('get-list','HotelController@getList')->name('admin.hotels.list');
        Route::get('create','HotelController@create')->name('admin.hotels.create');
        Route::post('store','HotelController@store')->name('admin.hotels.store');
        Route::get('{hotelId}/edit','HotelController@edit')->name('admin.hotels.edit');
        Route::post('{hotelId}/update','HotelController@update')->name('admin.hotels.update');
        Route::get('{hotelId}/destroy','HotelController@destroy')->name('admin.hotels.destroy');
        Route::post('{hotelId}/media','HotelController@media')->name('admin.hotels.media');
        Route::get('{hotelId}/{mediaId}/main-media','HotelController@mainMedia')->name('admin.hotels.main.media');
        Route::get('{mediaId}/delete-media','HotelController@deleteMedia')->name('admin.hotels.media.delete');
        Route::get('{mediaId}/update-media','HotelController@updateMedia')->name('admin.hotels.media.update');
    });

    Route::group(['prefix' => 'facts'], function(){
        Route::get('/','Backend\Content\FactController@index')->name('admin.facts');
        Route::get('get-list','Backend\Content\FactController@getList')->name('admin.facts.list');
        Route::get('create','Backend\Content\FactController@create')->name('admin.facts.create');
        Route::post('store','Backend\Content\FactController@store')->name('admin.facts.store');
        Route::get('{factId}/edit','Backend\Content\FactController@edit')->name('admin.facts.edit');
        Route::post('{factId}/update','Backend\Content\FactController@update')->name('admin.facts.update');
        Route::get('{factId}/destroy','Backend\Content\FactController@destroy')->name('admin.facts.destroy');
        Route::post('{factId}/media','Backend\Content\FactController@media')->name('admin.facts.media');
        Route::get('{factId}/{mediaId}/main-media','Backend\Content\FactController@mainMedia')->name('admin.facts.main.media');
        Route::get('{mediaId}/delete-media','Backend\Content\FactController@deleteMedia')->name('admin.facts.media.delete');
        Route::get('{mediaId}/update-media','Backend\Content\FactController@updateMedia')->name('admin.facts.media.update');
        Route::get('{factId}/crop-create','Backend\Content\FactController@cropCreate')->name('admin.facts.crop.create');
        Route::get('{mediaId}/crop-show','Backend\Content\FactController@showCrop')->name('admin.facts.crop.show');
        Route::post('{mediaId}/save-crop','Backend\Content\FactController@saveCrop')->name('admin.facts.crop.save');
    });

    Route::group(['prefix'=>'leisure'],function(){
        Route::get('/','LeisureController@index')->name('admin.leisure');
        Route::get('get-list','LeisureController@getList')->name('admin.leisure.list');
        Route::get('create','LeisureController@create')->name('admin.leisure.create');
        Route::post('store','LeisureController@store')->name('admin.leisure.store');
        Route::get('{leisureId}/edit','LeisureController@edit')->name('admin.leisure.edit');
        Route::post('{leisureId}/update','LeisureController@update')->name('admin.leisure.update');
        Route::get('{leisureId}/destroy','LeisureController@destroy')->name('admin.leisure.destroy');
        Route::post('{leisureId}/media','LeisureController@media')->name('admin.leisure.media');
        Route::get('{leisureId}/{mediaId}/main-media','LeisureController@mainMedia')->name('admin.leisure.main.media');
        Route::get('{mediaId}/delete-media','LeisureController@deleteMedia')->name('admin.leisure.media.delete');
        Route::get('{mediaId}/update-media','LeisureController@updateMedia')->name('admin.leisure.media.update');
    });

    Route::group(['prefix' => 'settings'], function () {

        Route::group(['prefix' => 'menu', 'middleware' => 'adminPermissionMiddleware:settings_menu'], function () {
            Route::get('/', 'Backend\Settings\MenuController@index')->name('admin.settings.menu');
            Route::get('get-list', 'Backend\Settings\MenuController@getList')->name('admin.settings.menu.list');
            Route::get('create', 'Backend\Settings\MenuController@create')->name('admin.settings.menu.create');
            Route::post('store', 'Backend\Settings\MenuController@store')->name('admin.settings.menu.store');
            Route::get('{menuId}/edit', 'Backend\Settings\MenuController@edit')->name('admin.settings.menu.edit');
            Route::post('{menuId}/update', 'Backend\Settings\MenuController@update')->name('admin.settings.menu.update');

            Route::get('{menuId}/view', 'Backend\Settings\MenuController@view')->name('admin.settings.menu.item.view');
            Route::get('{menuId}/submenu/create', 'Backend\Settings\MenuController@itemCreate')->name('admin.settings.menu.item.create');
            Route::post('{menuId}/submenu/store', 'Backend\Settings\MenuController@itemStore')->name('admin.settings.menu.item.store');
            Route::get('{menuId}/submenu/{submenuId}/edit', 'Backend\Settings\MenuController@itemEdit')->name('admin.settings.menu.item.edit');
            Route::post('{menuId}/submenu/{submenuId}/update', 'Backend\Settings\MenuController@itemUpdate')->name('admin.settings.menu.item.update');
            Route::get('{menuId}/submenu/{submenuId}/up', 'Backend\Settings\MenuController@up')->name('admin.settings.menu.up');
            Route::get('{menuId}/submenu/{submenuId}/down', 'Backend\Settings\MenuController@down')->name('admin.settings.menu.down');
            Route::get('{menuId}/submenu/{submenuId}/destroy', 'Backend\Settings\MenuController@itemDestroy')->name('admin.settings.menu.destroy');

            Route::group(['prefix' => 'manage'], function () {
                Route::get('/', 'Backend\SettingController@menu')->name('admin.menu.manage');
                Route::post('update', 'Backend\SettingController@menuUpdate')->name('admin.menu.manage.update');
                Route::get('footer','Backend\SettingController@footer')->name('admin.footer.manage');
                Route::post('footer-update','Backend\SettingController@footerUpdate')->name('admin.footer.manage.update');
            });
        });

        Route::group(['prefix' => 'seo', 'middleware' => 'adminPermissionMiddleware:settings_seo'], function () {
            Route::get('handle', 'Backend\Settings\SeoController@handle')->name('admin.settings.seo.handle');
            Route::post('update', 'Backend\Settings\SeoController@update')->name('admin.settings.seo.update');
        });

        Route::group(['prefix' => 'media'], function () {
            Route::get('model/{owner}/{modelId}', 'Backend\Settings\MediaController@getModelMedia')->name('admin.media.model');
            Route::post('model/{owner}/{modelId}/upload', 'Backend\Settings\MediaController@addMediaForModel')->name('admin.media.model.upload');
            Route::get('{mediaId}/delete', 'Backend\Settings\MediaController@deleteMediaForModel')->name('admin.media.model.delete');
            Route::get('{mediaId}/set-main', 'Backend\Settings\MediaController@setMediaMain')->name('admin.media.model.main');
            Route::get('editor-images', 'Backend\Settings\MediaController@getEditorImages')->name('admin.media.editor.images');
            Route::post('editor-images/upload', 'Backend\Settings\MediaController@addMediaForEditor')->name('admin.media.editor.upload');

            Route::get('editor-files', 'Backend\Settings\MediaController@getEditorFiles')->name('admin.media.editor.files');
            Route::post('editor-files/upload', 'Backend\Settings\MediaController@addFilesForEditor')->name('admin.media.editor.files.upload');

            Route::get('editor-album', 'Backend\Settings\MediaController@getEditorAlbum')->name('admin.media.editor.album');
            Route::post('editor-album/upload', 'Backend\Settings\MediaController@addAlbumForEditor')->name('admin.media.editor.album.upload');
        });
    });

    Route::group(['prefix' => 'menu'], function () {
        Route::get('/', 'Backend\SettingController@menu')->name('admin.menu');
        Route::post('update', 'Backend\SettingController@menuUpdate')->name('admin.menu.update');
        Route::get('footer','Backend\SettingController@footer')->name('admin.footer');
        Route::post('footer-update','Backend\SettingController@footerUpdate')->name('admin.footer.update');
    });

});
Route::get('/', function () {})->middleware('site.locale');
Route::get('change-locale/{locale}', 'Frontend\ChangeLocaleController@changeLocale')->name('changeLocale');

Route::group(['prefix' => '{locale}', 'middleware' => 'site.locale'], function () {
    Route::get('/', 'Frontend\HomeController@index')->name('home');

    Route::get('o-nas','Frontend\HomeController@ourView')->name('home.our.view');

    Route::group(['prefix'=>'tours'],function(){
        Route::get('/','Frontend\TourController@index')->name('home.tours');
        Route::get('private-tours','Frontend\TourController@vipTour')->name('home.tours.vip');
        Route::get('standard-tours','Frontend\TourController@standardTour')->name('home.tours.standard');
        Route::get('{slug}','Frontend\TourController@tourItem')->name('home.tours.item');
        Route::get('standard_tour/{slug}','Frontend\TourController@standardItem')->name('home.tours.standard.item');
        Route::get('private_tour/{slug}','Frontend\TourController@privateItem')->name('home.tours.private.item');
        Route::get('get-list','Frontend\TourController@getList')->name('home.tours.list');
    });

    Route::group(['prefix'=>'hotels'],function(){
        Route::get('/','Frontend\HotelController@index')->name('home.hotels');
        Route::get('get-list','Frontend\HotelController@getList')->name('home.hotels.list');
    });

    Route::group(['prefix'=>'dosug'],function(){
        Route::get('/','Frontend\LeisureController@index')->name('home.leisure');
        Route::get('{categoryId}/category','Frontend\LeisureController@categoryLeisure')->name('home.leisure.category');
        Route::get('{slug}','Frontend\LeisureController@leisureItem')->name('home.leisure.category.item');
    });

    Route::group(['prefix'=>'news'], function(){
        Route::get('/','Frontend\NewsController@index')->name('home.news');
        Route::get('{slug}','Frontend\NewsController@show')->name('home.news.show');
    });

    Route::group(['prefix'=>'gallery'], function(){
        Route::get('/','Frontend\GalleryController@index')->name('home.gallery');
        Route::get('image-gallery','Frontend\GalleryController@imageGallery')->name('home.gallery.image');
        Route::get('{slug}/image-gallery','Frontend\GalleryController@imageInnerGallery')->name('home.gallery.image.inner');
        Route::get('video-gallery','Frontend\GalleryController@videoGallery')->name('home.gallery.video');
        Route::get('video-gallery/{slug}','Frontend\GalleryController@videoItem')->name('home.gallery.video.item');
    });

    Route::group(['prefix'=>'search'], function(){
        Route::get('result','SearchController@query')->name('home.search');
//        Route::get('{query}','SearchController@queryList')->name('home.search.list');
    });

});
