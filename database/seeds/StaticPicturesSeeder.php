<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StaticPicturesSeeder extends Seeder
{
    /**
     * @var \App\Models\StaticPicture
     */
    private $staticPicture;

    public function __construct(\App\Models\StaticPicture $staticPicture)
    {
        $this->staticPicture = $staticPicture;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staticPictures = [
            [
                'id'=>1,
                'title'=>'Первое',
                'slug'=>'pervoe'
            ],
            [
                'id'=>2,
                'title'=>'Второе',
                'slug'=>'vtoroe'
            ]

        ];

        DB::table('static_pictures')->truncate();

        foreach ($staticPictures as $staticPicture) {
            DB::table('static_pictures')->insert($staticPicture);
        }
    }
}
