<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TourCategorySeeder extends Seeder
{
    /**
     * @var \App\Models\TourCategory
     */
    private $category;

    public function __construct(\App\Models\TourCategory $category)
    {
        $this->category = $category;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'title'=>json_encode(['ru'=>'VIP туры'])
            ],
            [
                'title'=>json_encode(['ru'=>'Стандартные'])
            ]
        ];

        DB::table('tour_categories')->truncate();

        foreach ($categories as $category) {
            DB::table('tour_categories')->insert($category);
        }
    }
}
