
let app = function() {

    var settings = {
        blockUi: {
            type: 'page',
            'element': ''
        },

        progressBarSelector: "#progressBar"
    };

    let handleClick = function () {

        $(document.body).on('click', '.handle-click', function (e) {
            e.preventDefault();
            let type = $(this).data('type');

            switch (type)
            {
                case 'triggerHiddenInput':
                    let input = $("#" + $(this).data('input-id'));
                    triggerHiddenInput(input);
                    break;

                case 'ajax-get':
                    var url = $(this).attr('href');
                    ajaxGet(url);
                    break;

                case 'delete-table-row':

                    var url = $(this).attr('href');
                    var title = $(this).data('confirm-title');
                    var message = $(this).data('confirm-message');
                    var cancelBtnText = $(this).data('cancel-text');
                    var confirmBtnText = $(this).data('confirm-text');
                    swal({
                        title: title,
                        text: message,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: cancelBtnText,
                        confirmButtonText: confirmBtnText
                    }).then((result) => {
                        if (result.value) {
                            ajaxGet(url)
                        }
                    })
                break;

                case 'confirm':
                    var url = $(this).attr('href');
                    var title = $(this).data('confirm-title');
                    var message = $(this).data('confirm-message');
                    var cancelBtnText = $(this).data('cancel-text');
                    var confirmBtnText = $(this).data('confirm-text');
                    var needUrlFollow = $(this).data('follow-url');

                    swal({
                        title: title,
                        text: message,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: cancelBtnText,
                        confirmButtonText: confirmBtnText
                    }).then((result) => {
                        if (result.value) {
                            if (needUrlFollow)
                            {
                                ajaxGet(url)
                            }

                        }
                    })

                    break;

                case 'modal':
                    var url = $(this).attr('href');
                    var modal = $($(this).data('modal'));
                    modal.modal('show');
                    loadContentInModal(url, modal)
                    break;

                case 'message':
                    showAlert('Упс','Вы должны создать категорию и обновить страницу','info');

            }
        });
    }

    let loadContentInModal = function (url, modal) {
        modal.find('.modal-title').html('...');
        modal.find('.modal-body').html('...');

        ajaxGet(url, "#largeModal .modal-body");
    }

    let updateModalContent = function (modal, modalTitle, modalBody) {
        $(modal).find('.modal-title').html(modalTitle);
        $(modal).find('.modal-body').html(modalBody);
        $(modal).find('.modal-body').removeClass('busy');
    }

    let triggerHiddenInput = function (input) {

        input.val('');
        input.trigger('click');
        submitFormOnInputChange(input)
    }

    let submitFormOnInputChange = function (input) {
        let form = input.closest('form');

        input.change(function () {
            form.submit();
        });
    }

    let handleFormSubmit = function () {

        $('body').on('submit', 'form', function(e) {

            if ($(this).hasClass('ajax'))
            {
                e.preventDefault();
                ajaxPost($(this));
            }

            if ($(this).attr('id') == 'filterForm')
            {
                e.preventDefault();
                var url = $(this).attr('action') + '?' + $(this).serialize();
                loadContentInTable(url, $('#ajaxTable'));

            }
        });
    }

    let handlePaginationClick = function () {
        $(document.body).on('click', '.pagination .page-link', function (e) {
            e.preventDefault()
            let url = $(this).attr('href');
            let table = $("#" + $(this).closest('.pagination_placeholder').data('table-id'));
            loadContentInTable(url, table);
        });
    }
    
    let handleLoadContentInTable = function () {

        $.each($('.ajax-content'), function() {

            let url = $(this).attr('data-ajax-content-url');

            loadContentInTable(url, $(this));

        })

    }

    let loadContentInTable = function(url, table) {

        $.ajax({
            method: 'get',
            url: url,
            dataType: 'json',

            beforeSend: function () {
                blockUI();
            },

            success: function (response) {
                table.find('tbody').html(response.tableData);

                if (response.pagination)
                {
                    table.closest('.box').find('.pagination_placeholder').html(response.pagination);
                }

            },

            error: function (response) {
                console.log(response);
            },
            complete: function () {
                unblockUi();
            }

        });
    }

    let ajaxGet = function (url, blockElem) {

        $.ajax({
            method: 'get',
            url: url,
            dataType: 'json',

            beforeSend: function () {
                if (blockElem)
                {
                    $(blockElem).addClass('busy');
                } else {

                    blockUI();
                }

            },

            success: function (response) {
                switch (response.type)
                {
                    // case 'updateCroppedImage':
                    //
                    //     break;
                    case 'update-table-row':
                        updateTableRow($(response.table), $(response.row), response.content)
                        break;

                    case 'delete-table-row':
                        deleteTableRow($(response.table), $(response.row));
                        break;

                    case 'delete-block':
                        $(response.block).fadeOut(function () {
                            $(this).remove();
                        });
                        break;

                    case 'updateModal':
                        updateModalContent(response.modal, response.modalTitle, response.modalContent);
                        break;

                    case 'reloadTable':
                        loadContentInTable(response.tableContentUrl, $(response.tableId))
                        break;

                    case 'updateBlock':
                        $(response.blockId).html(response.blockData);
                        break;
                }

                if (response.functions)
                {
                    $.each(response.functions, function (index, value) {
                        runFunction(value, response);
                    });
                }

            },

            error: function (response) {

            },
            complete: function () {
                if (blockElem)
                {
                    $(blockElem).removeClass('busy');
                } else {

                    unblockUi();
                }

            }

        });
    }

    let ajaxPost = function (form) {

        let url = form.attr('action');
        let formId = form.attr('id');
        let formData = new FormData(form[0]);
        let hasProgress = form.data('show-progress');

        settings.blockUi.type = form.data('ui-block-type');
        settings.blockUi.element = form.data('ui-block-element');

        $.ajax({
            method: 'post',
            url: url,
            data: formData,
            dataType: 'json',
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            xhr: function()
            {
                let xhr = new window.XMLHttpRequest();
                //Upload progress
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable)
                    {
                        let percentComplete = evt.loaded / evt.total * 100;
                        updateProgress(percentComplete);
                    }
                }, false);

                return xhr;
            },

            beforeSend: function () {

                blockUI();
                validationReset();
                initProgress();
            },

            success: function (response) {

                switch (response.type)
                {
                    case 'redirect':
                        location.replace(response.redirect_url);
                        break;

                    case 'prepend-table-row':
                        prependTableData($(response.table), response.content);
                        break;

                    case 'update-table-row':
                        updateTableRow($(response.table), $(response.row), response.content);
                        break;

                    case 'reloadTable':
                        loadContentInTable(response.tableContentUrl, $(response.tableId))
                        break;

                    case 'updateModal':
                        updateModalContent(response.modal, response.modalTitle, response.modalContent);
                        break;

                    case 'notify':
                        showNotify(response.notify_message, response.notify_type)
                        break;
                }

                if (response.functions)
                {
                    $.each(response.functions, function (index, value) {
                        runFunction(value, response);
                    });
                }
            },

            error: function (response) {

                if (response.status == 419) {
                    location.refresh();
                }

                if (response.status == 422) {
                    validationFail(response.responseJSON, formId);
                }

                if (response.responseJSON && response.responseJSON.type) {
                    if (response.responseJSON.type == 'alert') {
                        showAlert(response.responseJSON.header, response.responseJSON.message, response.responseJSON.alert_type)
                    }
                }

                unblockUi();

            },
            complete: function () {

                unblockUi();
                resetProgress();
            }
        });
    }

    let initProgress = function () {
        $(settings.progressBarSelector).find('.progress-bar').css('width', '0%');
        $(settings.progressBarSelector).find('.progress-bar').css('opacity', 1);

    }

    let updateProgress = function (percent) {
        $(settings.progressBarSelector).find('.progress-bar').css('width', percent +'%');
    }

    let resetProgress = function () {
        $(settings.progressBarSelector).find('.progress-bar').fadeOut(function () {
            $(this).css('width', 0)
        }).fadeIn();
    }

    let validationFail = function (response, formId) {
        for (input in response.errors) {

            var formGroup = $("#" + formId + " *[id='" + input + "']").closest('.form-group');

            formGroup.addClass('has-error');

            for (message in response.errors[input]) {
                formGroup.find('.help-block').append(' ' + response.errors[input][message]);
            }
        }
    }

    let validationReset = function () {
        $('.form-group').find('.help-block').html('');
        $('.form-group').removeClass('has-error');
    }
    
    let showAlert = function (header, message, type) {
        swal({
            type: type,
            title: header,
            text: message,

        })
    }
    
    let showNotify = function (message, type) {

        $.notify({
            // options
            message: message
        },{
            // settings
            type: type,
            newest_on_top: true,
            placement: {
                from: "top",
                align: "right"
            },

            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },

        });
    }

    let prependTableData = function (table, data) {
        table.find('tbody').prepend(data);
    }

    let updateTableRow = function (table, row, data) {

        table.find(row).fadeOut(function () {
            $(this).replaceWith(data);
            $(this).fadeIn();
        });
    }

    let deleteTableRow = function (table, row) {
        table.find(row).fadeOut(function () {
            $(this).remove();

        });
    }

    let runFunction = function(functionName, response)
    {
        switch (functionName)
        {
            case 'closeModal':
             closeModal(response);
                break;

            case 'initEditor':
                initEditor();
                break;
        }
    }

    let closeModal = function (data) {
        $(data.modal).modal('hide');
    }
    
    let blockUI = function () {

        switch (settings.blockUi.type)
        {
            case 'page':
                blockPage();
                break;

            case 'element':
                $(settings.blockUi.element).addClass('busy');
                break;
        }
    }
    
    let unblockUi = function () {
        switch (settings.blockUi.type)
        {
            case 'page':
                $('body').unblock();
                break;

            case 'element':
                $(settings.blockUi.element).removeClass('busy');
                break;
        }

    }

    let blockPage = function (options) {

        let el = $('body');

         options = $.extend(true, {
            opacity: 0.1,
            overlayColor: '#000000',
            state: 'primary',
            type: 'loader',
            centerX: true,
            centerY: true,
            message: 'Processing...',
            shadow: true,
            width: 'auto'
        }, options);

        let params = {
            message: '<div style="border: 1px solid #D2D6DE; border-radius: 5px; background-color: #fff; padding: 5px; color: #777777;">Processing <i class="fa fa-spinner fa-spin"></i></div>',
            centerY: options.centerY,
            centerX: options.centerX,
            css: {
                top: '30%',
                left: '50%',
                border: '0',
                padding: '0',
                backgroundColor: 'none',
                width: options.width
            },
            overlayCSS: {
                backgroundColor: options.overlayColor,
                opacity: options.opacity,
                cursor: 'wait'
            },

            onUnblock: function() {
                if (el) {
                    el.css('position', '');
                    el.css('zoom', '');
                }
            }
        };

        params.css.top = '50%';
        $.blockUI(params);
    }

    // let initDatePicker = function () {
    //     $('.dp').datepicker({
    //         autoclose: true,
    //         format: 'dd.mm.yyyy'
    //     })
    // }

    let initEditor = function () {
        $('.editor').each( function () {
            editor = CKEDITOR.replace($(this).attr('id'), {
                height: 500
            });

            editor.ui.addButton('ImageManager', {
                label: "Менеджер изображений",
                command: 'showImageManager',
                toolbar: 'insert',
                icon: '/app/js/vendors/ckeditor/image_man.png'
            });

            editor.ui.addButton('FileManager', {
                label: "Менеджер файлов",
                command: 'showFileManager',
                toolbar: 'insert',
                icon: '/app/js/vendors/ckeditor/image_file.png'
            });

            editor.ui.addButton('AlbumManager', {
                label: "Загрузка альбома",
                command: 'showAlbum',
                toolbar: 'insert',
                icon: '/app/js/vendors/ckeditor/albums.png'
            });

            editor.addCommand("showImageManager", {
                exec: function(edt) {
                    showGalleryImages(edt);
                }
            });

            editor.addCommand("showFileManager", {
                exec: function(edt) {
                    showFiles(edt);
                }
            });

            editor.addCommand("showAlbum", {
                exec: function(edt) {
                    showAlbum(edt);
                }
            });

        });
    };

    return {
        init: function () {
            handleFormSubmit();
            handleLoadContentInTable();
            handlePaginationClick();
            handleClick();
            // initDatePicker();
            initEditor();
        },
    }
}();


$(document.body).on('click','#show-crop-image',function(e){
    e.preventDefault();
    var url = $(this).attr('href');

    $.get(url, function (data) {
        $("#cropImagePlaceholder").html(data.response.image);
        $(".scale-form").css('display','block');
        $("#cropForm").attr('action', data.response.formAction);

        if(data.response.pastImage){
            $("#croppedImage").attr('src',data.response.pastImage +'?value=' + new Date().getTime());
        }else{
            $("#croppedDiv").css('display','none');



        }


        var $image = $('#full-size-picture');

        $image.cropper({
            aspectRatio: 1 / 1,
            viewMode: 3,
            crop: function(event) {
                $("#width").val(Math.round(event.detail.width));
                $("#height").val(Math.round(event.detail.height));
                $("#X").val(Math.round(event.detail.x));
                $("#Y").val(Math.round(event.detail.y));
                // console.log(event.detail.x);
                // console.log(event.detail.y);
                // console.log(event.detail.width);
                // console.log(event.detail.height);
                // console.log(event.detail.rotate);
                // console.log(event.detail.scaleX);
                // console.log(event.detail.scaleY);
            }
        });
        var cropper = $image.data('cropper');
    });
});


$(document.body).on('click','#rotate',function(e){
    var $image = $('#full-size-picture');

});


$(document.body).on('click','.show-crop',function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    var aspectRatio = $(this).data('original-title');
    // var rotate = document.getElementById('rotate');
    // $('#full-size-picture').css('rotate','45deg');
    // console.log(aspectRatio);
    $.get(url, function (data) {
        // console.log(data);
        $("#cropImagePlaceholder").html(data.response.image);
        $(".scale-form").css('display','block');
        $("#cropForm").attr('action', data.response.formAction);
        // alert();
        $('#imageName').val(data.response.imageDesc);
        $("#imageSecondName").val(data.response.imageDesc2);

        if(data.response.pastImage){
            $("#croppedImage").attr('src',data.response.pastImage +'?value=' + new Date().getTime());
        }else{
            $("#croppedDiv").css('display','none');

        }

        var $image = $('#full-size-picture');
        // $image.css('rotate',45);
        // console.log(aspectRatio);
        // $image.cropper('rotate', 90);
        $image.cropper({
            aspectRatio: aspectRatio,
            viewMode: 0,

            crop: function(event) {
                // alert(event.detail.width);
                $("#width").val(Math.round(event.detail.width));
                $("#height").val(Math.round(event.detail.height));
                $("#X").val(Math.round(event.detail.x));
                $("#Y").val(Math.round(event.detail.y));
                $("#rotate").val(event.detail.rotate);

                // console.log(event.detail.y);
                // console.log(event.detail.width);
                // console.log(event.detail.height);
                // console.log(event.detail.scaleX);
                // console.log(event.detail.scaleY);
                // console.log(event.detail.x);

            }
        })

        var cropper = $image.data('cropper');
    });
});


$(document).ready(function() {
    app.init();
});



