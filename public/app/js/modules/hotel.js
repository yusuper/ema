// $(document).ready(function () {

// });

var formFilter = function(){
    let form = $('#filterForm');
    let params = $(form).serialize();
    let url =   $("#filterForm").attr('action')+'?'+window.location.search.substr(1) + '&' + params;

    loadContent(url)
};

let loadContent = function (url) {
    $.ajax({
        method: 'get',
        url: url,
        dataType: 'json',

        success: function (response) {
            $("#objects_list").html(response.content)
        }
    })
};
if ($("#objects_list").length)
{
    let url = $("#objects_list").data('url');
    loadContent(url);
}

$(document.body).on('change', '.filterType', function (e) {
    formFilter();
});

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

$('#filterSearch').keyup(delay(function (e) {
    formFilter();
}, 500));


$(document.body).on('click', '#ajaxPagination .paginator .paginator__link', function (e) {
    e.preventDefault();
    let url = $(this).attr('href');
    loadContent(url)
});

$('form').submit(function (e) {
    e.preventDefault();
    formFilter();
});