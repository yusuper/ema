<?php
/**
 * Created by PhpStorm.
 * User: yusup
 * Date: 1/7/19
 * Time: 12:52 PM
 */

namespace App\Http\ViewComposers\Frontend;

use App\Models\Category;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\View\View;

class CommonComposer
{
    /**
     * @var HttpRequest
     */
    private $request;
    /**
     * @var Category
     */
    private $category;

    public function __construct(HttpRequest $request,Category $category)
    {
        $this->request = $request;
        $this->category = $category;
    }

    public function compose(View $view)
    {
        $currentLocale = $this->getCurrentLocale();
        $menu = $this->category->where('owner','menu-item-12')->orderBy('_lft','asc')->get();
//        foreach($menu as $item){
//            dd($item->children);
//        }

        $children = $this->category->where('parent_id','!=',null)->where('owner','menu-item-12')->get();
//        dd($menu);
        $view->with('currentLocale', $currentLocale);
        $view->with('menu', $menu);
        $view->with('children', $children);

    }

    public function getCurrentLocale()
    {
//        dd(in_array($this->request->segment(1), config('project.locales')));
        $locales = config('project.locales');
        foreach($locales as $locale){
            if (in_array($this->request->segment(1), $locale)) {
                return $this->request->segment(1);
            }
        }


        return config('project.default_locale');
    }
}