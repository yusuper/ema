<?php

namespace App\Http\Middleware;

use Closure;
use App;


class LocaleMiddleware
{
    protected $exceptions;

    public function __construct()
    {
        $this->exceptions = ['_debugbar'];
    }

    public function handle($request, Closure $next)
    {
        $locale = $request->segment(1);
        if(in_array($locale, $this->exceptions)) return $next($request);
        if (!$locale) return redirect(config('project.default_locale'));

        $exceptions = array_merge(config('project.locales'), $this->exceptions);

//        dd($exceptions);
//        if(!in_array($request->segment(1), $exceptions)) app()->abort('404');

        app()->setLocale($locale);
        return $next($request);
    }
}