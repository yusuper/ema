<?php

namespace App\Http\Controllers;

use App\Models\Media;
use App\Models\Tour;
use App\Models\TourCategory;
use Illuminate\Http\Request;
use App\Services\MediaService;
use Intervention\Image\Facades\Image;

class TourController extends Controller
{
    use ResponseTrait;
    /**
     * @var Tour
     */
    private $tour;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var MediaService
     */
    private $mediaService;
    /**
     * @var TourCategory
     */
    private $category;

    public function __construct(Tour $tour,
                                Media $media,
                                MediaService $mediaService,
                                TourCategory $category)
    {
        $this->tour = $tour;
        $this->media = $media;
        $this->mediaService = $mediaService;
        $this->category = $category;
    }

    public function index()
    {
        return view('backend.content.tours.index',[
            'title'=>'Туры'
        ]);
    }

    public function getList()
    {
        $tours = $this->tour->with('mainImage','category')->orderBy('created_at','desc')->paginate(20);
        return response()->json([
            'tableData' => view('backend.content.tours.list', [
                'tours' => $tours
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $tours->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $categories = $this->category->get();
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание тура',
            'modalContent' => view('backend.content.tours.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'categories'=>$categories,
                'formAction' => route('admin.tours.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }


    public function store(Request $request, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }

        $array = [
            'title' => $request->title,
            'category_id'=>$request->category_id,
            'site_display' => $site_display,
            'short_desc' => $request->short_desc,
            'standard_desc' => $request->standard_desc,
            'private_desc' => $request->private_desc,
            'price'=>$request->price,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
        ];
//        dd($array);
        $tour = $this->tour->create($array);

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.content.tours.tour_item', ['tour' => $tour])->render()
        ]);
    }

    public function edit($tourId)
    {
        $foreign_languages = config('project.foreign_locales');
        $tour = $this->tour->with('media')->find($tourId);
        $categories = $this->category->get();
        $create = 0;
        $medias = $tour->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование страницы',
            'modalContent' => view('backend.content.tours.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'tour' => $tour,
                'categories'=>$categories,
                'formAction' => route('admin.tours.update', ['id' => $tourId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(Request $request, $tourId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $tour = $this->tour->find($tourId);
        $tour->category_id=$request->input('category_id');
        $tour->title = $request->input('title');
        $tour->short_desc = $request->input('short_desc');
        $tour->standard_desc = $request->input('standard_desc');
        $tour->private_desc = $request->input('private_desc');
        $tour->site_display = $site_display;
        $tour->price = $request->price;
        $tour->meta_description = $request->meta_description;
        $tour->meta_keywords = $request->meta_keywords;
        $tour->save();


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $tourId,
            'content' => view('backend.content.tours.tour_item', ['tour' => $tour])->render()
        ]);
    }

    public function media(Request $request, MediaService $mediaService, $tourId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image,'image', "tours", $tourId, $thumbSizes);
        }

        $tours = $this->tour->whereHas('media', function ($query) use ($tourId) {
            $query->where('model_id', $tourId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.content.tours.media_list', [
                    'tours' => $tours,
                ])->render(),
        ]);
    }


    public function mainMedia($tourId, $mediaId)
    {
//        dd($pageId,$mediaId);
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'tours')
            ->where('model_id', $tourId)->get();
//        dd($otherMedia);
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'tours')->where('model_id', '=', $tourId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }

    public function updateMedia(Request $request, MediaService $mediaService,$mediaId)
    {

        $image = $request->image;
        $thumbSizes = ['512', '256', '128'];
//        $mediaService->editVideo($image,$mediaId, $thumbSizes);

    }

    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'tours')->find($mediaId);
        if($media->type=='image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }


    public function destroy($tourId)
    {
        $medias = $this->media->where(['owner' => 'tours', 'model_id' => $tourId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $tour = $this->tour->find($tourId);
        $tour->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $tourId,
        ]);

    }


    public function cropCreate($tourId)
    {
        $images = $this->mediaService->showCroppedModal('tours',$tourId);

//        var_dump($images->toArray());
        $data = [
            'modalTitle' => 'Обрезание картинки',
            'modalContent' => view('backend.cropper.form', [
                'title' => 'Кроп',
                'images'=>$images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()];

        return $this->responseJson($data);
    }

    public function showCrop($mediaId)
    {

        $image = $this->media->where('id',$mediaId)->first();

        $imgObj = Image::make(storage_path('app/public/uploaded_images/' .$image->getOriginal('original_file_name')));
        $pastImage = $this->mediaService->existingCroppedImage($image);
//        dd($pastImage);
        $data = [
            'image' => view('backend.cropper.crop_image', [
                'imageUrl' => $image->original_file_name,
                'width'=> $imgObj->width() / 2,
            ])->render(),
            'formAction' => route('admin.tours.crop.save',['mediaId'=>$mediaId]),
            'pastImage'=>$pastImage


        ];
        return $this->responseJson($data);
    }

    public function saveCrop(Request $request,$mediaId)
    {
        $imageItem = $this->media->where('id',$mediaId)->first();

        $params = ['width' => $request->width, 'height' => $request->height, 'offsetX' => $request->X, 'offsetY' => $request->Y];


        $imageSrc = $this->mediaService->cropImage($params,$mediaId);

        return $this->responseJson([
            'croppedImageSrc' => $imageSrc,
            'croppedText'=>'Обрезанная картинка'
        ]);
    }

}
