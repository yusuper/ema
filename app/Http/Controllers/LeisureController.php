<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Leisure;
use App\Models\Media;
use Illuminate\Http\Request;
use App\Services\MediaService;

class LeisureController extends Controller
{
    use ResponseTrait;
    /**
     * @var Leisure
     */
    private $leisure;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Leisure $leisure, Media $media, Category $category)
    {

        $this->leisure = $leisure;
        $this->media = $media;
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->where('owner','leisure')->get();
        $title = 'Досуг';
        return view('backend.leisure.index',[
            'title'=>$title,
            'categories'=>$categories
        ]);
    }

    public function getList()
    {
        $leisures = $this->leisure->paginate(20);
        return response()->json([
            'tableData' => view('backend.leisure.list', [
                'leisures' => $leisures
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $leisures->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $create = 1;
        $categories = $this->category->where('owner','leisure')->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание досуга',
            'modalContent' => view('backend.leisure.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'categories'=>$categories,
                'formAction' => route('admin.leisure.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function store(Request $request, MediaService $mediaService)
    {

        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }

        $array = [
            'title' => $request->title,
            'site_display' => $site_display,
            'category_id'=>$request->category,
            'short_desc' => $request->short_desc,
            'desc' => $request->desc,
            'price'=>$request->price,
            'phone'=>$request->phone,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
        ];
//        dd($array);
        $leisure = $this->leisure->create($array);

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.leisure.leisure_item', ['leisure' => $leisure])->render()
        ]);
    }

    public function edit($leisureId)
    {
        $foreign_languages = config('project.foreign_locales');
        $leisure = $this->leisure->with('media')->find($leisureId);
        $create = 0;
        $medias = $leisure->media->chunk(2);
        $categories = $this->category->where('owner','leisure')->get();

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование страницы',
            'modalContent' => view('backend.leisure.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'leisure' => $leisure,
                'categories'=>$categories,
                'formAction' => route('admin.leisure.update', ['id' => $leisureId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(Request $request, $leisureId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $leisure = $this->leisure->find($leisureId);
        $leisure->title = $request->input('title');
        $leisure->short_desc = $request->input('short_desc');
        $leisure->desc = $request->input('desc');
        $leisure->phone = $request->input('phone');
        $leisure->site_display = $site_display;
        $leisure->price = $request->price;
        $leisure->category_id = $request->category;
        $leisure->meta_description = $request->meta_description;
        $leisure->meta_keywords = $request->meta_keywords;
        $leisure->save();


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $leisureId,
            'content' => view('backend.leisure.leisure_item', ['leisure' => $leisure])->render()
        ]);
    }



    public function destroy($leisureId)
    {
        $medias = $this->media->where(['owner' => 'leisures', 'model_id' => $leisureId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $leisure = $this->leisure->find($leisureId);
        $leisure->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $leisureId,
        ]);

    }


    public function media(Request $request, MediaService $mediaService, $leisureId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image,'image', "leisures", $leisureId, $thumbSizes);
        }

        $leisures = $this->leisure->whereHas('media', function ($query) use ($leisureId) {
            $query->where('model_id', $leisureId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.leisure.media_list', [
                'leisures' => $leisures,
            ])->render(),
        ]);
    }

    public function mainMedia($leisureId, $mediaId)
    {
//        dd($pageId,$mediaId);
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'leisures')
            ->where('model_id', $leisureId)->get();
//        dd($otherMedia);
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'leisures')->where('model_id', '=', $leisureId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }


    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'leisures')->find($mediaId);
        if($media->type=='image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }

}
