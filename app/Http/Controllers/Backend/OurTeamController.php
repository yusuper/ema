<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait;
use App\Models\Media;
use App\Services\MediaService;
use Intervention\Image\Facades\Image;
use App\Models\OurTeam;

class OurTeamController extends Controller
{
    use ResponseTrait;

    /**
     * @var Media
     */
    private $media;
    /**
     * @var MediaService
     */
    private $mediaService;
    /**
     * @var OurTeam
     */
    private $ourTeam;

    public function __construct(OurTeam $ourTeam, Media $media, MediaService $mediaService)
    {
        $this->media = $media;
        $this->mediaService = $mediaService;
        $this->ourTeam = $ourTeam;
    }

    public function index()
    {
        return view('backend.our_team.index', [
            'title' => 'Наша команда'
        ]);
    }

    public function getList()
    {
        $ourTeam = $this->ourTeam->orderBy('created_at','desc')->paginate(20);
//        dd($ourTeam);
        return response()->json([
            'tableData' => view('backend.our_team.list', [
                'ourTeam' => $ourTeam
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $ourTeam->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание сотрудника',
            'modalContent' => view('backend.our_team.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'formAction' => route('admin.team.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }


    public function store(Request $request, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $array = [
            'title' => $request->title,
            'site_display' => $site_display,
            'short_desc' => $request->short_desc,
        ];
//        dd($array);
//        dd($request->all());
        $man = $this->ourTeam->create($array);
        if ($request->has('image')) {
            $thumbSizes = ['512', '256', '128'];
            $image = $request->image;
            $this->mediaService->uploadImage($image, 'image', 'our_team', $man->id, $thumbSizes,1);
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.our_team.item', ['man' => $man])->render()
        ]);


    }


    public function edit($manId)
    {
        $foreign_languages = config('project.foreign_locales');
        $man = $this->ourTeam->with('media')->find($manId);
        $create = 0;
        $medias = $man->media->chunk(2);
//        dd($man->media);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование сотрудника',
            'modalContent' => view('backend.our_team.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'man' => $man,
                'formAction' => route('admin.team.update', ['id' => $manId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(Request $request, $manId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $man = $this->ourTeam->find($manId);
        $man->title = $request->input('title');
        $man->site_display = $site_display;
        $man->short_desc = $request->input('short_desc');
        $man->save();


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $manId,
            'content' => view('backend.our_team.item', ['man' => $man])->render()
        ]);
    }

    public function media(Request $request, MediaService $mediaService, $manId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image, 'image', "our_team", $manId, $thumbSizes);
        }

        $teamPictures = $this->ourTeam->whereHas('media', function ($query) use ($manId) {
            $query->where('model_id', $manId)->orderBy('created_at', 'desc');
        })->get();
//        dd($teamPictures);

        return $this->responseJson([
            'media' => view('backend.our_team.media_list', [
                'teamPictures' => $teamPictures,
            ])->render(),
        ]);
    }


    public function mainMedia($manId, $mediaId)
    {
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'our_team')
            ->where('model_id', $manId)->get();
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'our_team')->where('model_id', '=', $manId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }


    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'our_team')->find($mediaId);
        if ($media->type == 'image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }


    public function cropCreate($modelId)
    {
        $images = $this->mediaService->showCroppedModal('our_team', $modelId);
//        var_dump($images->toArray());
        $data = [
            'modalTitle' => 'Обрезание картинки',
            'modalContent' => view('backend.cropper.form', [
                'title' => 'Кроп',
                'images' => $images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()];

        return $this->responseJson($data);
    }

    public function showCrop($mediaId)
    {

        $image = $this->media->where('id', $mediaId)->first();
        $imgObj = Image::make(storage_path('app/public/uploaded_images/' . $image->getOriginal('original_file_name')));
        $pastImage = $this->mediaService->existingCroppedImage($image);
//        dd($pastImage);
        $data = [
            'image' => view('backend.cropper.crop_image', [
                'imageUrl' => $image->original_file_name,
                'width' => $imgObj->width() / 2,
            ])->render(),
            'formAction' => route('admin.team.crop.save', ['imageId' => $mediaId]),
            'pastImage' => $pastImage


        ];
        return $this->responseJson($data);
    }

    public function saveCrop(Request $request, $mediaId)
    {
        $imageItem = $this->media->where('id', $mediaId)->first();

        $params = ['width' => $request->width, 'height' => $request->height, 'offsetX' => $request->X, 'offsetY' => $request->Y];


        $imageSrc = $this->mediaService->cropImage($params, $mediaId);

        return $this->responseJson([
            'croppedImageSrc' => $imageSrc,
            'croppedText' => 'Обрезанная картинка'
        ]);
    }


    public function destroy($manId)
    {
        $medias = $this->media->where(['owner' => 'our_team', 'model_id' => $manId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $man = $this->ourTeam->find($manId);
        $man->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $manId,
        ]);

    }

}
