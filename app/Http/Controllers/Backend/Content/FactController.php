<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\ResponseTrait;
use App\Models\Fact;
use App\Models\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\MediaService;

class FactController extends Controller
{

    use ResponseTrait;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var Fact
     */
    private $fact;
    /**
     * @var MediaService
     */
    private $mediaService;

    public function __construct(Media $media, Fact $fact, MediaService $mediaService)
    {
        $this->media = $media;
        $this->fact = $fact;
        $this->mediaService = $mediaService;
    }

    public function index()
    {
        return view('backend.content.facts.index',[
            'title'=>'Интересные факты'
        ]);
    }

    public function getList()
    {
        $facts = $this->fact->paginate(20);
        return response()->json([
            'tableData' => view('backend.content.facts.list', [
                'facts' => $facts
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $facts->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание факта',
            'modalContent' => view('backend.content.facts.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'formAction' => route('admin.facts.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }


    public function store(Request $request, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }

        $array = [
            'title' => $request->title,
            'site_display' => $site_display,
            'short_desc' => $request->short_desc,
        ];
//        dd($array);
        $fact = $this->fact->create($array);

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.content.facts.item', ['fact' => $fact])->render()
        ]);
    }

    public function edit($factId)
    {
        $foreign_languages = config('project.foreign_locales');
        $fact = $this->fact->with('media')->find($factId);
        $create = 0;
        $medias = $fact->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование страницы',
            'modalContent' => view('backend.content.facts.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'fact' => $fact,
                'formAction' => route('admin.facts.update', ['id' => $factId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(Request $request, $factId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $fact = $this->fact->find($factId);
        $fact->title = $request->input('title');
        $fact->short_desc = $request->input('short_desc');
        $fact->site_display = $site_display;
        $fact->save();


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $factId,
            'content' => view('backend.content.facts.item', ['fact' => $fact])->render()
        ]);
    }

    public function media(Request $request, MediaService $mediaService, $factId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image,'image', "facts", $factId, $thumbSizes);
        }

        $facts = $this->fact->whereHas('media', function ($query) use ($factId) {
            $query->where('model_id', $factId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.content.facts.media_list', [
                'facts' => $facts,
            ])->render(),
        ]);
    }


    public function mainMedia($factId, $mediaId)
    {
//        dd($pageId,$mediaId);
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'facts')
            ->where('model_id', $factId)->get();
//        dd($otherMedia);
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'facts')->where('model_id', '=', $factId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }

    public function updateMedia(Request $request, MediaService $mediaService,$mediaId)
    {

        $image = $request->image;
        $thumbSizes = ['512', '256', '128'];
//        $mediaService->editVideo($image,$mediaId, $thumbSizes);

    }

    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'facts')->find($mediaId);
        if($media->type=='image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }


    public function destroy($factId)
    {
        $medias = $this->media->where(['owner' => 'facts', 'model_id' => $factId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $fact = $this->fact->find($factId);
        $fact->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $factId,
        ]);
    }

    public function cropCreate($modelId)
    {
        $images = $this->mediaService->showCroppedModal('facts',$modelId);
//        var_dump($images->toArray());
        $data = [
            'modalTitle' => 'Обрезание картинки',
            'modalContent' => view('backend.cropper.form', [
                'title' => 'Кроп',
                'images'=>$images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()];

        return $this->responseJson($data);
    }

    public function showCrop($imageId)
    {
        $image = $this->media->where('id',$imageId)->first();
//        dd($image);
        $imgObj = Image::make(storage_path('app/public/uploaded_images/' .$image->getOriginal('original_file_name')));
        $pastImage = $this->mediaService->existingCroppedImage($image);
//        dd($pastImage);
        $data = [
            'image' => view('backend.cropper.crop_image', [
                'imageUrl' => $image->original_file_name,
                'width'=> $imgObj->width() / 2,

            ])->render(),
            'formAction' => route('admin.facts.crop.save',['imageId'=>$imageId]),
            'pastImage'=>$pastImage,
            'imageDesc'=>$image->desc,
            'imageDesc2'=>$image->desc_2


        ];
        return $this->responseJson($data);
    }

    public function saveCrop(Request $request,$imageId)
    {
//        dd($imageId);
        $imageItem = $this->media->where('id',$imageId)->first();
//        var_dump($request->Y);
//        die;
        $params = ['width' => $request->width, 'height' => $request->height, 'offsetX' => $request->X, 'offsetY' => $request->Y];


        $imageSrc = $this->mediaService->cropImage($params,$imageId);

        return $this->responseJson([
            'type'=>'updateCroppedImage',
            'croppedImageSrc' => $imageSrc,
            'croppedText'=>'Обрезанная картинка',
        ]);
    }
}
