<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponseTrait;
use App\Http\Controllers\FormatTrait;
use App\Http\Requests\Backend\NewsCategoryRequest;
use App\Http\Requests\Backend\NewsRequest;
use App\Models\Category;
use App\Models\Media;
use App\Models\News;
use App\Services\ImageService;
use App\Services\MediaService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;
use App\Services\CategoryService;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    use ResponseTrait;
    use FormatTrait;

    private $news;

    /**
     * @var MediaService
     */
    private $mediaService;
    /**
     * @var CategoryService
     */
    private $categoryService;
    /**
     * @var Media
     */
    private $media;

    public function __construct(News $news, MediaService $mediaService, CategoryService $categoryService, Media $media)
    {
        $this->news = $news;
        $this->mediaService = $mediaService;
        $this->categoryService = $categoryService;
        $this->media = $media;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $news = $this->news->orderBy('title')->get();

        return view('backend.content.news.index', [
            'title' => 'Список новостей',
            'news' => $news
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getList(Request $request)
    {
        $news = $this->news->orderBy('created_at', 'desc')->paginate(20);

        return response()->json([
            'tableData' => view('backend.content.news.list', [
                'news' => $news,
                'filters' => $request->all()
            ])->render(),
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $categories = $this->categoryService->categoriesForSelect('news');
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание новости',
            'modalContent' => view('backend.content.news.form', [
                'foreign_languages' => $foreign_languages,
                'categories' => $categories,
                'create' => $create,
                'formAction' => route('admin.content.news.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    /**
     * @param NewsRequest $request
     * @param ImageService $imageService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(NewsRequest $request)
    {
        $request->merge(['site_display' => $request->input('site_display')]);

        $news = $this->news->create($request->all());

        if ($request->has('image')) {
            $thumbSizes = ['512', '256', '128'];
            $images = $request->image;
            foreach ($images as $image) {
                $this->mediaService->uploadImage($image,'news', $news->id, $thumbSizes);
            }
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.content.news.news_item', ['item' => $news])->render()
        ]);


    }

    /**
     * @param $itemId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function edit($itemId)
    {
        $foreign_languages = config('project.foreign_locales');
        $news = $this->news->find($itemId);
        $categories = $this->categoryService->categoriesForSelect('news');
        $medias = $news->media->chunk(2);
        $create = 0;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование новости',
            'modalContent' => view('backend.content.news.form', [
                'foreign_languages' => $foreign_languages,
                'categories' => $categories,
                'create' => $create,
                'medias'=>$medias,
                'news' => $news,
                'formAction' => route('admin.content.news.update', ['itemId' => $itemId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);



    }

    /**
     * @param NewsRequest $request
     * @param $itemId
     * @param ImageService $imageService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(NewsRequest $request, $itemId, ImageService $imageService)
    {
        $news = $this->news->find($itemId);
        if ($request->hasFile('image')) {

            $path = config('project.news_images_upload_path');
            $imgName_old = $news->image;
            $img_old = explode('/', $imgName_old);
            $img_old = array_pop($img_old);

            if (file_exists($path . '/' . $img_old)) {
                @unlink($path . '/' . $img_old);
            }
            $uploadPath = config('project.news_images_upload_path');
            $file = $request->file('image');
            if ($file->isValid()) {
                $imgName = uniqid() . '.' . $file->guessExtension();
                $imageService->setImage($file);
                $imageService->setPath($uploadPath . '/' . $imgName);
                $imageService->resize(1024, null);
                $news->image = $imgName;

            }
        }

        $news->category_id = $request->category_id;
        $news->title = $request->title;
        $news->short_content = $request->short_content;
        $news->long_content = $request->long_content;
        $news->meta_description = $request->meta_description;
        $news->meta_keywords = $request->meta_keywords;

        if ($request->has('site_display')) {
            $news->site_display = 1;
        } else {
            $news->site_display = 0;
        }
        $news->save();

        return response()->json([
            'functions' => ['updateTableRow', 'closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $itemId,
            'content' => view('backend.content.news.news_item', ['item' => $news])->render()
        ]);
    }

    /**
     * @param $itemId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($itemId)
    {
        $news = $this->news->find($itemId);
        $news->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $itemId,
        ]);
    }

    private function fillSeoFields($newsItem)
    {
        $titleRu = $newsItem->getTranslation('title', 'ru');
        $titleEn = $newsItem->getTranslation('title', 'en');
        $titleKz = $newsItem->getTranslation('title', 'kz');

        $metaDescRu = $newsItem->getTranslation('meta_description', 'ru');
        $metaDescEn = $newsItem->getTranslation('meta_description', 'en');
        $metaDescKz = $newsItem->getTranslation('meta_description', 'kz');


        $metaDescription = [
            'ru' => (empty($metaDescRu)) ? $titleRu : $metaDescRu,
            'en' => (empty($metaDescEn)) ? $titleEn : $metaDescEn,
            'kz' => (empty($metaDescKz)) ? $titleKz : $metaDescKz
        ];

        $newsItem->meta_description = $metaDescription;

        $newsItem->save();
    }

    public function media(Request $request, MediaService $mediaService, $itemId)
    {
        $images = $request->image;
//        dd($images);
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image,'image',"news", $itemId,$thumbSizes);
        }

        $news = $this->news->whereHas('media', function ($query) use ($itemId) {
            $query->where('model_id', $itemId)->orderBy('created_at', 'desc');
        })->get();
//        var_dump($news);
        return $this->responseJson([
            'media' => view('backend.content.news.media_list', ['news' => $news,])->render(),
        ]);
    }

    public function mainMedia($itemId, $mediaId)
    {
        $this->mediaService->mainMedia($itemId, $mediaId, 'news');
    }

    public function deleteMedia($mediaId)
    {
        $this->mediaService->deleteMediaItem($mediaId,'news');
    }


    public function cropCreate($itemId)
    {
        $images = $this->mediaService->showCroppedModal('news',$itemId);
//        var_dump($images->toArray());
        $data = [
            'modalTitle' => 'Обрезание картинки',
            'modalContent' => view('backend.cropper.form', [
                'title' => 'Кроп',
                'images'=>$images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()];

        return $this->responseJson($data);
    }

    public function showCrop($mediaId)
    {

        $image = $this->media->where('id',$mediaId)->first();
        $imgObj = Image::make(storage_path('app/public/uploaded_images/' .$image->getOriginal('original_file_name')));
        $pastImage = $this->mediaService->existingCroppedImage($image);
//        dd($pastImage);
        $data = [
            'image' => view('backend.cropper.crop_image', [
                'imageUrl' => $image->original_file_name,
                'width'=> $imgObj->width() / 2,
            ])->render(),
            'formAction' => route('admin.news.crop.save',['mediaId'=>$mediaId]),
            'pastImage'=>$pastImage,


        ];
        return $this->responseJson($data);
    }

    public function saveCrop(Request $request,$mediaId)
    {
        $imageItem = $this->media->where('id',$mediaId)->first();

        $params = ['width' => $request->width, 'height' => $request->height, 'offsetX' => $request->X, 'offsetY' => $request->Y];


        $imageSrc = $this->mediaService->cropImage($params,$mediaId);

        return $this->responseJson([
            'croppedImageSrc' => $imageSrc,
            'imageItem'=>$imageItem,
            'croppedText'=>'Обрезанная картинка'
        ]);
    }


    public function saveMediaDesc(Request $request,$mediaId)
    {
//        dd($request->all());
        $imageItem = $this->media->where('id',$mediaId)->first();

        $imageItem->update([
            'desc'=>$request->imageName,
            'desc_2'=>$request->imageSecondName
        ]);
    }
}
