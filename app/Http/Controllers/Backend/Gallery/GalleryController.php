<?php

namespace App\Http\Controllers\Backend\Gallery;

use App\Http\Controllers\ResponseTrait;
use App\Models\Gallery;
use App\Models\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\MediaService;
use Intervention\Image\Facades\Image;


class GalleryController extends Controller
{

    use ResponseTrait;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var Gallery
     */
    private $gallery;
    /**
     * @var MediaService
     */
    private $mediaService;

    public function __construct(Media $media, Gallery $gallery, MediaService $mediaService)
    {

        $this->media = $media;
        $this->gallery = $gallery;
        $this->mediaService = $mediaService;
    }

    public function index()
    {
        return view('backend.gallery.index', [
            'title' => 'Галерея'
        ]);
    }

    public function getList()
    {
        $albums = $this->gallery->orderBy('created_at','desc')->with('mainImage')->paginate(20);
        return response()->json([
            'tableData' => view('backend.gallery.list', [
                'albums' => $albums
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $albums->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание альбома',
            'modalContent' => view('backend.gallery.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'formAction' => route('admin.gallery.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }

    public function createVideo()
    {
        $foreign_languages = config('project.foreign_locales');
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание альбома',
            'modalContent' => view('backend.gallery.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'formAction' => route('admin.gallery.store'),
                'buttonText' => 'Создать',
                'is_video'   => 1
            ])->render()
        ]);
    }


    public function store(Request $request, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
//        dd($request->all());
        if($request->has('is_video')) {
            $is_video = 1;
        }else{
            $is_video = 0;
        }

        $desc = $request->desc ? $request->desc : '';
//        var_dump( $request->all() );
//        die;
        $array = [
            'title' => $request->title,
            'site_display' => $site_display,
            'short_desc' => $request->short_desc,
            'desc'=>$desc,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
            'is_video' =>$is_video
        ];
//        dd($array);
        $album = $this->gallery->create($array);

        if ($request->has('image')) {
//            dd(123);
            $thumbSizes = ['512', '256', '128'];
            $images = $request->image;
            foreach ($images as $image) {
                $this->mediaService->uploadImage($image,'image','galleries', $album->id, $thumbSizes,1);
            }
        }

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.gallery.item', ['album' => $album])->render()
        ]);
    }

    public function edit($itemId)
    {
        $foreign_languages = config('project.foreign_locales');
        $album = $this->gallery->with('media')->find($itemId);
        $create = 0;
        $medias = $album->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование фотоальбома',
            'modalContent' => view('backend.gallery.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'album' => $album,
                'formAction' => route('admin.gallery.update', ['itemId' => $itemId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function editVideo($itemId)
    {
        $foreign_languages = config('project.foreign_locales');
        $album = $this->gallery->with('media')->find($itemId);
        $create = 0;
        $medias = $album->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование видеоальбома',
            'modalContent' => view('backend.gallery.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'album' => $album,
                'formAction' => route('admin.gallery.update', ['itemId' => $itemId]),
                'buttonText' => 'Сохранить',
                'is_video'=>1
            ])->render()
        ]);
    }

    public function update(Request $request, $itemId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $desc = $request->desc ? $request->desc : '';

        $album = $this->gallery->find($itemId);
        $album->title = $request->input('title');
        $album->short_desc = $request->input('short_desc');
        $album->desc = $desc;
        $album->site_display = $site_display;
        $album->meta_description = $request->meta_description;
        $album->meta_keywords = $request->meta_keywords;
        $album->save();


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $itemId,
            'content' => view('backend.gallery.item', ['album' => $album])->render()
        ]);
    }

    public function media(Request $request, MediaService $mediaService, $itemId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        $lastImage = $this->gallery->with(['media'=>function($query) use ($itemId){
            $query->orderBy('order','desc')->where('model_id',$itemId);
        }])->find($itemId);
        //Осторожно, костыль
        $lastImage = $lastImage->media;
        $i = 0;
        foreach($lastImage as $image)
        {
            if($image->order > $i){
                $i = $image->order;
            }
        }
//        var_dump($i);
//        die;
        $order = $i+1;
//        $lastImage = $lastImage->sortBy('order');

        foreach ($images as $image) {
            $image = $mediaService->uploadImage($image, 'image', "galleries", $itemId, $thumbSizes,0,$order);
        }

        $albums = $this->gallery->whereHas('media', function ($query) use ($itemId) {
            $query->where('model_id', $itemId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.gallery.media_list', [
                'albums' => $albums,
            ])->render(),
        ]);
    }


    public function mainMedia($itemId, $mediaId)
    {
//        dd($pageId,$mediaId);
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'galleries')
            ->where('model_id', $itemId)->get();
//        dd($otherMedia);
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'galleries')->where('model_id', '=', $itemId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }

    public function updateMedia(Request $request, MediaService $mediaService, $mediaId)
    {

        $image = $request->image;
        $thumbSizes = ['512', '256', '128'];
//        $mediaService->editVideo($image,$mediaId, $thumbSizes);

    }

    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'galleries')->find($mediaId);
        if ($media->type == 'image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }


    public function destroy($itemId)
    {
        $medias = $this->media->where(['owner' => 'galleries', 'model_id' => $itemId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $album = $this->gallery->find($itemId);
        $album->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $itemId,
        ]);

    }

    public function cropCreate($modelId)
    {
        $images = $this->mediaService->showCroppedModal('galleries',$modelId);
//        var_dump($images->toArray());
        $data = [
            'modalTitle' => 'Обрезание картинки',
            'modalContent' => view('backend.cropper.form', [
                'title' => 'Кроп',
                'images'=>$images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()];

        return $this->responseJson($data);
    }

    public function showCrop($imageId)
    {
        $image = $this->media->where('id',$imageId)->first();
//        dd($image);
        $imgObj = Image::make(storage_path('app/public/uploaded_images/' .$image->getOriginal('original_file_name')));
        $pastImage = $this->mediaService->existingCroppedImage($image);
//        dd($pastImage);
        $data = [
            'image' => view('backend.cropper.crop_image', [
                'imageUrl' => $image->original_file_name,
                'width'=> $imgObj->width() / 2,

            ])->render(),
            'formAction' => route('admin.gallery.crop.save',['imageId'=>$imageId]),
            'pastImage'=>$pastImage,
            'imageDesc'=>$image->desc,
            'imageDesc2'=>$image->desc_2


        ];
        return $this->responseJson($data);
    }

    public function saveCrop(Request $request,$imageId)
    {
//        dd($imageId);
        $imageItem = $this->media->where('id',$imageId)->first();
//        var_dump($request->Y);
//        die;
        $params = ['width' => $request->width, 'height' => $request->height, 'offsetX' => $request->X, 'offsetY' => $request->Y];


        $imageSrc = $this->mediaService->cropImage($params,$imageId);

        return $this->responseJson([
            'type'=>'updateCroppedImage',
            'croppedImageSrc' => $imageSrc,
            'croppedText'=>'Обрезанная картинка',
        ]);
    }


    public function mediaUp($imageId)
    {
        $image = $this->media->find($imageId);
        $modelId = $image->model_id;
        $order = $image->order;
//        var_dump($order);
//        die;
        $previousImage = $this->media->where('model_id',$modelId)->where('order',$order-1)->first();
        $image->update([
            'order'=> $previousImage->order
        ]);
        $previousImage->update([
            'order' => $order
        ]);
        $images = $this->mediaService->showCroppedModal('galleries',$modelId);
//        $data = [
//            'type'=>'updateBlock',
//            'blockData' => view('backend.cropper.form_item', [
//                'images'=>$images->chunk(2),
//                'buttonText' => 'Сохранить',
//            ])->render()];

        return response()->json([
            'type'=>'updateBlock',
            'blockId'=>'.order-list',
            'blockData'=>view('backend.cropper.form_item', [
                'title' => 'Кроп',
                'images'=>$images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
//        var_dump($previousImage);
//        die;

    }

    public function mediaDown($imageId)
    {
        $image = $this->media->find($imageId);
        $modelId = $image->model_id;
        $order = $image->order;
//        var_dump($order);
//        die;
        $nextImage = $this->media->where('model_id',$modelId)->where('order',$order+1)->first();
        $image->update([
            'order'=> $nextImage->order
        ]);
        $nextImage->update([
            'order' => $order
        ]);
        $images = $this->mediaService->showCroppedModal('galleries',$modelId);
//        $data = [
//            'type'=>'updateBlock',
//            'blockData' => view('backend.cropper.form_item', [
//                'images'=>$images->chunk(2),
//                'buttonText' => 'Сохранить',
//            ])->render()];

        return response()->json([
            'type'=>'updateBlock',
            'blockId'=>'.order-list',
            'blockData'=>view('backend.cropper.form_item', [
                'title' => 'Кроп',
                'images'=>$images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
//        var_dump($previousImage);
//        die;

    }

}
