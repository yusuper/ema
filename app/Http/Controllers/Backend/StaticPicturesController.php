<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\ResponseTrait;
use App\Models\Media;
use App\Models\StaticPicture;
use App\Services\MediaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class StaticPicturesController extends Controller
{
    use ResponseTrait;
    /**
     * @var StaticPicture
     */
    private $staticPicture;
    /**
     * @var Media
     */
    private $media;
    /**
     * @var MediaService
     */
    private $mediaService;

    public function __construct(StaticPicture $staticPicture, Media $media, MediaService $mediaService)
    {
        $this->staticPicture = $staticPicture;
        $this->media = $media;
        $this->mediaService = $mediaService;
    }

    public function index()
    {
        return view('backend.static_pictures.index',[
            'title'=>'Статичные картинки'
        ]);
    }

    public function getList()
    {
        $staticPictures = $this->staticPicture->paginate(20);
        return response()->json([
            'tableData' => view('backend.static_pictures.list', [
                'staticPictures' => $staticPictures
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $staticPictures->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function edit($staticPictureId)
    {
        $foreign_languages = config('project.foreign_locales');
        $staticPicture = $this->staticPicture->with('media')->find($staticPictureId);
        $create = 0;
        $medias = $staticPicture->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование страницы',
            'modalContent' => view('backend.static_pictures.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'staticPicture' => $staticPicture,
                'formAction' => route('admin.static.pictures.update', ['id' => $staticPictureId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(Request $request, $staticPictureId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $staticPicture = $this->staticPicture->find($staticPictureId);
        $staticPicture->title = $request->input('title');
        $staticPicture->save();


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $staticPictureId,
            'content' => view('backend.static_pictures.item', ['staticPicture' => $staticPicture])->render()
        ]);
    }

    public function media(Request $request, MediaService $mediaService, $staticPictureId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image,'image', "static_pictures", $staticPictureId, $thumbSizes);
        }

        $staticPictures = $this->staticPicture->whereHas('media', function ($query) use ($staticPictureId) {
            $query->where('model_id', $staticPictureId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.static_pictures.media_list', [
                'staticPictures' => $staticPictures,
            ])->render(),
        ]);
    }


    public function mainMedia($staticPictureId, $mediaId)
    {
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'static_pictures')
            ->where('model_id', $staticPictureId)->get();
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'static_pictures')->where('model_id', '=', $staticPictureId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }


    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'static_pictures')->find($mediaId);
        if($media->type=='image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }


    public function cropCreate($modelId)
    {
        $images = $this->mediaService->showCroppedModal('static_pictures',$modelId);
//        var_dump($images->toArray());
        $data = [
            'modalTitle' => 'Обрезание картинки',
            'modalContent' => view('backend.cropper.form', [
                'title' => 'Кроп',
                'images'=>$images->chunk(2),
//                'formAction' => route('admin.theatre.artists.store'),
                'buttonText' => 'Сохранить',
            ])->render()];

        return $this->responseJson($data);
    }

    public function showCrop($mediaId)
    {

        $image = $this->media->where('id',$mediaId)->first();
        $imgObj = Image::make(storage_path('app/public/uploaded_images/' .$image->getOriginal('original_file_name')));
        $pastImage = $this->mediaService->existingCroppedImage($image);
//        dd($pastImage);
        $data = [
            'image' => view('backend.cropper.crop_image', [
                'imageUrl' => $image->original_file_name,
                'width'=> $imgObj->width() / 2,
            ])->render(),
            'formAction' => route('admin.static.pictures.crop.save',['imageId'=>$mediaId]),
            'pastImage'=>$pastImage


        ];
        return $this->responseJson($data);
    }

    public function saveCrop(Request $request,$mediaId)
    {
        $imageItem = $this->media->where('id',$mediaId)->first();

        $params = ['width' => $request->width, 'height' => $request->height, 'offsetX' => $request->X, 'offsetY' => $request->Y];


        $imageSrc = $this->mediaService->cropImage($params,$mediaId);

        return $this->responseJson([
            'croppedImageSrc' => $imageSrc,
            'croppedText'=>'Обрезанная картинка'
        ]);
    }

}
