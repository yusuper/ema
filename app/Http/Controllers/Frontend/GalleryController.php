<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    /**
     * @var Gallery
     */
    private $gallery;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Gallery $gallery, Category $category)
    {
        $this->gallery = $gallery;
        $this->category = $category;
    }

    public function index($locale)
    {
        $albums = $this->gallery->get();
        $meta = $this->category->where('slug','galereya')->first();
        $metaTitle = $meta->getTranslation('name',$locale);
        $metaDescription = 'Фото Кыргызстана, красоты Кыргызстана, Видео о Кыргызстане';

        return view('frontend.gallery.index',[
            'albums'=>$albums,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
        ]);
    }

    public function imageGallery($locale)
    {
        $albums = $this->gallery->where(['is_video'=>0,'site_display'=>1])
            ->with('media','mainImage')
            ->orderBy('created_at','desc')
            ->paginate(9);

        $meta = $this->category->where('slug','fotogalereya')->first();
        $append = $locale == 'ru' ? ' Кыргызстана' : ' of Kyrgyzstan';
        $metaTitle = $meta->getTranslation('name',$locale).$append.'  Wandererour';
        $metaDescription = 'Фото о Кыргызстане, фото о красоте Кыргызстана, горы Кыргызстана. Photos of Kyrgyzstan';

//        dd($albums);
        return view('frontend.gallery.index',[
            'albums'=>$albums,
            'title'=>__('common.photogallery'),
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
        ]);
    }

    public function imageInnerGallery(Request $request,$locale,$slug)
    {
        $album = $this->gallery->with('media','mainImage')->where('slug',$slug)->first();
        $currentUrl = $request->url();
        $randomGalleries = $this->gallery
            ->with('media','mainImage')
            ->where('slug','!=',$slug)
            ->where('site_display',1)
            ->get()->random(2);
        $metaTitle = $album->getTranslation('title',$locale);
        $metaDescription = $album->getTranslation('meta_description',$locale);
        $metaKeywords = $album->getTranslation('meta_keywords',$locale);
        return view('frontend.gallery.inner',[
            'album'=>$album,
            'currentUrl'=>$currentUrl,
            'randomGalleries'=>$randomGalleries,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }

    public function videoGallery($locale)
    {
        $albums = $this->gallery->where(['is_video'=>1,'site_display'=>1])
            ->with('media','mainImage')
            ->orderBy('created_at','desc')
            ->paginate(9);
        $meta = $this->category->where('slug','video')->first();
        $append = $locale == 'ru' ? ' Кыргызстана' : ' of Kyrgyzstan';
        $metaTitle = $meta->getTranslation('name',$locale).$append.' Wandererour';
        $metaDescription = 'Видео о Кыргызстане, видео о красоте Кыргызстана, горы Кыргызстана. Video of Kyrgyzstan';

//        dd($albums);
        return view('frontend.gallery.index',[
            'albums'=>$albums,
            'title'=>__('common.video'),
            'is_video'=>1,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
        ]);
    }


    public function videoItem(Request $request,$locale,$slug)
    {
        $album = $this->gallery->where('slug',$slug)->first();

        $randomGalleries = $this->gallery->where('is_video',1)->with('mainImage')->get()->random(1);
        $currentUrl = $request->url();
        $metaTitle = $album->getTranslation('title',$locale);
        $metaDescription = $album->getTranslation('meta_description',$locale);
        $metaKeywords = $album->getTranslation('meta_keywords',$locale);

        return view('frontend.gallery.video_item',[
            'album'=>$album,
            'randomGalleries'=>$randomGalleries,
            'currentUrl'=>$currentUrl,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }
}
