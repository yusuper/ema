<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Fact;
use App\Models\News;
use App\Models\OurTeam;
use App\Models\Page;
use App\Models\StaticPicture;
use App\Models\Tour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    /**
     * @var Page
     */
    private $page;
    /**
     * @var Tour
     */
    private $tour;
    /**
     * @var Fact
     */
    private $fact;
    /**
     * @var StaticPicture
     */
    private $staticPicture;
    /**
     * @var News
     */
    private $news;
    /**
     * @var OurTeam
     */
    private $ourTeam;

    public function __construct(Page $page,
                                Tour $tour,
                                Fact $fact,
                                StaticPicture $staticPicture,
                                News $news,
                                OurTeam $ourTeam)
    {

        $this->page = $page;
        $this->tour = $tour;
        $this->fact = $fact;
        $this->staticPicture = $staticPicture;
        $this->news = $news;
        $this->ourTeam = $ourTeam;
    }

    public function index($locale)
    {

//        dd($this->sphinxSearch);
//        $results = $sphinx->search('wewewe', 'wewewe')->get();
//        dd($results);
//        $mainPicture = $this->page->with('media','mainPage')->where('slug','glavnaya-kartinka')->first();
        $tours = $this->tour
            ->with('mainImage')
            ->where('site_display', 1)
            ->orderBy('created_at','desc')
            ->get();
//        foreach($tours as $tour){
//            dd($tour->mainImage)
//        }
//        dd($tours);
        $facts = $this->fact
            ->with('mainImage')
            ->where('site_display', 1)
            ->orderBy('created_at', 'desc');
        $facts = $facts->get()->random(5);
//        dd($facts);
        $fact = $facts->random(1);
        $aboutUs = $this->page
            ->where('slug', 'o-nas')
            ->where('site_display',1)
            ->first();

        $mainPicture = $this->staticPicture
            ->with('mainImage')
            ->where('slug', 'pervoe')
            ->first();
        $secondPicture = $this->staticPicture
            ->with('mainImage')
            ->where('slug','vtoroe')
            ->first();

        $news = $this->news
            ->with('mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc')
            ->take(4)
            ->get();

//        $meta = $this->setting->where('alias', 'posters')->first();

        $metaTitle = 'Wanderertour путешествие в Кыргызстан';
        $metaDescription = 'красивые места в Кыргызстане, туризм в Кыргызстане, походы в горы Кыргызстана,интересные факты о Кыргызстане,
        Beautiful places in Kyrgyzstan, interesting facts about Kyrgyzstan, tours in Kyrgyzstan, news of Kyrgyzstan';
        $metaKeywords = 'Кыргызстан, красивые места, Иссык-Куль, горы';

        $ourTeam = $this->ourTeam->where('site_display',1)->orderBy('created_at','desc')->with('mainImage')->get();

//        dd($fact);
//        dd($tours->mainImage->original_file_name);
//        dd(storage_path());
        return view('frontend.home.index', [
            'mainPicture' => $mainPicture,
            'tours' => $tours,
            'facts' => $facts,
            'fact' => $fact,
            'aboutUs' => $aboutUs,
            'secondPicture'=>$secondPicture,
            'news'=>$news,
            'ourTeam'=>$ourTeam,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords,
        ]);
    }

    public function ourView($locale)
    {
        $page = $this->page->with('media', 'mainImage')->where('slug', 'o-nas')->first();
//        dd($page->mainImage);
        return view('frontend.about_us.index', [
            'page' => $page
        ]);
    }


}
