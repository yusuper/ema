<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\News;
use App\Models\Tour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * @var News
     */
    private $news;
    /**
     * @var Tour
     */
    private $tour;
    /**
     * @var Category
     */
    private $category;

    public function __construct(News $news, Tour $tour, Category $category)
    {
        $this->news = $news;
        $this->tour = $tour;
        $this->category = $category;
    }

    public function index($locale)
    {
        $news = $this->news
            ->with('mainImage')
            ->where('site_display', 1)
            ->orderBy('created_at','desc')
            ->paginate(6);
        $meta = $this->category->where('slug','novosti')->first();
        $append = $locale == 'en' ? ' in Kyrgyzstan ' : '  Кыргызстана';
        $metaTitle = $meta->getTranslation('name',$locale).$append;
        $metaDescription = 'news in Kyrgyzstan, новости в Кыргызстане в сфере туризма,  новости Кыргызстана, news of Kyrgyzstan';
//        $metaKeywords = ' Туры Кыргызстан, красивые места, Иссык-Куль, горы, tours , Kyrgyzstan';

        return view('frontend.news.index', [
            'news' => $news,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
//            'metaKeywords' => $metaKeywords
        ]);
    }

    public function show(Request $request, $locale, $slug)
    {

//        $randomTour = $this->tour->with('media','mainImage')->get()->random(1);
//        $randomNews = $randomNews->merge($randomTour);
//        dd($randomNews);
        $currentUrl = $request->url();
        $item = $this->news
            ->where('slug', $slug)
            ->first();
        $metaTitle = $item->getTranslation('title',$locale);
        $metaDescription = $item->getTranslation('meta_description',$locale);
        $metaKeywords = $item->getTranslation('meta_keywords',$locale);

        $randomNews = $this->news->with('media','mainImage')->where('slug','!=',$item->slug)->get()->random(4);
        return view('frontend.news.show', [
            'newsItem' => $item,
            'currentUrl' => $currentUrl,
            'randomNews' => $randomNews,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }
}
