<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelController extends Controller
{
    private $seoAlias = 'oteli';
    /**
     * @var Hotel
     */
    private $hotel;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Hotel $hotel, Category $category)
    {
        $this->hotel = $hotel;
        $this->category = $category;
    }

    public function index($locale)
    {
        $seo = $this->getSeo($locale);

        $hotels = $this->hotel
            ->with('media','mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc')
            ->paginate(10);

        return view('frontend.hotels.index',[
            'hotels'=>$hotels,
            'pageItem' => __('common.hotels')
        ]);
    }

    public function getList(string $locale, Request $request)
    {
        $title = $request->input('filterSearch');


        $hotels = $this->hotel
            ->with('media','mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc');

        if ($title) {
            $str = mb_strtolower($title);
            $hotels->whereRaw("LOWER(`title`->'$.\"$locale\"') like '%$str%'");
        }

        $hotels = $hotels->paginate(2);

        return response()->json([
            'content' => view('frontend.hotels.list', [
                'hotels' => $hotels,
                'pagination' => view('backend.common.pagination', [
                    'links' => $hotels->appends($request->all())->links('frontend.common.pagination')
                ])->render(),
            ])->render()
        ]);
    }


    private function getSeo(string $locale): array
    {
        $data = $this->category->where('slug', $this->seoAlias)->first();

        return (isset($data->name[$locale])) ? $data->name[$locale] : [];
    }
}
