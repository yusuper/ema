<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Leisure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeisureController extends Controller
{
    /**
     * @var Leisure
     */
    private $leisure;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Leisure $leisure,Category $category)
    {
        $this->leisure = $leisure;
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->where('owner','leisure')->get();
        $leisures = $this->leisure
            ->with('media','mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc')
            ->paginate(10);

        return view('frontend.leisure.index',[
            'leisures'=>$leisures,
            'categories'=>$categories,
            'pageItem' => __('common.leisure')
        ]);
    }

    public function categoryLeisure($locale,$categoryId)
    {
        $leisures = $this->leisure
            ->with('media','mainImage','category')
            ->where('site_display',1)
            ->where('category_id',$categoryId)
            ->orderBy('created_at','desc')
            ->paginate(9);
        $category = $this->leisure
            ->with('category')
            ->where('category_id',$categoryId)
            ->first();


        return response()->json([
            'type'=>'updateBlock',
            'blockId'=>'#leisureList',
            'blockData'=> view('frontend.leisure.leisure_list',[
                'leisures'=>$leisures,
                'category'=>$category
            ])->render()
        ]);
    }

    public function leisureItem($locale,$slug)
    {

        $leisure = $this->leisure
            ->with('media')
            ->where('slug',$slug)
            ->first();

        return view('frontend.leisure.leisure_item',[
            'leisure'=>$leisure,
            'page'=>$leisure
        ]);
    }
}
