<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Tour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TourController extends Controller
{
    /**
     * @var Tour
     */
    private $tour;
    /**
     * @var Category
     */
    private $category;

    public function __construct(Tour $tour, Category $category)
    {
        $this->tour = $tour;
        $this->category = $category;
    }

    public function index($locale)
    {
        $tours = $this->tour
            ->with('media','mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc')
            ->paginate(10);
        $meta = $this->category->where('slug','tury')->first();
        $append = $locale == 'en' ? ' tours in Kyrgyzstan ' : ' туры в Кыргызстане';
        $metaTitle = $meta->getTranslation('name',$locale).$append;
        $metaDescription = 'tours in Kyrgyzstan, туры в Кыргызстане, походы в горы в Кыргызстане, mountains in Kyrgyzstan';
        $metaKeywords = ' Туры Кыргызстан, красивые места, Иссык-Куль, горы, tours , Kyrgyzstan';

        return view('frontend.tours.index',[
            'tours'=>$tours,
            'pageItem' => __('common.tours'),
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }

    public function getList(string $locale, Request $request)
    {

        $title = $request->input('filterSearch');


        $tours = $this->tour
            ->with('media','mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc');

        if ($title) {
            $str = mb_strtolower($title);
            $tours->whereRaw("LOWER(`title`->'$.\"$locale\"') like '%$str%'");
        }

        $tours = $tours->paginate(9);

        return response()->json([
            'content' => view('frontend.tours.list', [
                'tours' => $tours,
                'pagination' => view('backend.common.pagination', [
                    'links' => $tours->appends($request->all())->links('frontend.common.pagination')
                ])->render(),
            ])->render()
        ]);
    }

    public function tourItem(Request $request,$locale,$slug)
    {
        $tour = $this->tour
            ->with('media')
            ->where('slug',$slug)
            ->first();

        $metaTitle = $tour->getTranslation('title',$locale);
        $metaDescription = $tour->getTranslation('meta_description',$locale);
        $metaKeywords = $tour->getTranslation('meta_keywords',$locale);

        $currentUrl = $request->url();
//        dd($request->url());
        return view('frontend.tours.tour_item',[
            'tour'=>$tour,
            'page'=>$tour,
            'currentUrl'=>$currentUrl,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }

    public function vipTour($locale)
    {
        $title = __('tour.private_tours');
        $tours = $this->tour
            ->with('media','mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc')
            ->paginate(9);

        $meta = $this->category->where('slug','vip')->first();
        $append = $locale == 'en' ? ' tours in Kyrgyzstan ' : ' туры в Кыргызстане';
        $metaTitle = $meta->getTranslation('name',$locale).$append;
        $metaDescription = 'vip private tours in Kyrgyzstan,приватные туры в Кыргызстане, походы в горы в Кыргызстане, mountains in Kyrgyzstan';
        $metaKeywords = 'приватные туры Кыргызстан, красивые места, Иссык-Куль, горы, private tours , Kyrgyzstan';

        return view('frontend.tours.index',[
            'tours'=>$tours,
            'title'=>$title,
            'pageItem' => __('common.tours'),
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }

    public function standardTour($locale)
    {
        $title = __('tour.standard_tours');
        $tours = $this->tour
            ->with('media','mainImage')
            ->where('site_display',1)
            ->orderBy('created_at','desc')
            ->paginate(9);
        $standardTour = 1;
        $append = $locale == 'en' ? ' tours in Kyrgyzstan ' : ' туры в Кыргызстане';
        $meta = $this->category->where('slug','standart')->first();
        $metaTitle = $meta->getTranslation('name',$locale).$append;
        $metaDescription = 'standard tours in Kyrgyzstan,стандартные туры в Кыргызстане, походы в горы в Кыргызстане, mountains in Kyrgyzstan';
        $metaKeywords = 'Стандартные туры Кыргызстан, красивые места, Иссык-Куль, горы, tours , Kyrgyzstan';

        return view('frontend.tours.index',[
            'tours'=>$tours,
            'title'=>$title,
            'pageItem' => __('common.tours'),
            'standardTour'=>$standardTour,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }

    public function standardItem(Request $request,$locale,$slug)
    {
        $tour = $this->tour
            ->with('media')
            ->where('slug',$slug)
            ->first();
        $standardTour = 1;

        $currentUrl = $request->url();
        $metaTitle = $tour->getTranslation('title',$locale);
        $metaDescription = $tour->getTranslation('meta_description',$locale);
        $metaKeywords = $tour->getTranslation('meta_keywords',$locale);
//        dd($request->url());
        return view('frontend.tours.tour_item',[
            'tour'=>$tour,
            'page'=>$tour,
            'currentUrl'=>$currentUrl,
            'standardTour'=>$standardTour,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }

    public function privateItem(Request $request,$locale,$slug)
    {
        $tour = $this->tour
            ->with('media')
            ->where('slug',$slug)
            ->first();
        $privateTour = 1;

        $currentUrl = $request->url();
        $metaTitle = $tour->getTranslation('title',$locale);
        $metaDescription = $tour->getTranslation('meta_description',$locale);
        $metaKeywords = $tour->getTranslation('meta_keywords',$locale);
//        dd($request->url());
        return view('frontend.tours.tour_item',[
            'tour'=>$tour,
            'page'=>$tour,
            'currentUrl'=>$currentUrl,
            'privateTour'=>$privateTour,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaKeywords' => $metaKeywords
        ]);
    }
}
