<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Media;
use App\Services\MediaService;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    use ResponseTrait;

    /**
     * @var Hotel
     */
    private $hotel;
    /**
     * @var Media
     */
    private $media;

    public function __construct(Hotel $hotel, Media $media)
    {

        $this->hotel = $hotel;
        $this->media = $media;
    }


    public function index()
    {
        return view('backend.content.hotels.index',[
            'title'=>'Отели'
        ]);
    }

    public function getList()
    {
        $hotels = $this->hotel->paginate(20);
        return response()->json([
            'tableData' => view('backend.content.hotels.list', [
                'hotels' => $hotels
            ])->render(),
            'pagination' => view('backend.common.pagination', [
                'links' => $hotels->links('vendor.pagination.bootstrap-4'),
            ])->render(),
        ]);
    }

    public function create()
    {
        $foreign_languages = config('project.foreign_locales');
        $create = 1;

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Создание отеля',
            'modalContent' => view('backend.content.hotels.form', [
                'foreign_languages' => $foreign_languages,
                'create' => $create,
                'formAction' => route('admin.hotels.store'),
                'buttonText' => 'Создать'
            ])->render()
        ]);
    }


    public function store(Request $request, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }

        $array = [
            'title' => $request->title,
            'site_display' => $site_display,
            'short_desc' => $request->short_desc,
            'desc' => $request->desc,
            'price'=>$request->price,
            'phone'=>$request->phone,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
        ];
//        dd($array);
        $hotel = $this->hotel->create($array);

        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'prepend-table-row',
            'table' => '#ajaxTable',
            'content' => view('backend.content.hotels.hotel_item', ['hotel' => $hotel])->render()
        ]);
    }

    public function edit($hotelId)
    {
        $foreign_languages = config('project.foreign_locales');
        $hotel = $this->hotel->with('media')->find($hotelId);
        $create = 0;
        $medias = $hotel->media->chunk(2);

        return response()->json([
            'functions' => ['initEditor'],
            'type' => 'updateModal',
            'modal' => '#superLargeModal',
            'modalTitle' => 'Редактирование страницы',
            'modalContent' => view('backend.content.hotels.form', [
                'medias' => $medias,
                'create' => $create,
                'foreign_languages' => $foreign_languages,
                'hotel' => $hotel,
                'formAction' => route('admin.hotels.update', ['id' => $hotelId]),
                'buttonText' => 'Сохранить',
            ])->render()
        ]);
    }

    public function update(Request $request, $hotelId, MediaService $mediaService)
    {
        if ($request->has('site_display')) {
            $site_display = 1;
        } else {
            $site_display = 0;
        }
        $hotel = $this->hotel->find($hotelId);
        $hotel->title = $request->input('title');
        $hotel->short_desc = $request->input('short_desc');
        $hotel->desc = $request->input('desc');
        $hotel->phone = $request->input('phone');
        $hotel->site_display = $site_display;
        $hotel->price = $request->price;
        $hotel->meta_description = $request->meta_description;
        $hotel->meta_keywords = $request->meta_keywords;
        $hotel->save();


        return response()->json([
            'functions' => ['closeModal'],
            'modal' => '#superLargeModal',
            'type' => 'update-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $hotelId,
            'content' => view('backend.content.hotels.hotel_item', ['hotel' => $hotel])->render()
        ]);
    }

    public function media(Request $request, MediaService $mediaService, $hotelId)
    {
        $images = $request->image;
        $thumbSizes = ['512', '256', '128'];
        foreach ($images as $image) {
            $mediaService->uploadImage($image,'image', "hotels", $hotelId, $thumbSizes);
        }

        $hotels = $this->hotel->whereHas('media', function ($query) use ($hotelId) {
            $query->where('model_id', $hotelId)->orderBy('created_at', 'desc');
        })->get();

        return $this->responseJson([
            'media' => view('backend.content.hotels.media_list', [
                'hotels' => $hotels,
            ])->render(),
        ]);
    }


    public function mainMedia($hotelId, $mediaId)
    {
//        dd($pageId,$mediaId);
        $otherMedia = $this->media
            ->where('main_image', '!=', '0')
            ->where('owner', 'hotels')
            ->where('model_id', $hotelId)->get();
//        dd($otherMedia);
        foreach ($otherMedia as $other) {

            $other->main_image = 0;
            $other->update();
        }

        $media = $this->media->where('owner', '=', 'hotels')->where('model_id', '=', $hotelId)->where('id', $mediaId)->first();
        $media->main_image = 1;
        $media->save();
    }

    public function updateMedia(Request $request, MediaService $mediaService,$mediaId)
    {

        $image = $request->image;
        $thumbSizes = ['512', '256', '128'];
//        $mediaService->editVideo($image,$mediaId, $thumbSizes);

    }

    public function deleteMedia($mediaId)
    {
        $media = $this->media->where('owner', 'hotels')->find($mediaId);
        if($media->type=='image') {
            if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
            }
            if ($media->params) {
                foreach ($media->params as $size) {
                    if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                        unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                    }
                }
            }
        }
        $media->delete();
    }


    public function destroy($hotelId)
    {
        $medias = $this->media->where(['owner' => 'hotels', 'model_id' => $hotelId])->get();

        foreach ($medias as $media) {
            if ($media->type == 'image') {
                $path = config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name');
                if (file_exists(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'))) {
                    unlink(config('project.media_upload_path') . '/' . $media->getOriginal('original_file_name'));
                }
                if ($media->params) {
                    foreach ($media->params as $size) {
                        if (file_exists(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'))) {
                            unlink(config('project.media_upload_path') . '/' . $size . '_' . $media->getOriginal('original_file_name'));
                        }
                    }
                }
            }
            $media->delete();
        }
        $hotel = $this->hotel->find($hotelId);
        $hotel->delete();

        return response()->json([
            'type' => 'delete-table-row',
            'table' => '#ajaxTable',
            'row' => '.row-' . $hotelId,
        ]);

    }
}
