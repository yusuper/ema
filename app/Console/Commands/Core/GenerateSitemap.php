<?php

namespace App\Console\Commands\Core;

use Illuminate\Console\Command;
use App\Models\Gallery;
use App\Models\News;
use App\Models\Tour;
use App\Models\Category;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:sitemap-generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = Sitemap::create();
        foreach (config('project.foreign_locales') as $locale) {
            $sitemap->add(Url::create(route('home', ['locale' => $locale])));

            $sitemap->add(Url::create(route('home.news', ['locale' => $locale])));


            News::all()
                ->where('site_display', 1)
                ->each(function (News $newsItem) use ($sitemap, $locale) {
                    $sitemap->add(Url::create(route('home.news.show', [
                        'locale' => $locale,
                        'slug' => $newsItem->slug
                    ])));
                });

            $sitemap->add(Url::create(route('home.tours', ['locale' => $locale])));
            Tour::all()
                ->where('site_display', '=', 1)
                ->each(function (Tour $tour) use ($sitemap, $locale) {
                    $sitemap->add(Url::create(route('home.tours.item', [
                        'locale' => $locale,
                        'slug' => $tour->slug
                    ])));
                });

            $sitemap->add(Url::create(route('home.gallery.image',['locale'=>$locale])));

            Gallery::all()
                ->where('site_display', '=', 1)
                ->where('is_video', '!=', 1)
                ->each(function (Gallery $gallery) use ($sitemap, $locale) {
                    $sitemap->add(Url::create(route('home.gallery.image.inner', [
                        'locale' => $locale,
                        'slug' => $gallery->slug
                    ])));
                });

            $sitemap->add(Url::create(route('home.gallery.video',['locale'=>$locale])));

            Gallery::all()
                ->where('site_display', '=', 1)
                ->where('is_video', '=', 1)
                ->each(function (Gallery $gallery) use ($sitemap, $locale) {
                    $sitemap->add(Url::create(route('home.gallery.video.item', [
                        'locale' => $locale,
                        'slug' => $gallery->slug
                    ])));
                });

        }
        $sitemap->writeToFile(public_path('sitemap.xml'));
    }
}
