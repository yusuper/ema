<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;

class Hotel extends Model
{
    use Sluggable;
    use HasTranslations;

    public $translatable = ['title', 'short_desc', 'desc','address', 'meta_keywords', 'meta_description'];
    protected $table = 'hotels';

    protected $fillable = [
        'title',
        'slug',
        'short_desc',
        'desc',
        'address',
        'phone',
        'price',
        'site_display',
        'meta_keywords',
        'meta_description',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'hotels')
//            ->orderBy('main_image','desc')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','hotels')
            ->where('main_image','=',1);
    }
}
