<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Page extends Model
{
    use Sluggable;
    use HasTranslations;

    public $translatable = ['title', 'content', 'meta_keywords', 'meta_description'];

    protected $table = 'pages';
    protected $fillable = [
        'title',
        'slug',
        'content',
        'site_display',
        'meta_keywords',
        'meta_description',
    ];


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'pages')
//            ->orderBy('main_image','desc')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','pages')
            ->where('main_image','=',1);
    }
}
