<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
//use Laravel\Scout\Searchable;


class Gallery extends Model
{
//    use Searchable;
    use Sluggable;
    use HasTranslations;

    protected $table = 'galleries';
    public $translatable = ['title','short_desc','desc','meta_keywords','meta_description'];

    protected $fillable = [
        'title',
        'short_desc',
        'desc',
        'slug',
        'site_display',
        'is_video',
        'meta_description',
        'meta_keywords'

    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

//    public function searchableAs()
//    {
//        return 'galleries_index';
//    }
//
//    public function toSearchableArray()
//    {
//        $array = $this->toArray();
//        return $array;
//    }


    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'galleries')
            ->orderBy('order','asc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','galleries')
            ->where('main_image','=',1);
    }
}
