<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Tour extends Model
{

    use Sluggable;
    use HasTranslations;

    public $translatable = ['title', 'short_desc', 'desc','standard_desc','private_desc','address', 'meta_keywords', 'meta_description'];
    protected $table = 'tours';

    protected $fillable = [
        'title',
        'slug',
        'category_id',
        'short_desc',
        'desc',
        'standard_desc',
        'private_desc',
        'address',
        'price',
        'site_display',
        'meta_keywords',
        'meta_description',
    ];


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'tours')
//            ->orderBy('main_image','desc')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','tours')
            ->where('main_image','=',1);
    }

    public function croppedImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','tours')
        ->where('main_image','=',1);
    }

    public function category()
    {
        return $this->belongsTo(TourCategory::class,'category_id');
    }
}
