<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class TourCategory extends Model
{
    use HasTranslations;

    public $translatable = ['title'];
    protected $table = 'tour_categories';
    protected $fillable = [
        'title'
    ];
}
