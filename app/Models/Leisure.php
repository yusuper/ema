<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Leisure extends Model
{
    use Sluggable;
    use HasTranslations;

    protected $table = 'leisures';
    public $translatable = ['title', 'short_desc', 'desc','address', 'meta_keywords', 'meta_description'];
    protected $fillable = [
        'title',
        'city_id',
        'slug',
        'short_desc',
        'category_id',
        'desc',
        'address',
        'phone',
        'price',
        'site_display',
        'meta_keywords',
        'meta_description',
    ];


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'leisures')
//            ->orderBy('main_image','desc')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','leisures')
            ->where('main_image','=',1);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
