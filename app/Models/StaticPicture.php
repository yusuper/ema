<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class StaticPicture extends Model
{
    use HasTranslations;

    protected $table = 'static_pictures';
    protected $fillable = [
        'title',
        'slug'
    ];


    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'static_pictures')
//            ->orderBy('main_image','desc')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','static_pictures')
            ->where('main_image','=',1);
    }

}
