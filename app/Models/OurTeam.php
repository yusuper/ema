<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;


class OurTeam extends Model
{
    use HasTranslations;

    protected $table = 'our_teams';
    public $translatable = ['title','short_desc'];

    protected $fillable = [
        'title',
        'short_desc',
        'site_display'
    ];

    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'our_team')
//            ->orderBy('main_image','desc')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','our_team')
            ->where('main_image','=',1);
    }
}
