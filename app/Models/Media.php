<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $fillable = [
        'owner',
        'client_file_name',
        'original_file_name',
        'desc',
        'desc_2',
        'size',
        'mime',
        'model_id',
        'main_image',
        'params',
        'type',
        'order',
        'video_id',
    ];

    public function setParamsAttribute($value)
    {
        $this->attributes['params'] = json_encode($value);
    }

    public function getParamsAttribute($value)
    {
        $q = json_decode($value);
        return $q;
    }

    public function getOriginalFileNameAttribute($value)
    {
        return asset('storage/uploaded_images/'.$value);
    }

    protected $appends = [
        'thumb_128',
        'thumb_256',
        'thumb_512',
        'thumb_greyscale_128',
        'thumb_greyscale_256',
        'thumb_greyscale_512',
        'cropped'
    ];

    public function getCroppedAttribute()
    {
        $originalFileName = $this->getOriginal('original_file_name');
        $fileName = 'cropped_' . $originalFileName ;
        if (file_exists(storage_path('app/public/uploaded_images/' . $fileName)))
        {
            return asset('storage/uploaded_images/' . $fileName);
        }/* else {
            return '/app/images/no_image_placeholder.jpg';
        }*/
    }

    public function getThumb128Attribute()
    {
        if ($this->type == 'file')
        {
            return null;
        }

        $originalFileName = $this->getOriginal('original_file_name');
        $fileName = '128_' . $originalFileName ;
        if (file_exists(storage_path('app/public/uploaded_images/' . $fileName)))
        {
            return asset('storage/uploaded_images/' . $fileName);
        } else {
            return '/app/images/no_image_placeholder.jpg';
        }
    }

    public function getThumb256Attribute()
    {
        if ($this->type == 'file')
        {
            return null;
        }

        $originalFileName = $this->getOriginal('original_file_name');
        $fileName = '256_' . $originalFileName ;
        if (file_exists(storage_path('app/public/uploaded_images/' . $fileName)))
        {
            return asset('storage/uploaded_images/' . $fileName);
        } else {
            return '/app/images/no_image_placeholder.jpg';
        }
    }

    public function getThumb512Attribute()
    {
        if ($this->type == 'file')
        {
            return null;
        }

        $originalFileName = $this->getOriginal('original_file_name');
        $fileName = '512_' . $originalFileName ;
        if (file_exists(storage_path('app/public/uploaded_images/' . $fileName)))
        {
            return asset('storage/uploaded_images/' . $fileName);
        } else {
            return '/app/images/no_image_placeholder.jpg';
        }
    }

    public function getThumbGreyscale128Attribute()
    {
        if ($this->type == 'file')
        {
            return null;
        }
        $originalFileName = $this->getOriginal('original_file_name');
        $fileName = '128_greyscale_' . $originalFileName ;
        if (file_exists(storage_path('app/public/uploaded_images/' . $fileName)))
        {
            return asset('storage/uploaded_images/' . $fileName);
        } else {
            return '/app/images/no_image_placeholder.jpg';
        }
    }

    public function getThumbGreyscale256Attribute()
    {
        if ($this->type == 'file')
        {
            return null;
        }
        $originalFileName = $this->getOriginal('original_file_name');
        $fileName = '256_greyscale_' . $originalFileName ;
        if (file_exists(storage_path('app/public/uploaded_images/' . $fileName)))
        {
            return asset('storage/uploaded_images/' . $fileName);
        } else {
            return '/app/images/no_image_placeholder.jpg';
        }
    }

    public function getThumbGreyscale512Attribute()
    {
        if ($this->type == 'file')
        {
            return null;
        }

        $originalFileName = $this->getOriginal('original_file_name');
        $fileName = '512_greyscale_' . $originalFileName ;
        if (file_exists(storage_path('app/public/uploaded_images/' . $fileName)))
        {
            return asset('storage/uploaded_images/' . $fileName);
        } else {
            return '/app/images/no_image_placeholder.jpg';
        }
    }

    public function getThumbnail($size)
    {
        if ($this->type == 'file')
        {
            return null;
        }

        if (file_exists(storage_path('app/public/uploaded_images/' . $size . '_' . $this->getOriginal('original_file_name'))))
        {
            return asset('storage/uploaded_images/' . $size . '_' . $this->getOriginal('original_file_name'));
        }
    }


}
