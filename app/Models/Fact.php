<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;

class Fact extends Model
{
    use Sluggable;
    use HasTranslations;

    protected $table = 'facts';
    public $translatable = ['title','short_desc'];

    protected $fillable = [
        'title',
        'short_desc',
        'slug',
        'site_display',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public function media()
    {
        return $this->hasMany(Media::class, 'model_id', 'id')
            ->where('owner', 'facts')
            ->orderBy('created_at','desc');
    }

    public function mainImage()
    {
        return $this->hasOne(Media::class, 'model_id')->where('owner','=','facts')
            ->where('main_image','=',1);
    }
}
